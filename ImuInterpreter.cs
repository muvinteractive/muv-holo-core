﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static BirdUWP.StaticDefs;
using static BirdUWP.ElapsedSince;

namespace BirdUWP
{
    using SpPacket = Packet;

    /******************************************************************************
        Module:	MUV Client
        File:	CIMUDataIntpr.cpp
        Desc:	6/9-Axis Data Interpreter class
                Processes the 6/9-Axis raw data and extracts the basic linear/axial 
                movement parameters. This data is used as input to the UI logic module.
        Author:	#gr
        Creatd:	5-2014
        Remarks:

            see http://www.x-io.co.uk/res/doc/madgwick_internal_report.pdf for explenation
            about quaternions and fusion algorithm
            Q = q0 q1 q2 q3 = cos(phi/2) -rx*sin(phi/2) -ry*sin(phi/2) -rz*sin(phi/2) 

        -----------------------------
        Copyright (C) MUV Interactive
        All rights reserved.

    *******************************************************************************/


    //extern void MotionDebugHandleCmpsAdjustment(int x, int y, int z);
    //extern void MotionDebugHandleCmpsAxes(double x, double y, double z);


    //extern void MotionDebugHandleAcclAndGyroAxes(double A_aver_x,
    //                                             double A_aver_y,
    //                                             double A_aver_z,
    //                                             double staticA_aver_x,
    //                                             double staticA_aver_y,
    //                                             double staticA_aver_z,
    //                                             double G_aver_x,
    //                                             double G_aver_y,
    //                                             double G_aver_z,
    //                                             double orient_aver_x,
    //                                             double orient_aver_y,
    //                                             double orient_aver_z,
    //                                             double m_Q_q0,
    //                                             double m_Q_q1,
    //                                             double m_Q_q2,
    //                                             double m_Q_q3,
    //                                             double translation_aver_x,
    //                                             double translation_aver_y,
    //                                             double translation_aver_z,
    //                                             double velocity_aver_x,
    //                                             double velocity_aver_y,
    //                                             double velocity_aver_z);

    /******************************************************************************
        Defines
    *******************************************************************************/
    public class cvals
    {
        static public int n_gyro_calib_pkts = 400;       // how many packets used for G calibration
        static public int n_magneto_calib_max_pkts = 12 * 100;   // how many packets used for G calibration - about 12 secs
        static public int calibration_timeout_msec = 10000;
        static public int calibration_progress_resolution = 3; // % of 100
        static public int compass_calibration_timeout_msec = (int)(n_magneto_calib_max_pkts * 10 * 4); // allow 200% more than expected time (for 100pps) since in multithimble mode it takes more time)
        static public double PI = 3.14159265358979323846;
        static public double rad2PI = PI * 2.0;
    }

    public class ImuInterpreter
    {

        enum eState
        {
            STATE_INIT,
            STATE_INIT_CALIB,
            STATE_CALIB,
            STATE_CALIB_DONE,
            STATE_INIT_SESS,
            STATE_SESSION,
            STATE_INIT_COMPASS_CALIB,
            STATE_COMPASS_CALIB,
            STATE_COMPASS_CALIB_DONE,
            STATE__NUM
        };
        enum eMotionState
        {
            MOTSTATE_INVALID,
            MOTSTATE_GOTO_REST,
            MOTSTATE_REST,
            MOTSTATE_GOTO_MOTION,
            MOTSTATE_MOTION,
            MOTSTATE__NUM
        };
        enum eDevState
        {
            DEVSTATE_INVALID,
            DEVSTATE_ATREST,
            DEVSTATE_CONST_MOTION,
            DEVSTATE_ACCEL_MOTION,
            DEVSTATE__NUM
        };
        eState m_state;
        eMotionState m_motionState;
        int m_nCalibPkts;
        uint m_pktProcessId;
        int m_nBalanceQPkts;
        CVector m_velocity;
        CVector m_translation;
        CVecExpFilter m_aFiltLP;
        CVecExpFilter m_gFiltLP;
        CQuaternion m_Q = new CQuaternion();                    // quaternion of sensor frame relative to auxiliary frame
        SpPacket m_prevPkt;
        // a patch to account for the glitch in the MCU
        bool m_bGyroXNegSat = false;
        bool m_bGyroYNegSat = false;
        bool m_bGyroZNegSat = false;
        static CVector m_gyroOffset = new CVector();
        bool m_bGyroOffsetValid = false;
        public double m_samplingPeriodSecs = 0;
        DateTime m_calibStart;
        static bool m_blockProcessing;
        //std::function<void(int, int)>	m_calibStateCB;
        int m_calibProgress = 0;
        int m_calibResult = 0;

        const int S_OK = 0;
        const int E_FAIL = -1;
        const int S_FALSE = 1;

        const int S_NEW_STATE = (int)eState.STATE_CALIB_DONE;
        int NEXT_STATE(eState st) { m_state = st; return S_NEW_STATE; }

        /******************************************************************************
            Variable definitions

        *******************************************************************************/

        const int IMU_SAMPLE_RATE_HZ = 200;

        const float GRAVITY_MSS = 9.81f;

        const float MAX_ALLOWED_RECONST_PKT_N = 15; //original value was 5. Changed to 15 from version 6.1... since the meaning of lost packets is changed dramatically

        const float MAX_SAMPLE_PERIOD_SECS = 0.1f;

        const float twoKpDef = 2.0f * .5f;  // 2 * proportional gain
        const float twoKiDef = 2.0f * 0.0f; // 2 * integral gain

        const int INITSESS_PKT_NUM = 10;
        const int MIN_PKTS_TO_CALIB = 100000;   // min packets between calibrations
        const int N_CALIB_PKTS = 400;   // how many packets used for calibration
        const float GYRO_MAX_REST_VAL_RADSEC = 0.2f;     // threshold for Gyro rest decision
        const float ACCEL_MAX_REST_VAL_G = 0.02f;        // threshold for Accel rest decision
        const int N_PKTS_ATREST = 50;//(15;	// how many packets to establish rest

        const int DEF_X_SENSIT = 2500;      // horizontal movement def value
        const int DEF_Y_SENSIT = 2000;      // vertical movement def value
        const int DEF_Z_SENSIT = 50;        // depth movement def value

        const int MAX_X_SENSIT = 5000;      // horizontal movement max value
        const int MAX_Y_SENSIT = 4000;      // vertical movement max value
        const int MAX_Z_SENSIT = 150;       // depth movement max value

        const int MIN_X_SENSIT = 500;       // horizontal movement min value
        const int MIN_Y_SENSIT = 1000;      // vertical movement min value
        const int MIN_Z_SENSIT = 20;        // depth movement min value

        const float DEF_XY_LP_ALPHA = 0.3f;      // LP smoothing const

        const float ACC_GYRO_LP_CONST = 0.04f;

        const int ACC_G_VAL = 16384;            // assume 1g = 16K, from MPU datasheet)
        const int GYRO_DEGSEC_VAL = 131;
        const float DEG_TO_GRAD_FACT = 3.1415926f / 180.0f;

        double integralFBx = 0.0;
        double integralFBy = 0.0;
        double integralFBz = 0.0;  // integral error terms scaled by Ki
        double twoKp = twoKpDef;                                            // 2 * proportional gain (Kp)
        double twoKi = twoKiDef;                                            // 2 * integral gain (Ki)
        DateTime g_lastInputTime = new DateTime();

        bool IsVectorValid(CVector v)
        {
            return v.m_x != 0.0 || v.m_y != 0.0 || v.m_z != 0.0;
        }

        void MahonyAHRSupdate(CVector Gin, CVector Ain, CVector Min)
        {
            double q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;
            double hx, hy, bx, bz;
            double halfvx, halfvy, halfvz, halfwx, halfwy, halfwz;
            double halfex, halfey, halfez;
            double qa, qb, qc;

            CVector G = new CVector(Gin);
            CVector A = new CVector(Ain);
            CVector M = new CVector(Min);

            // Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
            if (!IsVectorValid(M))
            {
                MahonyAHRSupdateIMU(G, A);
                return;
            }

            // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
            if (IsVectorValid(A))
            {

                // Normalise accelerometer measurement
                A.Normlize();

                // Normalise magnetometer measurement
                M.Normlize();

                // Auxiliary variables to avoid repeated arithmetic
                q0q0 = m_Q.m_q1 * m_Q.m_q1;
                q0q1 = m_Q.m_q1 * m_Q.m_q2;
                q0q2 = m_Q.m_q1 * m_Q.m_q3;
                q0q3 = m_Q.m_q1 * m_Q.m_q4;
                q1q1 = m_Q.m_q2 * m_Q.m_q2;
                q1q2 = m_Q.m_q2 * m_Q.m_q3;
                q1q3 = m_Q.m_q2 * m_Q.m_q4;
                q2q2 = m_Q.m_q3 * m_Q.m_q3;
                q2q3 = m_Q.m_q3 * m_Q.m_q4;
                q3q3 = m_Q.m_q4 * m_Q.m_q4;

                // Reference direction of Earth's magnetic field
                hx = 2.0 * (M.m_x * (0.5f - q2q2 - q3q3) + M.m_y * (q1q2 - q0q3) + M.m_z * (q1q3 + q0q2));
                hy = 2.0 * (M.m_x * (q1q2 + q0q3) + M.m_y * (0.5f - q1q1 - q3q3) + M.m_z * (q2q3 - q0q1));
                bx = Math.Sqrt(hx * hx + hy * hy);
                bz = 2.0 * (M.m_x * (q1q3 - q0q2) + M.m_y * (q2q3 + q0q1) + M.m_z * (0.5f - q1q1 - q2q2));

                // Estimated direction of gravity and magnetic field
                halfvx = q1q3 - q0q2;
                halfvy = q0q1 + q2q3;
                halfvz = q0q0 - 0.5f + q3q3;
                halfwx = bx * (0.5f - q2q2 - q3q3) + bz * (q1q3 - q0q2);
                halfwy = bx * (q1q2 - q0q3) + bz * (q0q1 + q2q3);
                halfwz = bx * (q0q2 + q1q3) + bz * (0.5f - q1q1 - q2q2);

                // Error is sum of cross product between estimated direction and measured direction of field vectors
                halfex = (A.m_y * halfvz - A.m_z * halfvy) + (M.m_y * halfwz - M.m_z * halfwy);
                halfey = (A.m_z * halfvx - A.m_x * halfvz) + (M.m_z * halfwx - M.m_x * halfwz);
                halfez = (A.m_x * halfvy - A.m_y * halfvx) + (M.m_x * halfwy - M.m_y * halfwx);

                // Compute and apply integral feedback if enabled
                if (twoKi > 0.0)
                {
                    integralFBx += twoKi * halfex * m_samplingPeriodSecs;   // integral error scaled by Ki
                    integralFBy += twoKi * halfey * m_samplingPeriodSecs;
                    integralFBz += twoKi * halfez * m_samplingPeriodSecs;
                    G.m_x += integralFBx;   // apply integral feedback
                    G.m_y += integralFBy;
                    G.m_z += integralFBz;
                }
                else
                {
                    integralFBx = 0.0;  // prevent integral windup
                    integralFBy = 0.0;
                    integralFBz = 0.0;
                }

                // Apply proportional feedback
                G.m_x += twoKp * halfex;
                G.m_y += twoKp * halfey;
                G.m_z += twoKp * halfez;
            }

            // Integrate rate of change of quaternion
            G *= 0.5f * m_samplingPeriodSecs;       // pre-multiply common factors

            qa = m_Q.m_q1;
            qb = m_Q.m_q2;
            qc = m_Q.m_q3;
            m_Q.m_q1 += (-qb * G.m_x - qc * G.m_y - m_Q.m_q4 * G.m_z);
            m_Q.m_q2 += (qa * G.m_x + qc * G.m_z - m_Q.m_q4 * G.m_y);
            m_Q.m_q3 += (qa * G.m_y - qb * G.m_z + m_Q.m_q4 * G.m_x);
            m_Q.m_q4 += (qa * G.m_z + qb * G.m_y - qc * G.m_x);

            // Normalise quaternion
            m_Q.Normlize();
        }

        void MahonyAHRSupdateIMU(CVector Gin, CVector Ain)
        {
            double halfvx, halfvy, halfvz;
            double halfex, halfey, halfez;
            CVector G = new CVector(Gin);
            CVector A = new CVector(Ain);
            // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
            if (IsVectorValid(A))
            {

                // Normalise accelerometer measurement
                A.Normlize();

                // Estimated direction of gravity and vector perpendicular to magnetic flux
                halfvx = m_Q.m_q2 * m_Q.m_q4 - m_Q.m_q1 * m_Q.m_q3;
                halfvy = m_Q.m_q1 * m_Q.m_q2 + m_Q.m_q3 * m_Q.m_q4;
                halfvz = m_Q.m_q1 * m_Q.m_q1 - 0.5f + m_Q.m_q4 * m_Q.m_q4;

                // Error is sum of cross product between estimated and measured direction of gravity
                halfex = (A.m_y * halfvz - A.m_z * halfvy);
                halfey = (A.m_z * halfvx - A.m_x * halfvz);
                halfez = (A.m_x * halfvy - A.m_y * halfvx);

                // Compute and apply integral feedback if enabled
                if (twoKi > 0.0f)
                {
                    integralFBx += twoKi * halfex * m_samplingPeriodSecs;   // integral error scaled by Ki
                    integralFBy += twoKi * halfey * m_samplingPeriodSecs;
                    integralFBz += twoKi * halfez * m_samplingPeriodSecs;
                    G.m_x += integralFBx;   // apply integral feedback
                    G.m_y += integralFBy;
                    G.m_z += integralFBz;
                }
                else
                {
                    integralFBx = 0.0f; // prevent integral windup
                    integralFBy = 0.0f;
                    integralFBz = 0.0f;
                }

                // Apply proportional feedback
                G.m_x += twoKp * halfex;
                G.m_y += twoKp * halfey;
                G.m_z += twoKp * halfez;
            }

            // Integrate rate of change of quaternion
            G *= 0.5f * m_samplingPeriodSecs;       // pre-multiply common factors

            CQuaternion Qtemp = m_Q;

            m_Q.m_q1 += (-Qtemp.m_q2 * G.m_x - Qtemp.m_q3 * G.m_y - Qtemp.m_q4 * G.m_z);
            m_Q.m_q2 += (Qtemp.m_q1 * G.m_x + Qtemp.m_q3 * G.m_z - Qtemp.m_q4 * G.m_y);
            m_Q.m_q3 += (Qtemp.m_q1 * G.m_y - Qtemp.m_q2 * G.m_z + Qtemp.m_q4 * G.m_x);
            m_Q.m_q4 += (Qtemp.m_q1 * G.m_z + Qtemp.m_q2 * G.m_y - Qtemp.m_q3 * G.m_x);

            m_Q.Normlize();

        }

        void Init()
        {
            LOG.I("Init");
            m_state = eState.STATE_INIT;
        }

        public int ProcessPacket(SpPacket spPkt, ref CUIControlsState UICtrlState)
        {
            /*
                process a packet containging axis data
                #gr: 7-May-14
                Extracting dynamic acc from raw Acc/Gyr data:
                - Rest Calibration: detect Rest (sizeof A=g, "no gyro signal") - get the direction of G
                - on movemennt, keep track of G (using gyro) and subtr from A (assume 1g=16K)
                - get dynamic A: integrate to get velocity
                - gyro drift correction: on each rest we can do it again,
                    OR (if no rest for us) : use gradual compensation:
                        - averaging A over time should give the static A (G), so we can use it to correct G' towards G
            */
            //if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU))) {
            //	return S_FALSE;
            //}
            //const PktDataIMU& pktData = PKT_DATA(spPkt, typeof(PktDataIMU));

            int rc;

            m_pktProcessId++;

            // SM
            while (true)
            {
                switch (m_state)
                {
                    case eState.STATE_INIT:
                        // init the system
                        if (S_NEW_STATE == (rc = Init_OnIMUData(spPkt)))
                        {
                            continue; // run again
                        }
                        return rc;
                    #region forCalib
                    //case eState.STATE_INIT_CALIB:
                    //    // a new calib started
                    //    if (S_NEW_STATE == (rc = InitCalib_OnIMUData(spPkt)))
                    //    {
                    //        continue; // run again
                    //    }
                    //    return rc;
                    //case eState.STATE_CALIB:
                    //    // calibrate the system
                    //    if (S_NEW_STATE == (rc = Calib_OnIMUData(spPkt)))
                    //    {
                    //        continue; // run again
                    //    }
                    //    return rc;
                    //case eState.STATE_CALIB_DONE:
                    //    if (S_NEW_STATE == (rc = Calib_OnDone()))
                    //    {
                    //        continue; // run again
                    //    }
                    //    return rc;
                    //case eState.STATE_INIT_COMPASS_CALIB:
                    //    // a new compass calib started
                    //    if (S_NEW_STATE == (rc = InitCompassCalib_OnIMUData(spPkt)))
                    //    {
                    //        continue; // run again
                    //    }
                    //    return rc;
                    //case eState.STATE_COMPASS_CALIB:
                    //    // calibrate the system
                    //    if (S_NEW_STATE == (rc = CompassCalib_OnIMUData(spPkt)))
                    //    {
                    //        continue; // run again
                    //    }
                    //    return rc;
                    //case eState.STATE_COMPASS_CALIB_DONE:
                    //    if (S_NEW_STATE == (rc = CompassCalib_OnDone()))
                    //    {
                    //        continue; // run again
                    //    }
                    //    return rc;
                    #endregion
                    case eState.STATE_INIT_SESS:
                        if (S_NEW_STATE == (rc = InitSess_OnIMUData(spPkt)))
                        {
                            continue; // run again
                        }
                        return rc;
                    case eState.STATE_SESSION:
                        // run: get raw data and produce the state of the UI
                        if (S_NEW_STATE == (rc = Session_OnIMUData(spPkt, ref UICtrlState)))
                        {
                            continue; // run again
                        }
                        return rc;
                    default:
                        throw new Exception("Never get here");
                }
            }

            //return S_OK;
        }

        int Init_OnIMUData(SpPacket spPkt)
        {
            /*
                init the system
            */

            m_aFiltLP = new CVecExpFilter(ACC_GYRO_LP_CONST, FILTER_LPASS);
            m_gFiltLP = new CVecExpFilter(ACC_GYRO_LP_CONST, FILTER_LPASS);

            LOG.I("current gyro offsets: m_gyroOffset=" + m_gyroOffset);

            // go to next state
            return NEXT_STATE(eState.STATE_INIT_SESS);
        }

        PktDataIMU PKT_DATA_PTR(Packet spPkt, Type type)
        {
            if (spPkt.Data != null && spPkt.Data.GetType() == type)
                return spPkt.Data as PktDataIMU;
            return new PktDataIMU(false);
        }
        int InitSess_OnIMUData(SpPacket spPkt)
        {
            if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU)))
            {
                return S_OK;
            }
            BalanceQ_Init();
            m_prevPkt = spPkt.Duplicate();
            m_bGyroXNegSat = false;
            m_bGyroYNegSat = false;
            m_bGyroZNegSat = false;
            m_pktProcessId = 0;
            m_motionState = eMotionState.MOTSTATE_REST;
            return NEXT_STATE(eState.STATE_SESSION); // go to next state
        }

        void BalanceQ_Init()
        {
            // start collecting A to balance Q
            m_nBalanceQPkts = -1;
        }

        //int	BalanceQ_OnIMUData(SpPacket spPkt)
        //{
        //	/*
        //	rotate Q to reflect current orientation
        //	- we need here only static A with no dynamic components
        //	- therefore we collect and average A, and suppose we are AT REST
        //	- when we have enough data, we do the actual rotation (if not out of rest before that)
        //	*/
        //	static unique_static<CVector>	Asum;
        //	static unique_static<vector<CVector>> Avals;
        //
        //	if (m_nBalanceQPkts > INITSESS_PKT_NUM) {
        //		return S_FALSE;
        //	}
        //
        //	if (m_nBalanceQPkts == 0) {
        //		Asum.VAR = CVector();
        //		Avals.VAR.clear();
        //	}
        //	else
        //	if (m_nBalanceQPkts == INITSESS_PKT_NUM) {
        //		// A data collectedt, estimate static A
        //
        //		Asum.VAR /= INITSESS_PKT_NUM;
        //
        //		m_Q.RotateToEarthFrame(Asum.VAR);
        //		//Beep(1000,30);
        //		m_nBalanceQPkts++;
        //		return S_OK;
        //	} 
        //	// collect A
        //	CVector	A, G;
        //	GetAGFromPacket(PKT_DATA_PTR(spPkt, typeof(PktDataIMU)), A, G);
        //	Asum.VAR += A;
        //	m_nBalanceQPkts++;
        //
        //	/*
        //	Avals.push_back(A);
        //	if (m_nBalanceQPkts>5) {
        //		CVector	Aaver = Asum / m_nBalanceQPkts;
        //		CVector var;
        //		for_each(Avals.begin(), Avals.end(), [=, &var](CVector a){
        //			var += (a - Aaver).abs();
        //		});
        //		var /= m_nBalanceQPkts;
        //
        //		DrawNextVal(1, var.m_x);
        //
        //		if (var < 0.003) {
        //			Beep(2000, 20);
        //		}
        //	}*/
        //	return S_OK;
        //}
        //

        static CVector offsetG = new CVector();
        int Calib_OnIMUData(SpPacket spPkt)
        {
            /*
                calibrate the system
            */

            //CalcSamplingRate();

            if (m_nCalibPkts == 0)
            {

                LOG.I("Calib_OnIMUData: start of calibration");

                offsetG = new CVector();

            }
            else if (m_nCalibPkts >= cvals.n_gyro_calib_pkts)
            {

                LOG.I("Calib_OnIMUData: end of calibration");

                m_gyroOffset.m_x = offsetG.m_x / cvals.n_gyro_calib_pkts;
                m_gyroOffset.m_y = offsetG.m_y / cvals.n_gyro_calib_pkts;
                m_gyroOffset.m_z = offsetG.m_z / cvals.n_gyro_calib_pkts;

                LOG.I(LOG.ARG(() => m_gyroOffset));

                return NEXT_STATE(eState.STATE_CALIB_DONE); // go to next state
            }

            if ((DateTime.Now - m_calibStart).TotalMilliseconds > cvals.calibration_timeout_msec)
            {
                LOG.E("IMU Calib ended in timeout");
                m_calibResult = E_FAIL;
                return NEXT_STATE(eState.STATE_CALIB_DONE); // go to next state
            }

            // don't process non-IMU packets beyond here
            if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU)))
            {
                LOG.I("a non-IMU packet");
                return S_OK;
            }

            // else - calibrate...
            m_nCalibPkts++;

            // gyroscope vals must be in radian units 
            CVector A = new CVector(), G = new CVector(), M = new CVector();
            GetAGMFromPacket(PKT_DATA_PTR(spPkt, typeof(PktDataIMU)), ref A, ref G, ref M);

            // accomulate G data
            offsetG += G;

            //LOGV("accomulate G: GX=" + G.m_x + " GY=" + G.m_y + " GZ=" + G.m_z);

            Calib_ReportProgress(m_nCalibPkts, cvals.n_gyro_calib_pkts);

            return S_OK;
        }
        static double ShortToRadValue(short val)
        {
            return (double)val / GYRO_DEGSEC_VAL * DEG_TO_GRAD_FACT;
        }
        short RadToShortValue(double val)
        {
            double valToBecomeShort = val * GYRO_DEGSEC_VAL / DEG_TO_GRAD_FACT;
            short valAsShort = (short)(valToBecomeShort);
            if (valToBecomeShort >= short.MaxValue ||
                valToBecomeShort <= short.MinValue)
            {
                throw new Exception("cannot convert radian value to short");
            }
            return valAsShort;
        }
        static public bool IsIMUCalibValid()
        {
            // to be valid, it must be non-zero
            return !m_gyroOffset.IsDefault();
        }
        static public void SaveGyroOffsetIntoRam(short offsetX, short offsetY, short offsetZ)
        {//{m_x=-0.054225114508325817 m_y=-0.0017320061145165494 m_z=-0.055690658143685977 }
            m_gyroOffset.m_x = ShortToRadValue(offsetX);
            m_gyroOffset.m_y = ShortToRadValue(offsetY);
            m_gyroOffset.m_z = ShortToRadValue(offsetZ);
        }

        void LoadGyroOffsetFromRam(ref short offsetX, ref short offsetY, ref short offsetZ)
        {
            offsetX = RadToShortValue(m_gyroOffset.m_x);
            offsetY = RadToShortValue(m_gyroOffset.m_y);
            offsetZ = RadToShortValue(m_gyroOffset.m_z);
        }

        static CVector min = new CVector();
        static CVector max = new CVector();
        int CompassCalib_OnIMUData(SpPacket spPkt)
        {
            /*
                calibrate the compass
            */

            if ((DateTime.Now - m_calibStart).TotalMilliseconds > cvals.compass_calibration_timeout_msec)
            {
                LOG.E("Compass Calib ended in timeout. " + LOG.ARG(() => m_nCalibPkts));
                m_calibResult = E_FAIL;
                return NEXT_STATE(eState.STATE_COMPASS_CALIB_DONE); // go to next state
            }

            // don't process non-IMU packets 
            if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU)))
            {
                return S_OK;
            }

            if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU)).HasMagnetoData())
            {
                // some packets may not carry magneto, it's fine with us
                return S_OK;
            }

            if (m_nCalibPkts == 0)
            {

                const double maxDbl = double.MaxValue;
                min = new CVector(maxDbl, maxDbl, maxDbl);
                max = new CVector(-maxDbl, -maxDbl, -maxDbl);

                LOG.I("CompassCalib_OnIMUData: start of calibration");

                //// some packets may not carry magneto, it's fine with us
                //if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU))->HasMagnetoData()) {
                //	LOG.E("CompassCalib_OnIMUData: no magneto data, abort calibration");
                //	m_calibResult = E_FAIL;
                //	return NEXT_STATE(STATE_CALIB_DONE);
                //}

            }
            else if (m_nCalibPkts > cvals.n_magneto_calib_max_pkts)
            {
                #region magnetometer related
                //for (int i = 0; i <= 2; i++)
                //{
                //    LOG.I("axis " + i + ", min=" + min[i] + ", max=" + max[i]);
                //    REP_VARS.m_IMUCalibData.m_offsetM[i] = (max[i] + min[i]) / 2.0;
                //}

                //LOG.I("CompassCalib_OnIMUData: end of calibration, m_offsetM=" + REP_VARS.m_IMUCalibData.m_offsetM);
                #endregion
                return NEXT_STATE(eState.STATE_CALIB_DONE); // go to next state
            }

            //// don't process non-IMU packets beyond here
            //if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU))) {
            //	LOGV("non-IMU packet");
            //	return S_OK;
            //}

            // else - calibrate...
            m_nCalibPkts++;

            // gyroscope vals must be in radian units 
            CVector A = new CVector(), G = new CVector(), M = new CVector();
            GetAGMFromPacket(PKT_DATA_PTR(spPkt, typeof(PktDataIMU)), ref A, ref G, ref M);

            // M data
            LOG.I("Magneto values: MX=" + M.m_x + " MY=" + M.m_y + " MZ=" + M.m_z + " (num pkts=" + m_nCalibPkts + ")");

            // calculate min/max for given axes
            for (int i = 0; i <= 2; i++)
            {
                min[i] = Math.Min(min[i], M[i]);
                max[i] = Math.Max(max[i], M[i]);
            }

            Calib_ReportProgress(m_nCalibPkts, cvals.n_magneto_calib_max_pkts);

            return S_OK;
        }

        void Calib_ReportProgress(int currentN, int maxN)
        {
            //LOGV("Calib_ReportProgress");
            //if (m_calibStateCB)
            //{
            //    // create progress in cvals.calibration_progress_resolution % resolution
            //    int progress = ((currentN * 100 / maxN) / cvals.calibration_progress_resolution + 1) * cvals.calibration_progress_resolution;
            //    if (progress > m_calibProgress)
            //    {
            //        LOGV("Calib_ReportProgress before" + LOG.ARG(() => currentN));
            //        m_calibProgress = progress;
            //        m_calibStateCB(API_PROGRESS, m_calibProgress);
            //        LOGV("Calib_ReportProgress after" + LOG.ARG(() => currentN));
            //    }
            //}
        }

        int Calib_OnDone()
        {

            m_state = eState.STATE_INIT_SESS;

            // report to our owner
            #region calib
            //if (m_pOwnerThi)
            //{
            //    m_pOwnerThi->OnIMUCalibrationDone();
            //}
            //else
            //{
            //    LOGW("Owner Thimble not set");
            //}

            //if (m_calibStateCB) m_calibStateCB(m_calibResult, -1);
            #endregion
            return S_NEW_STATE; // go to next state
        }

        int CompassCalib_OnDone()
        {

            //m_state = eState.STATE_INIT_SESS;

            //if (m_calibStateCB) m_calibStateCB(m_calibResult, -1);

            return S_NEW_STATE; // go to next state
        }

        int ResetMotionParams()
        {
            m_velocity = new CVector();

            // reset the filters
            m_aFiltLP.InitSession();
            m_gFiltLP.InitSession();
            return S_OK;
        }
        CExpFilter m_sampleIntervFiltLP = new CExpFilter(0.002, FILTER_LPASS);
        //int count = 0;

        int CalcSamplingRate()
        {
            if (g_lastInputTime == new DateTime())
            {
                g_lastInputTime = DateTime.Now;
                // init the LP to expected value
                m_sampleIntervFiltLP.OnInput(1.0 / IMU_SAMPLE_RATE_HZ);
                return E_FAIL;
            }
            DateTime curTime = DateTime.Now;
            TimeSpan timeDiff;
            // this diff is highly irregular , must LP it
            timeDiff = curTime - g_lastInputTime;
            double timeDiffSecs = timeDiff.TotalMilliseconds / 1000.0;
            g_lastInputTime = curTime;
            if (timeDiffSecs > 1)
            {
                return S_FALSE;
            }
            if (timeDiffSecs > .1)
            {
                return S_OK;
            }
            m_samplingPeriodSecs = m_sampleIntervFiltLP.OnInput(timeDiffSecs);
            //           count = 0;
            return S_OK;
        }

        static int SeqNumCycDiff(int seq1, int seq2)
        {
            // return seq1 - seq2
            // - guarantied to be positive
            if (seq1 < seq2)
            { // handle wrap-around
                seq1 += 0x100; // from CLI_PKT_HDR_SEQ__BSIZE;
            }
            return seq1 - seq2;
        }

        static double m_IMU_lpfilter_XY_alpha = 0.3;
        // smooth out hand vibrations >> use 2nd order LP
        CExpFilter fLPx = new CExpFilter(m_IMU_lpfilter_XY_alpha, FILTER_LPASS);
        CExpFilter fLPy = new CExpFilter(m_IMU_lpfilter_XY_alpha, FILTER_LPASS);
        CExpFilter fLPx2 = new CExpFilter(m_IMU_lpfilter_XY_alpha, FILTER_LPASS);
        CExpFilter fLPy2 = new CExpFilter(m_IMU_lpfilter_XY_alpha, FILTER_LPASS);

        int Session_OnIMUData(SpPacket spPkt, ref CUIControlsState UICtrlState)
        {
            /*
                inside a session; process a new IMU packet
                - if we've lost packets, reconst the lost ones (via extrapulation)
            */
            // don't process non-IMU packets here
            if (!PKT_DATA_PTR(spPkt, typeof(PktDataIMU)))
            {
                return S_OK;
            }
            //LOGV("Session_OnIMUData");
            int rc;
            PktDataIMU pktData = PKT_DATA_PTR(spPkt, typeof(PktDataIMU));
            PktDataIMU prevPktData = PKT_DATA_PTR(m_prevPkt, typeof(PktDataIMU));
            if ((rc = CalcSamplingRate()) < 0)
            {
                LOG.I("first time ever - init" + spPkt);
                //CVector A, G;
                //GetAGFromPacket(pktData, A, G);
                //m_Q.RotateToEarthFrame(A); no need; this depends on the first data only - let it rotate by the algorithm
                m_state = eState.STATE_INIT_SESS;//SetSM_InitSession();
                return E_FAIL;
            }
            else
            if (rc == S_FALSE)
            {
                LOG.I("a new session");
                CVector A = new CVector(), G = new CVector(), M = new CVector();
                GetAGMFromPacket(pktData, ref A, ref G, ref M);
                m_Q.RotateToEarthFrame(A);
                m_state = eState.STATE_INIT_SESS;//SetSM_InitSession();
            }

            // any loses?
            int seqNum = spPkt.Head.GetSeqNum();
            int prevSeqNum = m_prevPkt.Head.GetSeqNum();

            int lostPkts = seqNum == prevSeqNum ? 0 : SeqNumCycDiff(seqNum, prevSeqNum) - 1;

            if (lostPkts > MAX_ALLOWED_RECONST_PKT_N)
            {
                //LOG.I("too many losses, restart session");
                //		Beep(1000,50);
                return NEXT_STATE(eState.STATE_INIT_SESS); // go to next state
            }

            if (lostPkts != 0)
            {
                // process last known packet again
                // - create an averaged packet and use it to interpolate values
                PktData averPkt = PktDataIMU.AverageTo(prevPktData, pktData);
                while ((lostPkts--) > 0)
                {
                    Session_OnAxisPacket((PktDataIMU)averPkt, ref UICtrlState);
                    //LOGV("IMU  lost" + LOG.ARG(() => seqNum) + LOG.ARG(() => prevSeqNum) + LOG.ARG(() => averPkt));
                }
            }

            // process current packet 
            Session_OnAxisPacket(pktData, ref UICtrlState);

            m_prevPkt = spPkt.Duplicate();

            return S_OK;
        }

        void swap(ref double d1, ref double d2)
        {
            double d = d1;
            d1 = d2;
            d2 = d;
        }
        CVecExpFilter filtG = new CVecExpFilter(0.2, FILTER_LPASS);
        DateTime m_logTime = DateTime.Now;
        int Session_OnAxisPacket(PktDataIMU spPktIMU, ref CUIControlsState UICtrlState)
        {
            /*
                process an IMU packet
                - packets here may be original or reconst'd (via extrapulation)
            */
            CVector staticA = new CVector(), G = new CVector(), M = new CVector();
            CVector currAccel = new CVector();
            //LOGV("Session_OnAxisPacket");

            GetAGMFromPacket(spPktIMU, ref staticA, ref G, ref M);

            ApplyAGMCalibration(ref staticA, ref G, ref M);

            // get an updated quaternion
            MahonyAHRSupdate(G, staticA, M);

            // rotate our accel vector to earth frame
            CVector staticArot = m_Q.RotateVec(staticA);

            // Calculate dynamic linear acceleration in Earth frame (subtracting gravity)
            CVector gravInEarth = new CVector(0.0, 0.0, 1.0);
            CVector linearA = staticArot - gravInEarth;

            CVector Glp = m_gFiltLP.OnInput(G);

            // Motion SM
            bool bRunSM = true;
            while (bRunSM)
            {
                bRunSM = false; // leave SM unless requested otherwise
                switch (m_motionState)
                {
                    case eMotionState.MOTSTATE_GOTO_REST:
                        // device at rest
                        ResetMotionParams();
                        // start balancing of Q
                        //BalanceQ_Init();

                        // goto next state now
                        m_motionState = eMotionState.MOTSTATE_REST;
                        bRunSM = true;
                        break;
                    case eMotionState.MOTSTATE_REST:
                        if (CalcDevState(ref linearA, ref G) != eDevState.DEVSTATE_ATREST)
                        {
                            // goto next state now
                            m_motionState = eMotionState.MOTSTATE_GOTO_MOTION;
                            bRunSM = true;
                            break;
                        }
                        //BalanceQ_OnIMUData(pkt);

                        break;
                    case eMotionState.MOTSTATE_GOTO_MOTION:
                        // just started motion after rest
                        /* 
                            Assess missed acc data based on linear extrap. to zero
                            - count aver of first known acc point and zero, times the data point we've missed
                                (curr_accel + 0) / 2  * missed_data_points
                                missed_data_points = N_PKTS_ATREST / 2
                        */
                        currAccel = linearA * (GRAVITY_MSS * m_samplingPeriodSecs);
                        m_velocity = currAccel * (N_PKTS_ATREST / 2 / 2);

                        m_motionState = eMotionState.MOTSTATE_MOTION; // on next pkt goto motion
                        break;
                    case eMotionState.MOTSTATE_MOTION:
                        // are we in motion or rest?
                        if (CalcDevState(ref linearA, ref G) == eDevState.DEVSTATE_ATREST)
                        {
                            // device is at rest, go to rest state
                            m_motionState = eMotionState.MOTSTATE_GOTO_REST;
                            bRunSM = true;
                            break;
                        }
                        // we are currently in motion 
                        if (CalcDevState(ref linearA, ref G) == eDevState.DEVSTATE_CONST_MOTION)
                        {
                            // device is at constant speed, give accel measurment less weight
                            m_velocity += linearA * (GRAVITY_MSS * m_samplingPeriodSecs / 3.0f);
                            /// BalanceQ_OnIMUData(pkt); >> not really working, A is not pointing to g, and needs a lot of summing - 100 - to run well, which is already too late for us
                            break;
                        }
                        if (CalcDevState(ref linearA, ref G) == eDevState.DEVSTATE_ACCEL_MOTION)
                        {

                            //BalanceQ_Init();
                            // device is accelerting/deceleraing
                            // Calculate linear velocity (integrate acceleartion): convert from 'g' to m/s/s
                            m_velocity += linearA * (GRAVITY_MSS * m_samplingPeriodSecs);
                            // cross-check dir of movment with gyro
                            if (m_velocity.m_x * Glp.m_y < 0)
                            {
                                //m_velocity.m_x *= 0.8;
                            }
                            break;
                        }
                        break;
                    default:
                        throw new Exception("never get here");
                }
            }


            //UICtrlState.m_trans.m_x = -(int)fLPx2.OnInput(fLPx.OnInput(G.m_z *m_samplingPeriodSecs));
            //UICtrlState.m_trans.m_y = -(int)fLPy2.OnInput(fLPy.OnInput(G.m_y *m_samplingPeriodSecs));

            //UICtrlState.m_angularVelocity.m_x = -G.m_z;
            //UICtrlState.m_angularVelocity.m_y = G.m_x;
            UICtrlState.m_angularVelocity = G;

            //UICtrlState.m_trans_dz = int(velx * REP_VARS.m_IMU_Z_sens);
            //UICtrlState.m_trans_dy = int(velx * REP_VARS.m_IMU_Z_sens); 


            // save bird quaternion
            UICtrlState.m_orientQ = new CQuaternion(m_Q); 

            /* 
                rotate to WPF 3D model coordinates: 
                - assume Bird's nose is toward the positive X axis (below: facing the viewer)
                - we need this 'cause the Bird has different coors system assumption (see in thilogic)

                                    Y
                                    |
                                    |
                         Z _________|
                                     \
                                      \
                                       X

            */

            swap(ref UICtrlState.m_orientQ.m_q3, ref UICtrlState.m_orientQ.m_q2);
            swap(ref UICtrlState.m_orientQ.m_q4, ref UICtrlState.m_orientQ.m_q3);
            UICtrlState.m_orientQ.m_q3 = -UICtrlState.m_orientQ.m_q3;

            UICtrlState.m_orientQ = UICtrlState.m_orientQ.AsConjugate(); // make m_orientQ from world viewpoint
            //LOGV("UICtrlState.m_orientQ = " + UICtrlState.m_orientQ.m_q1 + ", " + UICtrlState.m_orientQ.m_q2 + ", " + UICtrlState.m_orientQ.m_q3 + ", " + UICtrlState.m_orientQ.m_q4);

            // save bird orientation as vector (direction of G)
            //UICtrlState.m_orient = m_Q.AsConjugate().RotateVec(gravInEarth);
            CQuaternion qTmp = m_Q.AsConjugate();
            CVector vTmp = qTmp.RotateVec(gravInEarth);
            UICtrlState.m_orient = vTmp;
            //if (ElapsedAsMSec(m_logTime) > 500)
            //{
            //    m_logTime = DateTime.Now;
            //    LOG.N("UICtrlState.m_orient: " + UICtrlState.m_orient.ToString(), LogLines.IMU);
            //}
            // save bird orientation as PRY
            // - use the well-known equation to convert to Euler 
            // https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
            var pitchAngle = (float)(-Math.Asin(-2.0 * (m_Q.m_q1 * m_Q.m_q3 - m_Q.m_q4 * m_Q.m_q2)) / cvals.rad2PI * 360.0);
            var rollAngle = (float)(-Math.Atan2(2.0 * (m_Q.m_q1 * m_Q.m_q2 + m_Q.m_q4 * m_Q.m_q3), m_Q.m_q4 * m_Q.m_q4 + m_Q.m_q1 * m_Q.m_q1 - m_Q.m_q2 * m_Q.m_q2 - m_Q.m_q3 * m_Q.m_q3) / cvals.rad2PI * 360.0);
            var yawAngle = (float)(-Math.Atan2(2.0 * (m_Q.m_q2 * m_Q.m_q3 + m_Q.m_q4 * m_Q.m_q1), m_Q.m_q4 * m_Q.m_q4 - m_Q.m_q1 * m_Q.m_q1 - m_Q.m_q2 * m_Q.m_q2 + m_Q.m_q3 * m_Q.m_q3) / cvals.rad2PI * 360.0);
            // rotate around the X axis 90 deg to match Bird position
            UICtrlState.m_orientPRY.pitchAngle = rollAngle;
            UICtrlState.m_orientPRY.rollAngle = -pitchAngle;
            UICtrlState.m_orientPRY.yawAngle = yawAngle;
            // rotate around the Y axis 180 deg to start at 0 deg
            UICtrlState.m_orientPRY.yawAngle = yawAngle + 180;

            //LOGV(LOG.ARG(() => UICtrlState.m_orientPRY.pitchAngle) + LOG.ARG(() => UICtrlState.m_orientPRY.rollAngle) + LOG.ARG(() => UICtrlState.m_orientPRY.yawAngle));

            // set sample period
            UICtrlState.m_IMUsamplePeriodMSec = m_samplingPeriodSecs * 1000.0;

            //if (IsVectorValid(M))
            //{
            //    UICtrlState.m_CompassAzimuth = atan(M.m_y / M.m_x) / ::cvals.PI;
            //    UICtrlState.m_CompassAzimuthValid = true;
            //}

            //double magnetoX;
            //double magnetoY;
            //double magnetoZ;
            ////	if (m_magnetoAdjustValid && spPktIMU.HasMagnetoData())
            //if (spPktIMU.HasMagnetoData()) {
            //	magnetoX = 0.3*spPktIMU.GetMagnetoX()*((m_magnetoAdjustX-128.0)/256.0+1.0);
            //	magnetoY = 0.3*spPktIMU.GetMagnetoY()*((m_magnetoAdjustY-128.0)/256.0+1.0);
            //	magnetoZ = 0.3*spPktIMU.GetMagnetoZ()*((m_magnetoAdjustZ-128.0)/256.0+1.0);
            //	UICtrlState.m_CompassAzimuth = atan(magnetoY/magnetoX)/3.14159265358979323846;
            //	UICtrlState.m_CompassAzimuthValid = true;
            //} else {
            //	magnetoX = 0;
            //	magnetoY = 0;
            //	magnetoZ = 0;
            //	UICtrlState.m_CompassAzimuth = 0;
            //	UICtrlState.m_CompassAzimuthValid = false;
            //}

            #region _DEBUG

            //            //// this section not thread/object-safe

            //            //DrawNextVal(0, UICtrlState.m_rot_v.m_y);
            //            //DrawNextVal(1, velx);
            //            static double sumGx = 0;
            //            sumGx += G.m_x * m_samplingPeriodSecs;
            //            static double sumGy = 0;
            //            sumGy += G.m_y * m_samplingPeriodSecs;
            //            static double sumGz = 0;
            //            sumGz += G.m_z * m_samplingPeriodSecs;
            //            //DrawNextVal(0, Glp.m_y);
            //            //DrawNextVal(2, m_translation.m_x);
            //            //DrawNextVal(0, m_velocity.m_y);

            //            static int iShow = 0;
            //            int averNum = 10;
            //            static CVector A_aver;
            //            static CVector staticA_aver;
            //            static CVector G_aver;
            //            static CVector translation_aver;
            //            static CVector velocity_aver;
            //            static CVector orient_aver;
            //            iShow++;
            //            A_aver += linearA;
            //            staticA_aver += staticA;
            //            G_aver += G;
            //            //G_aver += UICtrlState.m_rot_v;
            //            translation_aver += m_translation;
            //            velocity_aver += UICtrlState.m_velocity;
            //            orient_aver += UICtrlState.m_orient;

            //            if (0 != (iShow % averNum))
            //            {
            //                return 0;
            //            }

            //            A_aver /= averNum;
            //            staticA_aver /= averNum;
            //            G_aver /= averNum;
            //            translation_aver /= averNum;
            //            velocity_aver /= averNum;
            //            orient_aver /= averNum;

            //            MotionDebugHandleAcclAndGyroAxes(staticA_aver.m_x,
            //                staticA_aver.m_y,
            //                staticA_aver.m_z,
            //                A_aver.m_x,
            //                A_aver.m_y,
            //                A_aver.m_z,
            //                G_aver.m_x,
            //                G_aver.m_y,
            //                G_aver.m_z,
            //                orient_aver.m_x,
            //                orient_aver.m_y,
            //                orient_aver.m_z,
            //                m_Q.m_q1,
            //                m_Q.m_q2,
            //                m_Q.m_q3,
            //                m_Q.m_q4,
            //                translation_aver.m_x,
            //                translation_aver.m_y,
            //                translation_aver.m_z,
            //                velocity_aver.m_x,
            //                velocity_aver.m_y,
            //                velocity_aver.m_z);

            //            if (UICtrlState.m_CompassAzimuthValid)
            //            {
            //                if (IsVectorValid(M))
            //                {
            //                    MotionDebugHandleCmpsAxes(M.m_x, M.m_y, M.m_z);
            //                }
            //            }

            //            A_aver.Init();
            //            staticA_aver.Init();
            //            G_aver.Init();
            //            translation_aver.Init();
            //            velocity_aver.Init();

            #endregion // DEBUG

            return S_OK;
        }

        void SetCompassAdjustmentValues(uint x, uint y, uint z)
        {
            LOG.I("SetCompassAdjustmentValues: x=" + x + ", y=" + y + ", z=" + z);
            //REP_VARS.m_IMUCalibData.m_adjustM.m_x = x;
            //REP_VARS.m_IMUCalibData.m_adjustM.m_y = y;
            //REP_VARS.m_IMUCalibData.m_adjustM.m_z = z;
            //MotionDebugHandleCmpsAdjustment(x, y, z);
        }

        uint lastPktProcessId = 0;
        eDevState rc = eDevState.DEVSTATE_INVALID;
        CExpFilter filtHP = new CExpFilter(0.02, FILTER_HPASS);
        CExpFilter filtLP = new CExpFilter(0.25, FILTER_LPASS);
        CVecExpFilter filtLP_G = new CVecExpFilter(0.5, FILTER_LPASS); // for 200Hz
        int st_nNoMovement = N_PKTS_ATREST - 1;
        int debugCount = 1;

        eDevState CalcDevState(ref CVector pA, ref CVector pG)
        {
            /*
                find out current device state, based on latest data packet
                we can be:
                    - at rest, 
                    - const motion,
                    - accelerated motion

                test for lineaer acceleration, and rotation.
                - we need the lineaer acc, not static, since in static we cannot separate 
                the rotation from linear movement. static conatins also rotation info, lineaer not
            */

            bool bAccelRest = true;
            bool bGyroRest = true;
            bool bMovement = false;
            if (debugCount % 10 == 0)
                debugCount = 0;
            debugCount++;
            // don't process the same packet twice
            if (m_pktProcessId == lastPktProcessId)
            {
                return rc;
            }

            // save last processed pkt id
            lastPktProcessId = m_pktProcessId;

            if (pA != new CVector())
            {
                // get vect lenght
                /// float lenA = pA->Length();

                // use X only
                // - take abs, so we tread accel/decel the same way
                double lenA = Math.Abs(pA.m_x);

                // remove DC 
                var lenAhp = filtHP.OnInput(lenA);
                // create slow changing signal
                var lenAlp = filtLP.OnInput(lenAhp);
                // check for the signal's positive val threshold
                // - we detect here only the rising edge of accel/decel
                bAccelRest = lenAlp < 0.025;
            }
            if (pG != new CVector())
            {
                CVector Glp = filtLP_G.OnInput(pG);

                ///float lenGlp = Glp.Length();
                // use y only
                var lenGlp = Math.Abs(Glp.m_y);

                bGyroRest = lenGlp < 0.1;// GYRO_MAX_REST_VAL_RADSEC;
            }

            {
                var nNoMovement = st_nNoMovement; // get from static var (only in this scope)

                if (bGyroRest && bAccelRest)
                {
                    nNoMovement++;
                }
                else
                {
                    nNoMovement--;
                }

                nNoMovement = nNoMovement < 0 ? 0 : nNoMovement;
                nNoMovement = nNoMovement >= N_PKTS_ATREST ? N_PKTS_ATREST - 1 : nNoMovement;

                bMovement = nNoMovement < (N_PKTS_ATREST * 2 / 3);

                st_nNoMovement = nNoMovement; // save back to static var
            }

            /* remove for the hand demo*/
            if (!bMovement)
            {
                rc = eDevState.DEVSTATE_ATREST;
            }
            else if (bAccelRest)
            {
                rc = eDevState.DEVSTATE_CONST_MOTION;
            }
            else
            {
                rc = eDevState.DEVSTATE_ACCEL_MOTION;
            }

            //#ifdef MUVDEBUG_WIN
            //	if (bGyroRest) {
            //		SetDlgItemText(IDC_ATREST_G, "G at rest");
            //	}
            //	else {
            //		//DrawNextVal(0, 0, RGB(0, 255, 0));
            //		SetDlgItemText(IDC_ATREST_G, "-------");
            //	}
            //
            //	if (bAccelRest) {
            //		SetDlgItemText(IDC_ATRESTA, "A at rest");
            //	}
            //	else {
            //		//DrawNextVal(0, 0, RGB(0, 255, 0));
            //		SetDlgItemText(IDC_ATRESTA, "-------");
            //	}
            //
            //	if (!bMovement) {
            //		//DrawNextVal(0, 0, RGB(0, 255, 0));
            //		SetDlgItemText(IDC_ATREST, "at rest");
            //	}
            //	else {
            //		SetDlgItemText(IDC_ATREST, "-------");
            //	}
            //
            //#endif

            return rc;
        }

        int GetAGMFromPacket(PktDataIMU spPktIMU, ref CVector A, ref CVector G, ref CVector M)
        {
            // convert accel input to g units 
            A.m_x = (double)spPktIMU.GetAcceleroX() / ACC_G_VAL;
            A.m_y = (double)spPktIMU.GetAcceleroY() / ACC_G_VAL;
            A.m_z = (double)spPktIMU.GetAcceleroZ() / ACC_G_VAL;
            // convert gyro input to radians
            G.m_x = ShortToRadValue(spPktIMU.GetGyroX());
            G.m_y = ShortToRadValue(spPktIMU.GetGyroY());
            G.m_z = ShortToRadValue(spPktIMU.GetGyroZ());
            // get magneto input 
            if (spPktIMU.HasMagnetoData())
            {
                if (false)//REP_VARS.m_IMUCalibData.MagnetoAdjustValid())
                {
                    // apply adjustment values
                    // - this data comes frm the HW itself, so we apply it here before anything else
                    //LOGV("spPktIMU.GetMagnetoX()=" + spPktIMU.GetMagnetoX() + " spPktIMU.GetMagnetoY()=" + spPktIMU.GetMagnetoY() + " spPktIMU.GetMagnetoZ()=" + spPktIMU.GetMagnetoZ());
                    //M.m_x = 0.3 * spPktIMU.GetMagnetoX() * ((m_IMUCalibData.m_adjustM.m_x - 128.0) / 256.0 + 1.0);
                    //M.m_y = 0.3 * spPktIMU.GetMagnetoY() * ((m_IMUCalibData.m_adjustM.m_y - 128.0) / 256.0 + 1.0);
                    //M.m_z = 0.3 * spPktIMU.GetMagnetoZ() * ((m_IMUCalibData.m_adjustM.m_z - 128.0) / 256.0 + 1.0);
                }
                else
                {
                    // no adjustment values? take raw data - better than nothing
                    LOG.W("no magneto adjustment values; run IMU calibration");
                    M.m_x = spPktIMU.GetMagnetoX();
                    M.m_y = spPktIMU.GetMagnetoY();
                    M.m_z = spPktIMU.GetMagnetoZ();
                }
            }
            else
            {
                M = new CVector();
            }

            return S_OK;
        }

        void ApplyAGMCalibration(ref CVector A, ref CVector G, ref CVector M)
        {

            // A
            // no caibration needed

            // G
            // remove gyro offset
            //CVector tmp = G;
            //LOG.I("ApplyAGMCalibration: before=" + G.ToString());
            G = G - m_gyroOffset;
            //LOG.I("ApplyAGMCalibration: before=" + tmp.ToString() + "; after=" + G.ToString());
            // adjust for rotating
            G *= 0.9f;

            // M
            // apply offset
            //if (IsVectorValid(M))
            //{
            //    M = M - m_IMUCalibData.m_offsetM;
            //}
        }
    }
}