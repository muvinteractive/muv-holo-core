﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Gaming.Input;
using Windows.System;
using Windows.UI.Input.Preview.Injection;
using static BirdUWP.ThrowHelper;
//using UnityEngine.EventSystems;
namespace BirdUWP
{
    static class StaticDefs
    {
        public const bool FILTER_LPASS = true;
        public const bool FILTER_HPASS = false;
    }
    public class RteDebug : Exception
    {
        public RteDebug(string msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
             : base("RUNTIME_ERROR thrown at: " + fileName + ":" + lineNumber + " " + msg)
        {
        }
    }
    static class ThrowHelper
    {
        public static void EST_INVARIANT_THROW_RTE(bool expr,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            if (!expr)
                THROW_RTE("class invariant cannot be established", lineNumber, fileName);
        }
        public static void THROW_RTE(string msg = "",
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            throw new RteDebug(msg, lineNumber, fileName);
        }
        public static void EST_INVARIANT_THROW_RTE([CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            throw new RteDebug("class invariant cannot be established", lineNumber, fileName);
        }
    }
    public struct Point
    {

        public int x;
        public int y;
        public Point(int px = Int32.MaxValue, int py = Int32.MinValue)
        {
            x = px;
            y = py;
        }

        public static Point operator +(Point aPoint1, Point aPoint2)
        {
            Point resPt;
            resPt.x = aPoint1.x + aPoint2.x;
            resPt.y = aPoint1.y + aPoint2.y;
            return resPt;
        }
        public bool IsUndefined() { return x == int.MaxValue && y == int.MinValue; }
        public bool IsDefined() { return !IsUndefined(); }
        public bool IsDefault() { return x == 0 && y == 0; }
        public void Reset() { x = int.MaxValue; y = int.MinValue; }
        public static bool operator ==(Point p1, Point p2)
        {
            return p1.x == p2.x && p1.y == p2.y;
        }
        public static bool operator !=(Point p1, Point p2)
        {
            return !(p1 == p2);
        }

        public Point Rot90()
        {
            var tmpx = x;
            // rotate xy plane around 0,0
            x = y;
            y = -tmpx;
            return this;
        }
        public override string ToString()
        {
            return "x= " + x + "; y= " + y;
        }
        //    base_point(base_point<double> coors) : base_point{ (T) coors.x, (T) coors.y } { }

        //    base_point(base_point<int> coors) : base_point{ (T) coors.x, (T) coors.y } { }
        //	void Reset() { x = std::numeric_limits < T >::max(); y = std::numeric_limits < T >::lowest(); }
        //bool IsUndefined() { return !m_defined; }
        //bool IsDefined() { return !IsUndefined(); }
    }
    public struct DblPoint
    {
        public double x;
        public double y;
        public DblPoint(double px = double.MaxValue, double py = double.MinValue)
        {
            x = px;
            y = py;
        }
        public override string ToString()
        {
            return "x= " + x + "; y= " + y;
        }
        //bool m_defined = true;
        //static public void GetLimits(ref T maxValue, ref T minValue)
        //{

        //    TypeCode typeCode = Convert.GetTypeCode(x);
        //    switch (typeCode)
        //    {
        //        case TypeCode.Byte:
        //            maxValue = (T)(Object)byte.MaxValue;
        //            minValue = (T)(Object)byte.MinValue;
        //            break;
        //        case TypeCode.Char:
        //            maxValue = (T)(Object)char.MaxValue;
        //            minValue = (T)(Object)char.MinValue;
        //            break;
        //        case TypeCode.Decimal:
        //            maxValue = (T)(Object)decimal.MaxValue;
        //            minValue = (T)(Object)decimal.MinValue;
        //            break;
        //        case TypeCode.Double:
        //            maxValue = (T)(Object)decimal.MaxValue;
        //            minValue = (T)(Object)decimal.MinValue;
        //            break;
        //        case TypeCode.Int16:
        //            maxValue = (T)(Object)short.MaxValue;
        //            minValue = (T)(Object)short.MinValue;
        //            break;
        //        case TypeCode.Int32:
        //            maxValue = (T)(Object)int.MaxValue;
        //            minValue = (T)(Object)int.MinValue;
        //            break;
        //        case TypeCode.Int64:
        //            maxValue = (T)(Object)long.MaxValue;
        //            minValue = (T)(Object)long.MinValue;
        //            break;
        //        case TypeCode.SByte:
        //            maxValue = (T)(Object)sbyte.MaxValue;
        //            minValue = (T)(Object)sbyte.MinValue;
        //            break;
        //        case TypeCode.Single:
        //            maxValue = (T)(Object)float.MaxValue;
        //            minValue = (T)(Object)float.MinValue;
        //            break;
        //        case TypeCode.UInt16:
        //            maxValue = (T)(Object)ushort.MaxValue;
        //            minValue = (T)(Object)ushort.MinValue;
        //            break;
        //        case TypeCode.UInt32:
        //            maxValue = (T)(Object)uint.MaxValue;
        //            minValue = (T)(Object)uint.MinValue;
        //            break;
        //        case TypeCode.UInt64:
        //            maxValue = (T)(Object)ulong.MaxValue;
        //            minValue = (T)(Object)ulong.MinValue;
        //            break;
        //        default:
        //            maxValue = default(T);//set default value
        //            break;
        //    }
        //}
        public bool IsUndefined() { return x == double.MaxValue && y == double.MinValue; }
        public bool IsDefined() { return !IsUndefined(); }
        public bool IsDefault() { return x == 0 && y == 0; }
        public void Reset() { x = double.MaxValue; y = double.MinValue; }
        public static DblPoint operator +(DblPoint aPoint1, DblPoint aPoint2)
        {
            DblPoint resPt;
            resPt.x = aPoint1.x + aPoint2.x;
            resPt.y = aPoint1.y + aPoint2.y;
            return resPt;
        }
        DblPoint Rot90()
        {
            var tmpx = x;
            // rotate xy plane around 0,0
            x = y;
            y = -tmpx;
            return this;
        }
        //    base_point(base_point<double> coors) : base_point{ (T) coors.x, (T) coors.y } { }

        //    base_point(base_point<int> coors) : base_point{ (T) coors.x, (T) coors.y } { }
        //	void Reset() { x = std::numeric_limits < T >::max(); y = std::numeric_limits < T >::lowest(); }
        //bool IsUndefined() { return !m_defined; }
        //bool IsDefined() { return !IsUndefined(); }
    }
    //	bool operator !=(base_point  aPoint) {
    //		return !(*this == aPoint);
    //}
    //bool operator ==(base_point  aPoint) {
    //		return x == aPoint.x  y == aPoint.y;
    //	}
    //	base_point operator -= (base_point  aPoint)
    //{
    //    x -= aPoint.x;
    //    y -= aPoint.y;
    //    return *this;
    //}
    //}
    //base_point operator /= (int div)
    //{
    //    x /= div;
    //    y /= div;
    //    return *this;
    //}
    //base_point abs()
    //{
    //    x = ::abs(x);
    //    y = ::abs(y);
    //    return *this;
    //}
    //base_point max(T val)
    //{
    //    x = std::max(x, val);
    //    y = std::max(y, val);
    //    return *this;
    //}
    //base_point operator *= (base_point  aPoint)
    //{
    //    x *= aPoint.x;
    //    y *= aPoint.y;
    //    return *this;
    //}
    //base_point operator *= (T val)
    //{
    //    x *= val;
    //    y *= val;
    //    return *this;
    //}
    //base_point operator /(T div) {
    //		base_point res;
    //res.x = x / div;
    //		res.y = y / div;
    //		return res;
    //	}
    //	base_point operator *(T mult) {
    //		base_point res;
    //res.x = x* mult;
    //res.y = y* mult;
    //		return res;
    //	}

    //base_point  Transpose()
    //{
    //    std::swap(x, y);
    //    return *this;
    //}
    //base_point  operator ++(int)
    //{
    //    x++;
    //    y++;
    //    return *this;
    //}
    //base_point  operator +=(T val)
    //{
    //    x += val;
    //    y += val;
    //    return *this;
    //}
    //base_point  operator -=(T val)
    //{
    //    x -= val;
    //    y -= val;
    //    return *this;
    //}
    //base_point operator -()
    //{
    //    return{ -x, -y };
    //}
    //base_point  DevOrientRot90(T dimX, T dimY)
    //{
    //    /* device was rotated 90deg CW
    //     - top-left corner is on 0,0
    //     - rot 90deg around 0,0
    //     - translate coors so that top-left corner is on 0,0
    //     */
    //    y = -y;
    //    Rot90();
    //    y = -y;

    //    x += dimY;
    //    return *this;
    //}
    //base_point  DevOrientRot90(int timesCW, T maxX, T maxY)
    //{
    //    // rotate dev timesCW times 
    //    timesCW = 0x3; // we never need more than 3 rotations
    //    if (timesCW == 0) return *this;
    //    DevOrientRot90(maxX, maxY);
    //    return DevOrientRot90(--timesCW, maxY, maxX);
    //}

    //double Length()
    //{
    //    return sqrt(Cli::sqr(x) + Cli::sqr(y));
    //}

    //bool IsAround(base_point  aPoint, T radius) {
    //		// is this point around Apoint by radius?
    //		return (Cli::sqr(x - aPoint.x) + Cli::sqr(y - aPoint.y)) <= Cli::sqr((int) radius);
    //	}

    //	T DistFrom(base_point  aPoint) {
    //		// get distance from aPoint
    //		return static_cast<T>(sqrt(Cli::sqr(x - aPoint.x) + Cli::sqr(y - aPoint.y)));
    //	}

    //	bool IsAroundRect(base_point  aPoint, base_point distRect) {
    //		// are we within rect distance?
    //		return static_cast<T>(::abs(x - aPoint.x) < distRect.x  ::abs(y - aPoint.y) < distRect.y);
    //	}

    //	bool IsWithinRect(base_point  rect_left_top_point, base_point  rect_right_bot_point) {
    //		// is this point inside the given rect? 
    //		return
    //			x >= rect_left_top_point.x 
    //			y >= rect_left_top_point.y 
    //			x <= rect_right_bot_point.x 
    //			y <= rect_right_bot_point.y;
    //	}

    //	void LimitToRect(base_point  rect_left_top_point, base_point  rect_right_bot_point)
    //{
    //    // limit this point to given rect
    //    x = std::max(std::min(x, rect_right_bot_point.x), rect_left_top_point.x);
    //    y = std::max(std::min(y, rect_right_bot_point.y), rect_left_top_point.y);
    //}

    //bool IsNonZero() {
    //		return 	x != T{ 0 }  y != T{ 0 };
    //	}

    //	bool IsNonNeg() {
    //		return 	x >= T{ 0 }  y >= T{ 0 };
    //	}

    //	bool IsZero() {
    //		return 	!IsNonZero();
    //	}
    //	std::string ToString()
    //{
    //    std::ostrstream oss;
    //    oss << x << ' ' << y << std::ends;
    //    return oss.str();
    //}
    //void FromString(std::string str)
    //{
    //    std::istrstream iss(str.c_str());
    //    iss >> x >> y;
    //    return;
    //}

    //}


    public class CVector
    {
        public double m_x = 0;
        public double m_y = 0;
        public double m_z = 0;

        public bool IsDefault() { return Math.Abs(m_x) < 0.0001 && Math.Abs(m_y) < 0.0001 && Math.Abs(m_z) < 0.0001; }

        public CVector() { }

        public CVector(double x, double y, double z)
        {
            m_x = x;
            m_y = y;
            m_z = z;

        }

        public CVector(int x, int y, int z)
        {
            m_x = x;
            m_y = y;
            m_z = z;

        }
        public CVector(double scalar)
        {
            m_x = scalar;
            m_y = scalar;
            m_z = scalar;
        }

        public CVector(CVector vec)
        {
            m_x = vec.m_x;
            m_y = vec.m_y;
            m_z = vec.m_z;
        }
        void Init()
        {
            m_x = 0;
            m_y = 0;
            m_z = 0;
        }
        public override string ToString()
        {
            return
                "m_x = " + m_x +
                "; m_y = " + m_y +
                "; m_z = " + m_z;
        }
        public CVector Cross(CVector vec1)
        {

            CVector res = new CVector();
            res.m_x = (m_y * vec1.m_z - m_z * vec1.m_y);
            res.m_y = (m_z * vec1.m_x - m_x * vec1.m_z);
            res.m_z = (m_x * vec1.m_y - m_y * vec1.m_x);
            return res;
        }
        public double Dot(CVector vec1)
        {
            return m_x * vec1.m_x + m_y * vec1.m_y + m_z * vec1.m_z;
        }
        public double Length()
        {
            return Math.Sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
        }
        public static CVector operator -(CVector vec1, CVector vec2)
        {
            CVector res = new CVector();
            res.m_x = vec1.m_x - vec2.m_x;
            res.m_y = vec1.m_y - vec2.m_y;
            res.m_z = vec1.m_z - vec2.m_z;
            return res;
        }
        public static CVector operator +(CVector vec1, CVector vec2)
        {
            CVector res = new CVector();
            res.m_x = vec1.m_x + vec2.m_x;
            res.m_y = vec1.m_y + vec2.m_y;
            res.m_z = vec1.m_z + vec2.m_z;
            return res;
        }
        public static CVector operator *(CVector vec, double val)
        {
            CVector res = new CVector();
            res.m_x = vec.m_x * val;
            res.m_y = vec.m_y * val;
            res.m_z = vec.m_z * val;
            return res;
        }
        public static CVector operator /(CVector vec, double val)
        {
            CVector res = new CVector();
            res.m_x = vec.m_x / val;
            res.m_y = vec.m_y / val;
            res.m_z = vec.m_z / val;
            return res;
        }
        public static CVector operator -(CVector vec, double val)
        {
            CVector res = new CVector();
            res.m_x = vec.m_x - val;
            res.m_y = vec.m_y - val;
            res.m_z = vec.m_z - val;
            return res;
        }
        public double this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return m_x;
                    case 1:
                        return m_y;
                    case 2:
                        return m_z;
                }
                THROW_RTE("bad index");
                return 0;
            }
            set
            {
                switch (index)
                {
                    case 0:
                        m_x = value;
                        return;
                    case 1:
                        m_y = value;
                        return;
                    case 2:
                        m_z = value;
                        return;
                }
                THROW_RTE("bad index");
            }

        }
        public static bool operator <(CVector vec, double val)
        {
            return
               vec.m_x < val &&
               vec.m_y < val &&
               vec.m_z < val;
        }

        public static bool operator >(CVector vec, double val)
        {
            return
               vec.m_x > val &&
               vec.m_y > val &&
               vec.m_z > val;
        }

        public static bool operator ==(CVector vec1, CVector vec2)
        {
            return
                vec1.m_x == vec2.m_x &&
                vec1.m_y == vec2.m_y &&
                vec1.m_z == vec2.m_z;
        }
        public static bool operator !=(CVector vec1, CVector vec2)
        {
            return !(vec1 == vec2);
        }
        public override bool Equals(object obj)
        {
            return obj is CVector ? this == (CVector)obj : false;
        }
        static public float invSqrt(float x)
        {
            float xhalf = 0.5f * x;
            int i = BitConverter.ToInt32(BitConverter.GetBytes(x), 0);
            i = 0x5f3759df - (i >> 1);
            x = BitConverter.ToSingle(BitConverter.GetBytes(i), 0);
            x = x * (1.5f - xhalf * x * x);
            return x;
        }

        public CVector Normlize()
        {
            double recipNorm = Math.Sqrt((m_x * m_x + m_y * m_y + m_z * m_z));
            m_x /= recipNorm;
            m_y /= recipNorm;
            m_z /= recipNorm;
            //double recipNorm = invSqrt((float)(m_x * m_x + m_y * m_y + m_z * m_z));
            //m_x *= recipNorm;
            //m_y *= recipNorm;
            //m_z *= recipNorm;
            return this;
        }
        public CVector Abs()
        {
            CVector res = new CVector();
            res.m_x = Math.Abs(m_x);
            res.m_y = Math.Abs(m_y);
            res.m_z = Math.Abs(m_z);
            return res;
        }

    }

    public class CExpFilter
    {
        double m_y = 0;     // IIR output val
        double m_x = 0;     // last input val
        double m_lpfilter_alpha = 0;
        bool m_bInit = false;   // filter is init
        bool m_bLowNHighpass = false;   // filter is low-pass/high-pass

        public CExpFilter() { }
        public CExpFilter(double lpfilter_alpha, bool bLowNHighpass)
        {
            m_y = 0;
            Init(lpfilter_alpha, bLowNHighpass);
        }
        public double OnInput(double x)
        {
            m_x = x;

            if (!m_bInit)
            {
                // on init, just take the steady statcurrent value
                m_y = x;
                m_bInit = true;
            }
            else
            {
                // calc IIR next value
                // see http://en.wikipedia.org/wiki/Low-pass_filter
                m_y = m_y + m_lpfilter_alpha * (x - m_y);
            }

            return Output();
        }
        public void Init(double lpfilter_alpha, bool bLowNHighpass)
        {
            m_lpfilter_alpha = lpfilter_alpha;
            m_bLowNHighpass = bLowNHighpass;
            m_bInit = false;
            VerifyClassInvars();
        }
        public void InitSession()
        {
            m_y = 0.0;
        }
        public double Output()
        {
            return m_bLowNHighpass ? m_y : m_x - m_y;
        }
        public static double CalcAlpha(double dT, double RC)
        {
            return dT / (RC + dT);
        }
        public void SetAlpha(double alpha)
        {
            m_lpfilter_alpha = alpha;
            //LOGV("m_lpfilter_alpha = " + m_lpfilter_alpha);
            VerifyClassInvars();
        }
        public void VerifyClassInvars()
        {
            // Verify class invariants (call after changes to class)
            if (!(m_lpfilter_alpha >= 0.0 && m_lpfilter_alpha <= 1.0))
                THROW_RTE("class invariant cannot be established");
        }
    }

    class CVecExpFilter
    {
        CExpFilter m_filtX = new CExpFilter();
        CExpFilter m_filtY = new CExpFilter();
        CExpFilter m_filtZ = new CExpFilter();

        public CVecExpFilter()
        { }

        public CVecExpFilter(double lpfilter_alpha, bool bLowNHighpass)
        {
            Init(lpfilter_alpha, bLowNHighpass);
        }
        public CVector OnInput(CVector v)
        {
            CVector res = new CVector();
            res.m_x = (float)m_filtX.OnInput(v.m_x);
            res.m_y = (float)m_filtY.OnInput(v.m_y);
            res.m_z = (float)m_filtZ.OnInput(v.m_z);
            return res;
        }
        public CVector GetValue()
        {
            CVector res = new CVector();
            res.m_x = (float)m_filtX.Output();
            res.m_y = (float)m_filtY.Output();
            res.m_z = (float)m_filtZ.Output();
            return res;
        }
        public void Init(double lpfilter_alpha, bool bLowNHighpass)
        {
            m_filtX.Init(lpfilter_alpha, bLowNHighpass);
            m_filtY.Init(lpfilter_alpha, bLowNHighpass);
            m_filtZ.Init(lpfilter_alpha, bLowNHighpass);
        }
        public void InitSession()
        {
            m_filtX.InitSession();
            m_filtY.InitSession();
            m_filtZ.InitSession();
        }

    }

    public class CQuaternion
    {
        public double m_q1 = 0;
        public double m_q2 = 0;
        public double m_q3 = 0;
        public double m_q4 = 0;

        public CQuaternion() { }
        public CQuaternion(double q1, double q2, double q3, double q4)
        {
            m_q1 = q1;
            m_q2 = q2;
            m_q3 = q3;
            m_q4 = q4;
        }
        public CQuaternion(CVector vec)
        {
            m_q1 = 0;
            m_q2 = vec.m_x;
            m_q3 = vec.m_y;
            m_q4 = vec.m_z;
        }
        public CQuaternion(CQuaternion origQ)
        {
            m_q1 = origQ.m_q1;
            m_q2 = origQ.m_q2;
            m_q3 = origQ.m_q3;
            m_q4 = origQ.m_q4;
        }
        public void Assign(CQuaternion orig)
        {
            m_q1 = orig.m_q1;
            m_q2 = orig.m_q2;
            m_q3 = orig.m_q3;
            m_q4 = orig.m_q4;
        }

        public CQuaternion Normlize()
        {
            double recipNorm = CVector.invSqrt((float)(m_q1 * m_q1 + m_q2 * m_q2 + m_q3 * m_q3 + m_q4 * m_q4));
            m_q1 *= recipNorm;
            m_q2 *= recipNorm;
            m_q3 *= recipNorm;
            m_q4 *= recipNorm;
            return this;
        }
        public CQuaternion AsConjugate()
        {
            return new CQuaternion(m_q1, -m_q2, -m_q3, -m_q4);
        }
        public CVector AsBaseVector()
        {
            CVector vec = new CVector();
            vec.m_x = m_q2;
            vec.m_y = m_q3;
            vec.m_z = m_q4;
            return vec;
        }
        public CVector RotateVec(CVector vec)
        {
            {
                /*
                    A three dimensional vector can be rotated by a quaternion using the relationship
                    Vrot = Q ox V ox Q*
                */
                CQuaternion qConj = AsConjugate();
                CQuaternion qInp = new CQuaternion(vec);
                return Product(qInp).Product(qConj).AsBaseVector();
            }
        }

        /*
        CQuaternion	defs
        */
        public CQuaternion RotateToEarthFrame(CVector gravity_sensFrame)
        {
            /*
            Create earth orientation in sensor frame
            - gravity_sensFrame: the direction of gravity, sensor frame
            - we'll use this Q to rotate from sensor to earth coors
            */
            CVector gravity_earthFrame = new CVector(0, 0, 1); // in Earth frame, G is always down
                                                               // cross our direction with gravity's, to create the rotational diff
            CQuaternion res = new CQuaternion(gravity_sensFrame.Cross(gravity_earthFrame));
            res.m_q1 = Math.Sqrt(Math.Pow(gravity_earthFrame.Length(), 2) * Math.Pow(gravity_sensFrame.Length(), 2)) + gravity_earthFrame.Dot(gravity_sensFrame);
            Assign(res.Normlize());
            return this;
        }

        public CQuaternion Product(CQuaternion B)
        {
            /*
            A ox B = [
            a1b1 - a2b2 - a3b3 - a4b4
            a1b2 + a2b1 + a3b4 - a4b3
            a1b3 - a2b4 + a3b1 + a4b2
            a1b4 + a2b3 - a3b2 + a4b1
            ]'
            */
            CQuaternion P = new CQuaternion();

            P.m_q1 = m_q1 * B.m_q1 - m_q2 * B.m_q2 - m_q3 * B.m_q3 - m_q4 * B.m_q4;
            P.m_q2 = m_q1 * B.m_q2 + m_q2 * B.m_q1 + m_q3 * B.m_q4 - m_q4 * B.m_q3;
            P.m_q3 = m_q1 * B.m_q3 - m_q2 * B.m_q4 + m_q3 * B.m_q1 + m_q4 * B.m_q2;
            P.m_q4 = m_q1 * B.m_q4 + m_q2 * B.m_q3 - m_q3 * B.m_q2 + m_q4 * B.m_q1;
            return P;
        }

    }
    public class TimeChecker
    {
        string m_name = "";
        uint m_count = 0;
        uint m_customVal = 0;
        DateTime m_time = new DateTime();
        TimeSpan m_tSpan = new TimeSpan();
        public TimeChecker(string name) { m_name = name; m_count = 0; }
        public void Reset() { m_count = 0; m_tSpan = TimeSpan.Zero; m_customVal = 0; }
        public void Start() { m_time = DateTime.Now; }
        public void AddSample(uint customVal = 0) { m_tSpan += (DateTime.Now - m_time); m_count++; m_customVal += customVal; }
        public string ToString(bool toReset = true)
        {
            string outStr = m_name + ": ";
            if (m_count == 0)
                outStr += "No data";
            else
                outStr += "av.time=" + (((float)m_tSpan.TotalMilliseconds) / m_count).ToString() + "; count=" + m_count;
            if (m_customVal > 0)
                outStr += "; customVal=" + m_customVal / m_count;
            if (toReset)
                Reset();
            return outStr;
        }
    }

    public static class InpInjector
    {
        static InputInjector m_injector = null;
        static public InputInjector Injector
        {
            get
            {
                if (m_injector == null)
                    m_injector = InputInjector.TryCreate();
                return m_injector;
            }
        }
        static public void MoveMouse(int x, int y, InjectedInputMouseOptions mouseOpt)
        {
            try
            {
                InjectedInputMouseInfo info = new InjectedInputMouseInfo();
                List<InjectedInputMouseInfo> infoArr = new List<InjectedInputMouseInfo>();
                info.DeltaX = x;
                info.DeltaY = y;
                info.MouseOptions = mouseOpt;
                infoArr.Add(info);
                Injector.InjectMouseInput(infoArr);
            }
            catch (Exception ex)
            {
                LOG.E("MoveMouse: x=" + x + "; y=" + y + "; " + ex.Message);
            }
        }
        static public void InjectKbd(VirtualKey vKey)
        {
            InjectedInputKeyboardInfo info = new InjectedInputKeyboardInfo();
            List<InjectedInputKeyboardInfo> infoArr = new List<InjectedInputKeyboardInfo>();
            info.VirtualKey = (ushort)vKey;
            infoArr.Add(info);
            Injector.InjectKeyboardInput(infoArr);
            infoArr.Clear();
            info.KeyOptions = InjectedInputKeyOptions.KeyUp;
            infoArr.Add(info);
            Injector.InjectKeyboardInput(infoArr);
        }
        static public void InjectKbd(VirtualKey[] vKeys)
        {
            List<InjectedInputKeyboardInfo> infoArr = new List<InjectedInputKeyboardInfo>();
            foreach (var vKey in vKeys)
            {
                InjectedInputKeyboardInfo info = new InjectedInputKeyboardInfo();
                info.VirtualKey = (ushort)vKey;
                infoArr.Add(info);
            }
            Injector.InjectKeyboardInput(infoArr);
            infoArr.Clear();
            foreach (var vKey in vKeys)
            {
                InjectedInputKeyboardInfo info = new InjectedInputKeyboardInfo();
                info.VirtualKey = (ushort)vKey;
                info.KeyOptions = InjectedInputKeyOptions.KeyUp;
                infoArr.Add(info);
            }
            Injector.InjectKeyboardInput(infoArr);
        }
        static public void InjectGamePadBtn(GamepadButtons buttons)
        {
            InjectedInputGamepadInfo info = new InjectedInputGamepadInfo();
            info.Buttons = buttons;
            Injector.InjectGamepadInput(info);
        }
    }
    public enum LogLines { ANY = -1, PKT_COUNT, INFO, WARN, ERR, STAT, CONN, DYNAMIC, IMU, EXT_SESS, TOTAL };
    public delegate int LogString(String logStr, int line);
    public static class LOG
    {
        struct Line
        {
            static int iter = 0;
            static
            public int m_num;
            public int m_count;
            public Line(int num = 0, int count = 0)
            {
                m_num = iter++;
                m_count = count;
            }
            public void Init(int num = 0) { m_num = num; m_count = 0; }
        }
        static Line[] m_lines = new Line[(int)LogLines.TOTAL];
        static LogString m_logString = null;
        public static LogString LogStringMethod
        {
            get { return m_logString == null ? LogStringDef : m_logString; }
            set { m_logString = value; }
        }

        static int LogStringDef(String logStr, int line)
        {
            Debug.WriteLine(logStr);
            return 0;
        }
        static int LogMsgA(LogLines ind, string prefix, string msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            if (fileName.Length > 0)
            {
                fileName = Path.GetFileName(fileName);
            }
            String logMsg = "[" + DateTime.Now.ToString("mm:ss.fff") +"] " + fileName + ":" + lineNumber + " ";
            if (ind != LogLines.ANY)
            {
                m_lines[(int)ind].m_count++;
                logMsg = m_lines[(int)ind].m_count.ToString() + ") " + logMsg;
            }
            logMsg += prefix + msg;
            int l = LogStringMethod(logMsg, (int)ind);
            return l;
        }
        static void LogMsg(LogLines ind, string prefix, string msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            LogMsgA(ind, prefix, msg, lineNumber, fileName);
        }
        public static void I(String msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            LogMsg(LogLines.INFO, "", msg, lineNumber, fileName);
        }
        public static void E(String msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            LogMsg(LogLines.ERR, "ERROR: ", msg, lineNumber, fileName);
        }
        public static void W(String msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            LogMsg(LogLines.WARN, "WARNING: ", msg, lineNumber, fileName);
        }
        //async public static Task<int> Na(String msg, int line)
        //{
        //    return await LogMsgA((LogLines)line, "", msg);
        //}
        public static void N(String msg, LogLines line,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string fileName = "")
        {
            LogMsg(line, "", msg, lineNumber, fileName);
        }
        public static string ARG<T>(Expression<Func<T>> expr)
        {
            var body = ((MemberExpression)expr.Body);
            string res = body.Member.Name + "=" + ((FieldInfo)body.Member).GetValue(((ConstantExpression)body.Expression).Value);
            res += (body.Member.Name + "=");
            return res;
        }
    }
}
    //bool m_defined = true;
    //static public void GetLimits(ref T maxValue, ref T minValue)
    //{

    //    TypeCode typeCode = Convert.GetTypeCode(x);
    //    switch (typeCode)
    //    {
    //        case TypeCode.Byte:
    //            maxValue = (T)(Object)byte.MaxValue;
    //            minValue = (T)(Object)byte.MinValue;
    //            break;
    //        case TypeCode.Char:
    //            maxValue = (T)(Object)char.MaxValue;
    //            minValue = (T)(Object)char.MinValue;
    //            break;
    //        case TypeCode.Decimal:
    //            maxValue = (T)(Object)decimal.MaxValue;
    //            minValue = (T)(Object)decimal.MinValue;
    //            break;
    //        case TypeCode.Double:
    //            maxValue = (T)(Object)decimal.MaxValue;
    //            minValue = (T)(Object)decimal.MinValue;
    //            break;
    //        case TypeCode.Int16:
    //            maxValue = (T)(Object)short.MaxValue;
    //            minValue = (T)(Object)short.MinValue;
    //            break;
    //        case TypeCode.Int32:
    //            maxValue = (T)(Object)int.MaxValue;
    //            minValue = (T)(Object)int.MinValue;
    //            break;
    //        case TypeCode.Int64:
    //            maxValue = (T)(Object)long.MaxValue;
    //            minValue = (T)(Object)long.MinValue;
    //            break;
    //        case TypeCode.SByte:
    //            maxValue = (T)(Object)sbyte.MaxValue;
    //            minValue = (T)(Object)sbyte.MinValue;
    //            break;
    //        case TypeCode.Single:
    //            maxValue = (T)(Object)float.MaxValue;
    //            minValue = (T)(Object)float.MinValue;
    //            break;
    //        case TypeCode.UInt16:
    //            maxValue = (T)(Object)ushort.MaxValue;
    //            minValue = (T)(Object)ushort.MinValue;
    //            break;
    //        case TypeCode.UInt32:
    //            maxValue = (T)(Object)uint.MaxValue;
    //            minValue = (T)(Object)uint.MinValue;
    //            break;
    //        case TypeCode.UInt64:
    //            maxValue = (T)(Object)ulong.MaxValue;
    //            minValue = (T)(Object)ulong.MinValue;
    //            break;
    //        default:
    //            maxValue = default(T);//set default value
    //            break;
    //    }
    //}

