﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;

using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using System.Numerics;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Networking.Sockets;
using System.Diagnostics;
using System.Collections;
using Windows.UI.Input.Preview.Injection;
using System.Threading;

namespace BirdUWP
{
    using SendRespondSlot = SendRespondPkt;
    public enum AddedDevResponse { CONTINUE, STOP, STOP_AND_CONNECT };
    public delegate AddedDevResponse AddedDeviceCB(String devName);

    public class DeviceInfoItem
    {
        public DeviceInformation Info { get; set; }
        int Ind { get; set; }
        public string Name { get { return Info == null ? "" : Info.Name; } }
        public String Id { get { return Info == null ? "" : Info.Id; } }
        public override string ToString()
        {
            return Ind.ToString() + ") " + (Info == null ? "empty data" : Info.Name + "; CanPair: " + Info.Pairing.CanPair) + "; IsPaired: " + Info.Pairing.IsPaired;
        }
        public async Task<BluetoothDevice> GetBluetoothDevice()
        {
            BluetoothDevice dev = null;
            for (int i = 0; i < 5 && dev == null; i++)
            {
                try
                {
                    dev = await BluetoothDevice.FromIdAsync(Id);
                }
                catch (Exception ex)
                {
                    LOG.E("PairDevice: " + ex.Message);
                    await Task.Delay(500);
                }
            }
            if (dev == null)
            {
                LOG.I("Cannot get bluetooth device");
                return null;
            }
            return dev;
        }
        public async Task<bool> Unpair()
        {
            DeviceUnpairingResult res = await Info.Pairing.UnpairAsync();
            bool stat = res.Status == DeviceUnpairingResultStatus.AlreadyUnpaired ||
                res.Status == DeviceUnpairingResultStatus.Unpaired;
            if (!stat)
                LOG.W("Cannot unpair device: " + res.Status);
            return stat;
        }
        public async Task<bool> Pair(bool enforceUnpair)
        {
            bool retVal = true;
            if (enforceUnpair && Info.Pairing.IsPaired)
                retVal = await Unpair();

            if (Info.Pairing.IsPaired)
                return true;

            DevicePairingResult res = await Info.Pairing.PairAsync();
            return res.Status == DevicePairingResultStatus.AlreadyPaired ||
                res.Status == DevicePairingResultStatus.Paired;
        }
    }
    [Flags]
    public enum CheckPktState { DONT_CARE, I_AM_DONE, BLOCK_IT };
    public enum ExecutionState { READY_FOR_USE, WAITING, DONE, ABORTED_ON_TIMEOUT };
    public class SendRespondPkt
    {
        //std::condition_variable m_condVar;
        ExecutionState m_executionState = ExecutionState.READY_FOR_USE;
        PktType m_waitForType = PktType.Unhandled;
        Packet m_spRespondedPkt = null;

        public SendRespondPkt() { m_waitForType = 0; }
        public SendRespondPkt(PktType waitForType) { m_waitForType = waitForType; }
        //The method that incapsulates the logic of incomming packet check - does it suits the one that we are waiting for
        public bool IsDone(Packet packet)
        {
            return packet.Head.TypeOfPkt == m_waitForType || 
                m_waitForType == PktType.Void && packet.Head.m_thiId == SerialPortIO.Me.ThiId;
        }

        //Checks is currently arrived packet is the one that we are waiting for or it is one that has to be blocked or something taht we are not not care
        //Returns bitmask
        public virtual CheckPktState CheckPkt(Packet packet)
        {
            if (m_executionState == ExecutionState.WAITING && IsDone(packet))
            {
                Done(packet);
                return CheckPktState.I_AM_DONE | CheckPktState.BLOCK_IT; //by default there is no meaning 
            }
            return CheckPktState.DONT_CARE;
        }
        //sends the sygnal to the first thread
        void Done(Packet packet)
        {
            lock (this)
            {
                m_spRespondedPkt = packet;
                //if (m_packetCB)
                //    m_packetCB->SetPkt(packet);
                m_executionState = ExecutionState.DONE;
                Monitor.Pulse(this);
            }
        }
        //waiting for specified packet or for timeout
        public Packet Wait(int timeOut, bool lastTry = false)
        {
            lock (this)
            {
                if (timeOut > 1000)
                    timeOut = 1000;
                if (m_executionState == ExecutionState.DONE)
                    return m_spRespondedPkt;
                m_executionState = ExecutionState.WAITING;
                Monitor.Wait(this, timeOut);
                //m_condVar.wait_for(lock, std::chrono::milliseconds(timeOut), [this]{return m_executionState == DONE; });
                //if (m_packetCB)
                //          m_packetCB->ProcessPkt(m_executionState == DONE);
                return m_spRespondedPkt;
            }
        }
        public virtual bool SeqNumberIsLocked() { return false; }
    }

    public enum ProcessState { CONTINUE, BLOCK };
    class SendRespondPktMan
    {
        public enum Defaults { WaitingTimeout = 100 };
        public ProcessState CheckPkt(Packet packet)
        {
            //std::unique_lock < std::mutex > lock (m_listMutex) ;
            lock (this)
            {
                ProcessState procState = ProcessState.CONTINUE;
                foreach (var sendRespPacket in m_sendRespondPktList)
                {
                    CheckPktState chkState = sendRespPacket.CheckPkt(packet);
                    if ((chkState & CheckPktState.BLOCK_IT) == CheckPktState.BLOCK_IT)
                        procState = ProcessState.BLOCK;
                    if ((chkState & CheckPktState.I_AM_DONE) == CheckPktState.I_AM_DONE)
                        return procState;
                }
                return procState;
            }
        }
        public SendRespondSlot Add(PktType pktType)
        {
            return Add(new SendRespondPkt(pktType));
        }
        public SendRespondSlot Add(SendRespondPkt sendRespondPkt)
        {
            lock (this)
            {
                m_sendRespondPktList.Add(sendRespondPkt);
            }
            return sendRespondPkt;
        }
        public void Remove(SendRespondSlot slot)
        {
            lock (this)
            {
                m_sendRespondPktList.Remove(slot);
            }
        }
        public Packet Wait(SendRespondSlot slot, int timeOut = 100)
        {
            return slot.Wait(timeOut);
        }
        //std::mutex m_listMutex;
        List<SendRespondPkt> m_sendRespondPktList = new List<SendRespondPkt>();
    };
    public class PortInfo
    {
        public PortInfo(String caption, String devId)
        {
            Caption = caption;
            DeviceId = devId;
        }
        public String Caption { get; set; }
        public String DeviceId { get; set; }
    }

    public class SerialPortIO
    {
        public static SerialPortIO Me = null;
        ushort m_thiId = 0;
        public ushort ThiId { get { return m_thiId; } set { m_thiId = value; } }
        AddedDeviceCB m_addedDevCb = null;
        List<PortInfo> m_ports = new List<PortInfo>();
        Dictionary<string, DeviceInfoItem> m_devices = new Dictionary<string, DeviceInfoItem>();

        void Log(string msg)
        {
            LOG.N("SerialPortIO: " + msg, LogLines.CONN);
        }
        //string Log { set { InternalLog(value); } }

        void LogPkt(int pktCount)
        {
            LOG.N(pktCount.ToString(), LogLines.PKT_COUNT);
        }
        public SerialPortIO()
        {
            Me = this;
        }
        //Windows.UI.Core.CoreDispatcher dispatcher;
        static DeviceWatcher watcher = null;
        static bool m_pairOnStop = false;
        static DeviceInfoItem m_pairableDev = null;
        public static int count = 0;
        public List<DeviceInfoItem> interfaces = new List<DeviceInfoItem>();
        public static bool isEnumerationComplete = false;
        public static string StopStatus = null;

        public void Stop()
        {
            m_runLoop = false;
            Task.Delay(1000);
        }

        public void WatchDevices(AddedDeviceCB addedDeviceCB)
        {
            try
            {
                m_addedDevCb = addedDeviceCB;
                count = 0;
                interfaces.Clear();
                if (watcher == null)
                {
                    //dispatcher = Window.Current.CoreWindow.Dispatcher;
                    String selector = BluetoothDevice.GetDeviceSelector();
                    string[] requestedProperties = new string[] { "System.Devices.Aep.DeviceAddress", "System.Devices.Aep.IsConnected" };
                    watcher = DeviceInformation.CreateWatcher("(System.Devices.Aep.ProtocolId:=\"{e0cbf06c-cd8b-4647-bb8a-263b43f0f974}\")",
                                                                requestedProperties,
                                                                DeviceInformationKind.AssociationEndpoint);
                    // Add event handlers
                    watcher.Added += watcher_Added;
                    watcher.Removed += watcher_Removed;
                    watcher.Updated += watcher_Updated;
                    watcher.EnumerationCompleted += watcher_EnumerationCompleted;
                    watcher.Stopped += watcher_Stopped;
                    Log("Enumeration started.");
                    watcher.Start();
                }
                else if (watcher.Status != DeviceWatcherStatus.Stopped)
                {
                    watcher.Stop();
                }
                if (watcher.Status == DeviceWatcherStatus.Stopped)
                {
                    Log("Enumeration started.");
                    watcher.Start();
                }

            }
            catch (ArgumentException)
            {
                //The ArgumentException gets thrown by FindAllAsync when the GUID isn't formatted properly
                //The only reason we're catching it here is because the user is allowed to enter GUIDs without validation
                //In normal usage of the API, this exception handling probably wouldn't be necessary when using known-good GUIDs 
                Log("Caught ArgumentException. Failed to create watcher.");
            }
        }

        void StopWatcher()
        {
            try
            {
                if (watcher.Status == Windows.Devices.Enumeration.DeviceWatcherStatus.Stopped)
                {
                    StopStatus = "The enumeration is already stopped.";
                }
                else
                {
                    watcher.Stop();
                }
            }
            catch (ArgumentException)
            {
                Log("Caught ArgumentException. Failed to stop watcher.");
            }
        }

        void watcher_Added(DeviceWatcher sender, DeviceInformation deviceInterface)
        {
            AddDevice(new DeviceInfoItem { Info = deviceInterface });
        }

        void watcher_Updated(DeviceWatcher sender, DeviceInformationUpdate devUpdate)
        {
        }

        void watcher_Removed(DeviceWatcher sender, DeviceInformationUpdate devUpdate)
        {

        }

        void watcher_EnumerationCompleted(DeviceWatcher sender, object args)
        {
            isEnumerationComplete = true;
            watcher.Stop();
        }

        void watcher_Stopped(DeviceWatcher sender, object args)
        {
            if (watcher.Status == Windows.Devices.Enumeration.DeviceWatcherStatus.Aborted)
            {
                StopStatus = "Enumeration stopped unexpectedly. Click Watch to restart enumeration.";
            }
            else if (watcher.Status == Windows.Devices.Enumeration.DeviceWatcherStatus.Stopped)
            {
                StopStatus = "You requested to stop the enumeration. Click Watch to restart enumeration.";
            }
            LOG.I(StopStatus);
            if (m_pairOnStop && m_pairableDev != null)
                PairDevice(m_pairableDev);
        }


        SerialDevice m_dev = null;
        //SerialDevice.In inputStream = m_dev.InputStream;
        private StreamSocket chatSocket = null;
        DataReader dataReader = null;
        DataWriter dataWriter = null;
        byte[] startPacket = new byte[] { 0x80, 0, 0, 0x05, 0x2a, 0x07, 0xa8 };
        Windows.Storage.Streams.Buffer writeBuffer = new Windows.Storage.Streams.Buffer(1024);
        public static readonly Guid RfcommChatServiceUuid = Guid.Parse("34B1CF4D-1069-4AD6-89B6-E161D79BE4D8");
        bool m_runLoop = false;
        void AddDevice(DeviceInfoItem devInfo)
        {
            m_devices[devInfo.Name] = devInfo;
            switch (m_addedDevCb(devInfo.Name))
            {
                case AddedDevResponse.CONTINUE:
                    break;
                case AddedDevResponse.STOP:
                    watcher.Stop();
                    break;
                case AddedDevResponse.STOP_AND_CONNECT:
                    m_pairOnStop = true;
                    m_pairableDev = devInfo;
                    watcher.Stop();
                    break;
            }
        }

        public bool IsRunning { get { return m_runLoop; } }
        public async Task<bool> TryToReconnect()
        {
            m_runLoop = false;
            //if (m_pairableDev.Info.IsEnabled)
            //{
            await m_pairableDev.Info.Pairing.UnpairAsync();
            LOG.N("Unpaired", LogLines.DYNAMIC);
            dataReader = null;
            PairDevice(m_pairableDev);
            //}
            return true;
        }
        public async void AsyncMethod(DeviceInfoItem devInfo)
        {
            await devInfo.Info.Pairing.PairAsync();
        }

        Windows.Networking.HostName m_connectionHostName;
        string m_connectionServiceName = "";

        async Task<bool> PrepareConnectionProp(BluetoothDevice dev)
        {
            var rfcommServices = await dev.GetRfcommServicesAsync(); //.GetRfcommServicesForIdAsync(RfcommServiceId.FromUuid(RfcommChatServiceUuid), BluetoothCacheMode.Uncached);
            if (rfcommServices.Services.Count == 0)
            {
                Log("The list of rfcommServices is empty");
                return false;
            }
            var rfcommServicesDedicated = await dev.GetRfcommServicesForIdAsync(RfcommServiceId.FromUuid(rfcommServices.Services[0].ServiceId.Uuid));
            Log(rfcommServicesDedicated.Services.Count.ToString());
            var chatService = rfcommServicesDedicated.Services[0];
            var attributes = await chatService.GetSdpRawAttributesAsync();
            const UInt16 SdpServiceNameAttributeId = 0x100;
            const byte SdpServiceNameAttributeType = (4 << 3) | 5;
            if (!attributes.ContainsKey(SdpServiceNameAttributeId))
            {
                Log(
                    "The Chat service is not advertising the Service Name attribute (attribute id=0x100). " +
                    "Please verify that you are running the BluetoothRfcommChat server.");
                return false;
            }
            var attributeReader = DataReader.FromBuffer(attributes[SdpServiceNameAttributeId]);
            var attributeType = attributeReader.ReadByte();
            if (attributeType != SdpServiceNameAttributeType)
            {
                Log(
                    "The Chat service is using an unexpected format for the Service Name attribute. " +
                    "Please verify that you are running the BluetoothRfcommChat server.");
                return false;
            }
            m_connectionHostName = chatService.ConnectionHostName;
            m_connectionServiceName = chatService.ConnectionServiceName;
            return true;
        }

        public async void UnpairDevice(DeviceInfoItem devInfo)
        {
            await devInfo.Unpair();
        }

        public async void PairDevice(DeviceInfoItem devInfo)
        {
            //watcher.Stop();
            m_pairableDev = devInfo;
            BluetoothDevice dev = await devInfo.GetBluetoothDevice();
            if (dev == null)
                return;
            bool succeed = false;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    bool res = await devInfo.Pair(false);// i > 0); //don't unpair on first try
                    if (!res)
                        return;
                    res = await PrepareConnectionProp(dev);
                    if (!res)
                    {
                        LOG.E("Cannot establish host and service names");
                        return;
                    }
                    //var serviceNameLength = attributeReader.ReadByte();

                    // The Service Name attribute requires UTF-8 encoding.
                    //attributeReader.UnicodeEncoding = System.Text.UnicodeEncoding.Utf8;

                    //watcher.Stop();
                    chatSocket = new StreamSocket();

                    await chatSocket.ConnectAsync(m_connectionHostName, m_connectionServiceName);
                    succeed = true;
                    break;
                }
                catch (Exception ex) when ((uint)ex.HResult == 0x80070490) // ERROR_ELEMENT_NOT_FOUND
                {
                    Log("Please verify that you are running the BluetoothRfcommChat server.");
                    return;
                }
                catch (Exception ex) when ((uint)ex.HResult == 0x80072740) // WSAEADDRINUSE
                {
                    Log("Please verify that there is no other RFCOMM connection to the same device.");
                    return;
                }
                catch (Exception ex)
                {
                    LOG.E("chatSocket.ConnectAsync failure: " + ex.Message);
                    await Task.Delay(200);
                    continue;
                }
            }
            if (succeed)
            {
                dataWriter = new DataWriter(chatSocket.OutputStream);
                dataReader = new DataReader(chatSocket.InputStream);
                Start();
            }
        }


        public async Task<bool> InitPort(String devId = "")
        {
            try
            {
                if (m_dev != null)
                {
                    return true;
                }
                var aqs = Windows.Devices.SerialCommunication.SerialDevice.GetDeviceSelector();
                var dis = await DeviceInformation.FindAllAsync(aqs);
                if (dis == null)
                {
                    //Log( "Cannot get com device");
                    return false;
                }
                if (String.IsNullOrEmpty(devId))
                {
                    devId = dis[0].Id;
                }
                else
                {
                    bool found = false;
                    foreach (DeviceInformation dev in dis)
                    {
                        const string DevIdPropTag = "System.Devices.DeviceInstanceId";
                        Object val = new Object();
                        if (dev.Properties.TryGetValue(DevIdPropTag, out val) &&
                             String.Compare(devId, val.ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                        {
                            found = true;
                            devId = dev.Id;
                        }
                        //    foreach(var prop in dev.Properties)//System.Devices.DeviceInstanceId - it is the properties that contains device ID
                        //    {
                        //        Log( prop.Key + "=" + (prop.Value == null ? "null" : prop.Value.ToString());
                        //        if (prop.Value != null && String.Compare(devId, prop.Value.ToString(),StringComparison.OrdinalIgnoreCase) == 0)
                        //        {
                        //            devId = dev.Id;
                        //            found = true;
                        //        }
                        //    }
                    }
                    if (!found)
                    {
                        Log("Cannot find device with specified ID=" + devId);
                        return false;
                    }
                }
                m_dev = await SerialDevice.FromIdAsync(devId);
                if (m_dev == null)
                {
                    //Log( "No ports found");
                    return false;
                }
                //Log( m_dev?.PortName;
                m_dev.BaudRate = 230400;
                m_dev.Parity = SerialParity.None;
                m_dev.ReadTimeout = TimeSpan.FromMilliseconds(1);
                m_dev.StopBits = SerialStopBitCount.One;
                m_dev.DataBits = 8;

                var inputStream = m_dev.InputStream;
                dataReader = new DataReader(inputStream);
                dataReader.InputStreamOptions = InputStreamOptions.Partial;
                var outputString = m_dev.OutputStream;
                dataWriter = new DataWriter(outputString);
                Log("Found Serial Port: " + m_dev.PortName + "; USB porduct: " + m_dev.UsbProductId);
                return true;
            }
            catch (Exception ex)
            {
                Log("InitPort Error: " + ex.Message);
                return false;
            }
        }
        List<InjectedInputMouseInfo> infoArr = new List<InjectedInputMouseInfo>();


        CUIControlsState ctrlState = new CUIControlsState();

        class PktParser
        {
            enum State { UNDEFINED, READ_HEAD, READ_DATA };
            const uint MaxPktSize = 256;
            byte[] m_data = new byte[MaxPktSize];
            public uint m_dataLen = 0;
            State m_state = State.UNDEFINED;
            Packet m_packet = new Packet();
            public Packet ParsedPacket
            {
                get
                {
                    Packet tmpPkt = m_packet;
                    m_packet = new Packet();
                    return tmpPkt;
                }
            }
            //public PktBuf() {}

            //public byte[] GetData(ref uint len)
            //{
            //    len = m_dataLen;
            //    m_dataLen = 1;
            //    return m_data;
            //}
            void Reset()
            {
                m_state = State.UNDEFINED;
                m_dataLen = 0;
            }
            public bool Parse(byte nextByte)
            {
                bool ret = false;
                if (m_dataLen == MaxPktSize)
                {
                    LOG.E("Too long pkt!!");
                    Reset();
                    return false;
                }
                m_data[m_dataLen] = nextByte;
                m_dataLen++;
                switch (m_state)
                {
                    case State.UNDEFINED:
                        if (nextByte == PktHead.Delimeter)
                        {
                            m_state = State.READ_HEAD;
                        }
                        else
                            m_dataLen = 0;
                        break;
                    case State.READ_HEAD:
                        if (m_dataLen == PktHead.CLI_PKT_HDR_LEN)
                        {
                            if (m_packet.Head.Parse(m_data, m_dataLen))
                            {
                                m_state = State.READ_DATA;
                            }
                            else
                            {
                                Reset();
                            }
                        }
                        break;
                    case State.READ_DATA:
                        if (m_dataLen == m_packet.Head.PacketLen)
                        {
                            if (PktHead.ValidateCS(m_data, m_dataLen) &&
                                m_packet.ParseData(m_data, m_dataLen))
                                ret = true;
                            Reset();
                        }
                        break;
                }
                return ret;
            }
        }

        SendRespondPktMan m_sendRespPacketMan = new SendRespondPktMan();
        public async void RequestIMUOffsets()
        {
            LOG.I("RequestIMUOffsets attempt ");
            Packet requestPkt = CreateThiCmdPacket(PktType.CmdGetThimbleGyroOffset);
            Packet responded = await SendRespondPkt(requestPkt, PktType.AckGetThimbleGyroOffset);
            LOG.I("RequestIMUOffsets result: " + ((responded != null) ? "succeed" : "failure"));
            if (responded != null)
                responded.ProcessData();
        }

        PktProcessor pktProcessor = new PktProcessor();
        public int ImuCount { get; set; }
        async void ReadData()
        {

            //await dataReader.LoadAsync(1);
            //uint readBytes = await dataReader.LoadAsync(dataReader.UnconsumedBufferLength);

            //byte[] byArray = new byte[readBytes];

            //dataReader.ReadBytes(byArray);
            m_runLoop = true;
            Packet packet = new Packet();
            //PktHead head = new PktHead();
            uint bytesToProcess = 0;
            const uint bufSize = 32;
            byte[] buf = new byte[bufSize];
            PktParser pktParser = new PktParser();

            pktProcessor.Run();
            while (m_runLoop)
            {
                try
                {
                    //tCheckReadData.Start();
                    bytesToProcess = await dataReader.LoadAsync(bufSize);
                    //tCheckReadData.AddSample(bytesToProcess);
                    if (bytesToProcess < 1)
                    {
                        await Task.Delay(1);
                        continue;
                    }
                    dataReader.ReadBytes(buf);
                    for (int i = 0; i < bytesToProcess; i++)
                    {
                        if (pktParser.Parse(buf[i]))
                        {
                            //continue;
                            packet = pktParser.ParsedPacket;
                            if (packet.Head.TypeOfPkt == PktType.IMU6axStatus)
                                ImuCount++;
                            if (m_sendRespPacketMan.CheckPkt(packet) == ProcessState.BLOCK)
                            {
                                LOG.I("Packet is blocked: " + packet);
                                continue;
                            }
                            pktProcessor.EnquePacket(packet);
                        }
                        //if (packet.Head.m_thiId != 0xbeef && packet.Head.m_thiId != 0 && m_thiId == 0)
                        //{
                        //    m_thiId = packet.Head.m_thiId;
                        //    //Log( "Process pkt 0x" + packet.Head.m_type.ToString("X2");
                        //}
                    }
                }
                catch (Exception ex)
                {
                    LOG.E("ReadData Exception: " + ex.Message);
                    continue;
                }

            }
            //await dataReader.LoadAsync(1);
        }

        public void Start()
        {
            try
            {
                //await InitPort();
            }
            catch (Exception ex)
            {
                Log("ERROR: " + ex.Message);
            }

            if (dataWriter == null)
            {
                return;
            }

            //byte[] buf = Encoding.ASCII.GetBytes(startPacket);
            //Log( string.Format("Write to COM - {0}", DateTime.Now.Ticks);
            //writeBuffer = 

            //await dataWriter.FlushAsync();
            SensKeepAlive();
            //Log( string.Format("After Sleeping - Before Reading  - {0}", DateTime.Now.Ticks);
            //Log( string.Format("Waiting for Read  - {0}", DateTime.Now.Ticks);
            ReadData();
            //Log( "Read " + data.Count.ToString() + " bytes of data";
        }

        byte[] m_syncPacket = null;
        ushort m_lastSentThiId = 0;
        void CreateThiSyncPacket()
        {
            if (m_syncPacket == null || m_lastSentThiId != m_thiId) //create ppsCmd
            {
                m_syncPacket = PktHead.ToBinary((byte)PktType.CmdSetPairedThiStatuses, m_thiId == 0 ? (byte)0 : (byte)3);
                if (m_thiId != 0)
                {
                    PktHead.SHORT_TO_BUFF_POS(m_thiId, ref m_syncPacket, PktHead.CLI_PKT_HDR_LEN);
                    m_syncPacket[PktHead.CLI_PKT_HDR_LEN + 2] = 0x03;
                }
                PktHead.SetCS(ref m_syncPacket);
            }
        }
        Packet m_lightModeOfPkt = null;
        Packet LightModeOfPkt
        {
            get
            {
                if (m_lightModeOfPkt == null)
                {
                    m_lightModeOfPkt = CreateThiCmdPacket(PktType.CmdSensorLightMode, 0);
                }
                return m_lightModeOfPkt;
            }
        }
        byte seqNum = 0;
        public Packet CreateThiCmdPacket(PktType pktType, Object data = null)
        {
            Packet cmdPacket = new Packet(pktType);
            cmdPacket.Head.m_type = (byte)pktType;
            cmdPacket.Head.m_thiId = (ushort)m_thiId;
            cmdPacket.Head.m_seqNum = ++seqNum;
            if (data != null)
                cmdPacket.Data[0] = data;
            return cmdPacket;
        }

        byte[] SyncPacket
        {
            get
            {
                //if (m_syncPacket == null && m_thiId > 0)
                //{
                CreateThiSyncPacket();
                //}
                return m_syncPacket;
            }
        }

        async Task SendData(byte[] data, uint delay)
        {
            lock (this)
            {
                if (data == null)
                    return;
                dataWriter.WriteBytes(data);
            }
            await dataWriter.StoreAsync();
            await Task.Delay(TimeSpan.FromMilliseconds(delay));
        }
        public bool SendingConnectingPackets { get; set; }
        int m_sentLightPkts = 0;
        public async Task<bool> SendSyncPackets()
        {
            try
            {
                await SendData(startPacket, 1);
                await SendData(SyncPacket, 0);
                if (m_sentLightPkts < 10)
                {
                    await SendPkt(LightModeOfPkt, 0);
                    m_sentLightPkts++;
                }
                return true;
            }
            catch (Exception ex)
            {
                LOG.E("SensKeepAlive " + ex.Message);
            }
            return false;
        }
        async void SensKeepAlive()
        {
            SendingConnectingPackets = true;
            Log("Started sending of conncecting packets");
            while (SendingConnectingPackets)
            {
                await SendSyncPackets();
                await Task.Delay(500);
            }
            Log("Ended sending conncecting packets");
        }

        public async Task<Packet> SendRespondPkt(Packet pkt, PktType waitFor, int retries = 5, int timeout = 100)
        {
            LOG.I("SendRespondPkt: " + pkt);
            byte[] pktBinData = pkt.ToBinary();
            Packet respondPkt = null;
            SendRespondPkt request = m_sendRespPacketMan.Add(waitFor);
            for (int tries = 0; tries < retries && respondPkt == null; tries++)
            {
                await SendData(pktBinData, 0);
                respondPkt = m_sendRespPacketMan.Wait(request, timeout);
            }
            m_sendRespPacketMan.Remove(request);
            return respondPkt;
        }

        public async Task SendPkt(Packet pkt, uint delay = 1)
        {
            byte[] pktBinData = pkt.ToBinary();
            await SendData(pktBinData, delay);
        }

        async void SetIMU(bool activateIMU)
        {
            Log("Send IMU " + (activateIMU ? "On" : "Off"));
            Packet cmdPkt = CreateThiCmdPacket(PktType.CmdSetIMU, activateIMU ? 1 : 0);
            await SendPkt(cmdPkt);
        }
        public void Read()
        {

            var receivedStrings = "";
            while (dataReader.UnconsumedBufferLength > 0)
            {
                // Note that the call to readString requires a length of "code units" 
                // to read. This is the reason each string is preceded by its length 
                // when "on the wire".
                uint bytesToRead = dataReader.ReadUInt32();
                receivedStrings += dataReader.ReadString(bytesToRead) + "\n";
            }

            // Populate the ElementsRead text block with the items we read 
            // from the stream.
            Log(receivedStrings);
        }

        public DeviceInfoItem GetDevByName(String name)
        {
            DeviceInfoItem devInfo = null;
            m_devices.TryGetValue(name, out devInfo);
            return devInfo;
        }
    }
}
