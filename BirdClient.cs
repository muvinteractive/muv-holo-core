﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirdUWP
{
    public class BirdClient
    {
        SerialPortIO m_serialPorts = null;
        string m_preferedDevNamePart = "";
        public BirdClient(LogString logStr = null)
        {
            LOG.LogStringMethod = logStr;
        }
        public void EnumerateDevices(AddedDeviceCB addedDevCB)
        {
            if (m_serialPorts == null)
                m_serialPorts = new SerialPortIO();
            m_serialPorts.WatchDevices(addedDevCB);
        }
        public AddedDevResponse AddedDevice(String devName)
        {
            bool isPrefered = false;
            isPrefered = (m_preferedDevNamePart.Length > 0 && devName.IndexOf(m_preferedDevNamePart) >= 0);
            if(isPrefered)
            {
                return AddedDevResponse.STOP_AND_CONNECT;
            }
            return AddedDevResponse.CONTINUE;
        }

        public bool Connect(string name)
        {
            if (m_serialPorts == null)
            {
                m_preferedDevNamePart = name;
                EnumerateDevices(AddedDevice);
            }
            else if (m_serialPorts.IsRunning)
                return true;
            
            DeviceInfoItem devInfo = m_serialPorts.GetDevByName(name);
            if (devInfo != null)
            {
                m_serialPorts.PairDevice(devInfo);
                return true;
            }
            return false;
        }
        public bool Unpair(string name)
        {
            if (m_serialPorts == null)
            {
                return false;
            }
            if (m_serialPorts.IsRunning)
                return true;

            DeviceInfoItem devInfo = m_serialPorts.GetDevByName(name);
            if (devInfo != null)
            {
                m_serialPorts.UnpairDevice(devInfo);
                return true;
            }
            return false;
        }
        public void Stop()
        {
            if(m_serialPorts != null)
                m_serialPorts.Stop();
        }
    }
}
