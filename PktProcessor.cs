﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Gaming.Input;
using Windows.System;
using Windows.UI.Input.Preview.Injection;
using static BirdUWP.ElapsedSince;

namespace BirdUWP
{
    class Bird
    {
        public bool Imu { get; set; }
        public Bird()
        {
            Imu = false;
        }
    }


    class MovementFilter
    {
        DblPoint m_prevdXdY;
        DateTime m_prevCallTime = new DateTime();
        double m_velocity = 0;
        static bool sensitivityBased = true;
        static int zeroCount = 0;
        static DateTime lastIMU;

        public MovementFilter()
        {
        }

        double factor = 3;
        static double minSensitivity = 2;

        public Point CalcDelta(CUIControlsState ctrlsState, DblPoint dXdY, double sensitivity)
        {
            double maxSensitivity = sensitivity;
            DateTime currTime = DateTime.Now;
            double vel = 0;
            bool validPoint = true;
            double origVel = 0;
            if (m_prevdXdY.IsDefined())
            {
                int deltaTime = (int)(currTime - m_prevCallTime).TotalMilliseconds;
                vel = Math.Sqrt(dXdY.x * dXdY.x + dXdY.y * dXdY.y) / deltaTime;
                if (vel < 0.00001)
                {//zero velocity - something is wrong
                    validPoint = false;
                    vel = m_velocity;//use the prev velocity value
                    zeroCount++;
                    //LOG("CalcDelta: zero velocity " << LOG_ARG(zeroCount) << ctrlsState.m_curSpPkt->GetType().IdName());
                }
                origVel = vel;

                //vel = m_velFilter.OnInput({ vel,0 }).x;
                if (validPoint)
                {
                    if (zeroCount > 3)
                    {
                        //LOG("CalcDelta elapsed since last IMU pkt: " << lastIMU.ElapsedAsMSec() << "; " << LOG_ARG(zeroCount));
                    }
                    zeroCount = 0;
                    lastIMU = DateTime.Now;
                }
                if (sensitivityBased)
                {
                    sensitivity *= (vel * factor);
                    sensitivity = Math.Min(sensitivity, maxSensitivity);
                    sensitivity = Math.Max(sensitivity, minSensitivity);
                }
                //Debug.WriteLine("sens=" + sensitivity + "; vel=" + vel);
            }
            m_velocity = vel;
            m_prevCallTime = currTime;
            Point resPt = new Point((int)(dXdY.x * sensitivity), (int)(dXdY.y * sensitivity));
            Point resPtOrig = resPt;
            if (resPt.IsDefault() && m_prevdXdY.IsDefined())
            {
                m_prevdXdY += dXdY;
                resPt = new Point((int)(m_prevdXdY.x * sensitivity), (int)(m_prevdXdY.y * sensitivity));
            }
            if (!resPt.IsDefault())
            {
                m_prevdXdY = dXdY;
            }
            //LOG.Nv("CalcDelta 1: dXdY=" + dXdY.ToString() + "; resPt=" + resPt + "; vel=" + vel.ToString() + "; sens.=" + sensitivity,
            //   logLine);
            if (!sensitivityBased)
            {
                double alpha = Math.Min(1.0, vel);
                //m_motionFilter.SetAlpha({ alpha, alpha });
                //resPt = m_motionFilter.OnInput({ (double)resPt.x, (double)resPt.y });
            }
            //LOG("CalcDelta 2: " << LOG_ARG(resPt) << LOG_ARG(sensitivity) << LOG_ARG(resPtOrig));
            return resPt;
        }
        void Reset()
        {
            m_prevdXdY.Reset();
            m_prevCallTime = DateTime.MinValue;

        }
    }

    public class PktProcessor
    {
        //[DllImport("user32.dll")]
        //private static extern IntPtr GetForegroundWindow();

        ConcurrentQueue<Packet> m_packets = new ConcurrentQueue<Packet>();
        Task m_processingTask;
        bool m_runLoop = true;
        Bird m_bird = new Bird();
        MovementFilter m_movementFilter = new MovementFilter();

        public void Run()
        {
            m_processingTask = new Task(ProcessingLoop);
            m_processingTask.Start();
            //ProcessingLoop();
        }
        public void EnquePacket(Packet packet)
        {
            m_packets.Enqueue(packet);
        }
        DateTime m_lastIMUoffsetRequest = DateTime.Now;
        void RequestIMUOffsets()
        {
            if (ElapsedAsMSec(m_lastIMUoffsetRequest) < 1000)
                return;
            m_lastIMUoffsetRequest = DateTime.Now;
            SerialPortIO.Me.RequestIMUOffsets();
            //Packet requestPkt = SerialPortIO.Me.CreateThiCmdPacket(PktType.CmdGetThimbleGyroOffset);
            //await SerialPortIO.Me.SendPkt(requestPkt);
        }
        DateTime m_lastImuSentTime = new DateTime();
        bool lastImuSentFlag;
        static bool g_gestureIsOn = false;
        static bool g_gestureIsOnChanged = false;
        public static bool GestureIsOn
        {
            get { return g_gestureIsOn; }
            set
            {
                g_gestureIsOnChanged = g_gestureIsOn != value;
                g_gestureIsOn = value;
            }
        }
        async void SetIMU(bool activateIMU)
        {
            //var frontWind = GetForegroundWindow();
            if (lastImuSentFlag == activateIMU && ElapsedAsMSec(m_lastImuSentTime) < 500)
            {
                return;
            }
            LOG.I("Send IMU " + (activateIMU ? "On" : "Off"));
            m_bird.Imu = activateIMU;
            lastImuSentFlag = activateIMU;
            m_lastImuSentTime = DateTime.Now;
            Packet cmdPkt = SerialPortIO.Me.CreateThiCmdPacket(PktType.CmdSetIMU, activateIMU ? 1 : 0);
            await SerialPortIO.Me.SendRespondPkt(cmdPkt, PktType.AckToClient, 5, 10);
            //if (m_bird.Imu)
            //    PktDataIMU.ReinitInterpreter();
        }

        static List<InjectedInputMouseInfo> infoArr = new List<InjectedInputMouseInfo>();
        static public void MoveMouse(int x, int y, InjectedInputMouseOptions mouseOpt)
        {
            try
            {
                if ((mouseOpt & InjectedInputMouseOptions.Wheel) == InjectedInputMouseOptions.Wheel)
                {
                    LOG.N("Whill: x=" + x + "; y=" + y, LogLines.DYNAMIC);
                    if (x == 0)
                        mouseOpt = InjectedInputMouseOptions.Wheel;
                    else
                        mouseOpt = InjectedInputMouseOptions.HWheel;
                }
                InpInjector.MoveMouse(x, y, mouseOpt);
                if ((mouseOpt & InjectedInputMouseOptions.Wheel) == InjectedInputMouseOptions.Wheel)
                    LOG.N("Whill succeed: x=" + x + "; y=" + y, LogLines.DYNAMIC);
            }
            catch (Exception ex)
            {
                LOG.E("MoveMouse: x=" + x + "; y=" + y + "; " + ex.Message);
            }

        }

        void InjectKbd(VirtualKey vKey)
        {
            InpInjector.InjectKbd(vKey);
        }

        public void InjectKbd(VirtualKey[] vKeys)
        {
            InpInjector.InjectKbd(vKeys);
        }

        DateTime lastMove = new DateTime();
        uint pps = 0;
        uint btns = 0;
        uint imu = 0;
        uint processedImu = 0;
        TimeChecker tCheckMouse = new TimeChecker("MouseMove: ");
        static uint sensitivity = 15;
        static public uint Sensitivity
        {
            get { return sensitivity; }
            set { sensitivity = value; }
        }


        void ProcStatPkt(Packet packet)
        {
            CUIControlsState ctrlState = Packet.CtrlState;
            ctrlState.Btn(BtnType.Pinch).Check();

            if (!PktDataIMU.IsIMUOffsetsRequested && ctrlState.Btn(BtnType.Finger).IsPressed)
                RequestIMUOffsets();

            PktDataStatus statData = packet.Data as PktDataStatus;
            if (statData.GesturesOn() != m_bird.Imu)
            {
                if (m_bird.Imu && !GestureIsOn && (!ctrlState.Btn(BtnType.Finger).IsPressed || !ctrlState.Btn(BtnType.Pinch).IsPressed))
                    SetIMU(false);
                if (GestureIsOn && m_bird.Imu && !statData.GesturesOn()) //gestures is switched on but bird still didn't get thiscommand
                    SetIMU(true);
                return;
            }

            if (m_bird.Imu && !ctrlState.Btn(BtnType.Pinch).IsPressed && !GestureIsOn)
            {
                SetIMU(false);
            }
        }
        static double minGap = 1;
        Point DxDy(bool useDynamicSens)
        {
            CUIControlsState ctrlState = Packet.CtrlState;
            uint sens = useDynamicSens ? 1 : sensitivity;
            DblPoint rowdXdY = new DblPoint((ctrlState.m_angularVelocity.m_z * -sens),
                (ctrlState.m_angularVelocity.m_x * sens));
            if (useDynamicSens)
                return m_movementFilter.CalcDelta(ctrlState, rowdXdY, sensitivity);
            return new Point((int)rowdXdY.x, (int)rowdXdY.y);
        }
        static bool useDynamicSensitivity = false;
        void ProcImuPkt(Packet packet)
        {
            imu++;
            CUIControlsState ctrlState = Packet.CtrlState;
            if (!ctrlState.Btn(BtnType.Pinch).IsPressed && !GestureIsOn)
                return;
            PktDataIMU imuData = packet.Data as PktDataIMU;
            TimeSpan tSpan = (DateTime.Now - lastMove);
            //packet.ProcessData();
            //packet.Data.ProcessPacket(packet, ref ctrlState);
            if ((DateTime.Now - lastMove).TotalMilliseconds < minGap)
            {
                //if (GestureIsOn)
                  //  packet.ProcessData();
                Task.Delay(1);
                //return;
            }
            packet.ProcessData();
            //tCheckMouse.Start();
            if (ctrlState.Btn(BtnType.Pinch).IsPressed)
            {
                Point dXdY = DxDy(useDynamicSensitivity);
                if (ctrlState.Btn(BtnType.Switch).IsPressed && ctrlState.Btn(BtnType.Switch).ElapsedSinceLastChange() < 300)
                    return; //to stable mouse clicks
                MoveMouse(dXdY.x, dXdY.y, InjectedInputMouseOptions.Move);
                //tCheckMouse.AddSample();
                lastMove = DateTime.Now;
            }
            else if (GestureIsOn && Packet.CtrlState.IsGestureChanged())
            {
                switch (Packet.CtrlState.GetGesture())
                {
                    case GestureId.gestId_left:
                        //InjectKbd(VirtualKey.Left);
                        break;
                    case GestureId.gestId_right:
                        //InjectKbd(VirtualKey.Right);
                        break;
                    case GestureId.gestId_up:
                        InjectKbd(VirtualKey.LeftWindows);
                        break;
                    case GestureId.gestId_down:
                        //InjectKbd(VirtualKey.LeftWindows);
                        break;
                }
            }
            processedImu++;
        }
        void ProcBtnsPkt(Packet packet)
        {
            if (packet.Head.TypeOfPkt != PktType.BtnStatus)
                LOG.E("Strange button packet");
            CUIControlsState ctrlState = Packet.CtrlState;
            //packet.Data.ProcessPacket(packet, ref ctrlState);
            bool activateIMU = ctrlState.Btn(BtnType.Pinch).IsPressed || GestureIsOn;// || ctrlState.Btn(BtnType.Home).IsPressed;
            if (activateIMU != m_bird.Imu)
                SetIMU(ctrlState.Btn(BtnType.Pinch).IsPressed);

            if (ctrlState.Btn(BtnType.Switch).IsChanged)
            {
                if (ctrlState.Btn(BtnType.Pinch).IsPressed)
                {
                    MoveMouse(0, 0,
                         ctrlState.Btn(BtnType.Switch).IsPressed ?
                         InjectedInputMouseOptions.LeftDown :
                         InjectedInputMouseOptions.LeftUp);
                }
                else if (ctrlState.Btn(BtnType.Switch).IsPressed)
                    InjectKbd(VirtualKey.LeftWindows);
            }
                if (ctrlState.Btn(BtnType.Home).IsStarted)//to avoid unattended home btn press 
                MouseClick();    //InpInjector.InjectGamePadBtn(GamepadButtons.A);
            if (ctrlState.Btn(BtnType.Home).IsPressed &&
                ElapsedAsMSec(m_lastMouseClick) > 500)
            {
                MouseClick();
            }
            btns++;
        }
        DateTime m_lastMouseClick;
        void MouseClick()
        {
            m_lastMouseClick = DateTime.Now;
            MoveMouse(0, 0, InjectedInputMouseOptions.LeftDown);
            Task.Delay(10);
            MoveMouse(0, 0, InjectedInputMouseOptions.LeftUp);
            LOG.N("Mouse click detected", LogLines.DYNAMIC);
        }
        DateTime m_lastOfnShift = DateTime.Now;
        bool m_onLongMovement = false;
        void ProcOfnPkt(Packet packet)
        {
            CUIControlsState ctrlState = Packet.CtrlState;
            if (ctrlState.m_OFN.IsCoorsStateChanged())
            {
                Point ofnPnt = ctrlState.m_OFN.GetCoors();
                //MoveMouse(ofnPnt.x, ofnPnt.y, InjectedInputMouseOptions.Wheel | InjectedInputMouseOptions.HWheel);
                //return;
                uint count = 0;
                if (ctrlState.m_OFN.GetDownMovement(out count) > 80 || count > 3)
                {
                    if (!m_onLongMovement)
                    {
                        //InjectKbd(VirtualKey.LeftWindows);
                        m_onLongMovement = true;
                    }
                    //VirtualKey actKey = ofnPnt.y > 0 ? (VirtualKey)0xBB : (VirtualKey)0xBD;
                    //InjectKbd(new VirtualKey[] { VirtualKey.Control, VirtualKey.Shift, actKey });
                }
                else
                {
                    m_onLongMovement = false;
                    //if (ctrlState.m_OFN.GetCoors().y < 0)
                    //    LOG.W("y=" + ctrlState.m_OFN.GetCoors().y);
                }
                if (ofnPnt.x != 0)
                {
                    //InjectKbd(ofnPnt.x > 0 ? VirtualKey.Left : VirtualKey.Right);
                    VirtualKey actKey = ofnPnt.x > 0 ? (VirtualKey)0xBB : (VirtualKey)0xBD;
                    InjectKbd(new VirtualKey[] { VirtualKey.Control, VirtualKey.Shift, actKey });
                }
            }
            else if (ctrlState.m_OFN.IsOFNTapAndHoldDetected())
            {
                //InjectKbd(VirtualKey.LeftWindows);
                //OfnMouseClick();
                //InjectKbd(VirtualKey.Escape);// 0x5b);
            }
        }
        async void ProcessingLoop()
        {
            LOG.I("ProcessingLoop started");
            Packet packet = null;
            int totalPackets = 0;
            //SerialPortIO serialPortIO = SerialPortIO.Me;
            DateTime logTimer = new DateTime();
            uint pktCount = 0;
            uint birdPktsCount = 0;
            ushort m_thiId = 0;
            int noPacketsCount = 0;
            int droppedPackets = 0;
            CUIControlsState ctrlState;
            Packet voidPkt = SerialPortIO.Me.CreateThiCmdPacket(PktType.Void);
            Packet lastNonVoidPkt = null;
            DateTime lastNonVoidPktTime = DateTime.Now;
            bool waitingForRealPackets = true;
            int nonBirdCount = 0;
            while (m_runLoop)
            {
                try
                {
                    if (!m_packets.TryDequeue(out packet))
                    {
                        if (waitingForRealPackets)
                        {
                            await Task.Delay(10);
                            continue;
                        }
                        await Task.Delay(1);
                        noPacketsCount++;
                        if (noPacketsCount > 100)
                        {
                            packet = voidPkt;
                        }
                        else
                            continue;
                    }
                    else
                    {
                        if (waitingForRealPackets)
                            LOG.I("First real packet arrived");
                        waitingForRealPackets = false;
                        if (m_packets.Count > 50)//something wrong. we have to clean up the queue
                        {
                            LOG.W("something wrong. we have to clean up the queue. Last pkt arrived at:" + lastNonVoidPktTime + "; " + lastNonVoidPkt);
                            while (m_packets.TryDequeue(out packet) && m_packets.Count > 10) ;
                        }
                        lastNonVoidPktTime = DateTime.Now;
                        lastNonVoidPkt = packet;
                    }
                    if (packet.Head.TypeOfPkt != PktType.Void)
                    {
                        pktCount++;
                        if (g_gestureIsOnChanged)
                        {
                            g_gestureIsOnChanged = false;
                            SetIMU(GestureIsOn);
                        }
                    }
                    //if (imu > 100 && SerialPortIO.Me.IsRunning || packet.Head.TypeOfPkt == PktType.Void && ElapsedSince.AsMSec(lastNonVoidPktTime) > 3000)
                    //{
                    //    LOG.E("Client is disconnected - reconnect is requried");
                    //    waitingForRealPackets = true;
                    //    await SerialPortIO.Me.TryToReconnect();
                    //}
                    noPacketsCount = 0;
                    //if (packet.Head.TypeOfPkt != PktType.Void && SerialPortIO.Me.SendingConnectingPackets)
                    //    SerialPortIO.Me.SendingConnectingPackets = false;
                    if (packet.Head.TypeOfPkt == PktType.IMU6axStatus)
                    {
                        if (m_packets.Count > 15 || ElapsedSince.AsMSec(packet.Head.m_createdAt) > 15)
                        {
                            droppedPackets++;
                            imu++;
                            continue;
                        }
                    }
                    if (packet.Head.TypeOfPkt != PktType.IMU6axStatus)
                        packet.ProcessData();
                    //Log = packet.ToString();

                    if (packet.Head.TypeOfPkt != PktType.Void)
                        pps++;
                    int devider = m_bird.Imu ? 100 : 20;
                    if (pktCount % devider == 0)
                        LOG.N(pktCount.ToString(), LogLines.PKT_COUNT);

                    if (packet.Head.m_thiId != 0xbeef)
                    {
                        if (m_thiId == 0)
                        {
                            //m_thiId = packet.Head.m_thiId;
                        }
                        birdPktsCount++;
                    }
                    if (packet.Head.TypeOfPkt == PktType.ThiPairable)
                    {
                        PktDataPairableThimble pairableThi = packet.Data as PktDataPairableThimble;
                        if (m_thiId != 0 && m_thiId == pairableThi.ParableThiId)
                        {

                            LOG.I("Pairing bird. Queue size=" + m_packets.Count);
                            Packet pairPkt = SerialPortIO.Me.CreateThiCmdPacket(PktType.CmdPairThimble);
                            byte[] ushortBytes = BitConverter.GetBytes(m_thiId);
                            pairPkt.Data[0] = ushortBytes[1];
                            pairPkt.Data[1] = ushortBytes[0];
                            Packet resp = await SerialPortIO.Me.SendRespondPkt(pairPkt, PktType.Void, 50);
                            LOG.I("Bird is paired. Queue size=" + m_packets.Count);
                        }
                    }

                    if (packet.Head.TypeOfPkt == PktType.PairedThimblesReply && packet.Data.Count > 0)
                    {
                        m_thiId = (packet.Data as PktDataPairedThimbles).GetThiId();
                        SerialPortIO.Me.ThiId = m_thiId;
                    }
                    if (packet.Head.m_thiId == m_thiId)
                    {
                        ctrlState = Packet.CtrlState;

                        if (packet.Head.m_thiId == m_thiId)
                            if (packet.Data is PktDataBtn)
                            {
                                ProcBtnsPkt(packet);
                            }
                            else if (packet.Data is PktDataStatus)
                            {
                                ProcStatPkt(packet);
                            }
                            else if (packet.Data is PktDataIMU)
                            {
                                ProcImuPkt(packet);
                            }
                            else if (packet.Data is PktDataOFN)
                            {
                                ProcOfnPkt(packet);
                            }
                    }
                    else
                    {
                        nonBirdCount++;
                    }

                    if ((DateTime.Now - logTimer).TotalMilliseconds > 1000)
                    {
                        string statStr =
                            "pps= " + pps +
                            "; bird=" + birdPktsCount +
                            "; btns= " + btns +
                            "; imu= " + imu + "/" + SerialPortIO.Me.ImuCount +
                            "; moves= " + processedImu +
                            "; dropped=" + droppedPackets;
                        ;
                        LOG.N(statStr, LogLines.STAT);
                        logTimer = DateTime.Now;
                        if (!SerialPortIO.Me.SendingConnectingPackets && pps > 0)
                        {
                            SendSyncPackets();
                        }
                        pps = 0;
                        btns = 0;
                        imu = 0;
                        processedImu = 0;
                        droppedPackets = 0;
                        birdPktsCount = 0;
                        SerialPortIO.Me.ImuCount = 0;
                        nonBirdCount = 0;
                    }
                }
                catch (Exception ex)
                {
                    LOG.E("Processing log failure: " + ex.Message);
                }
            }
        }

        private async void SendSyncPackets()
        {
            await SerialPortIO.Me.SendSyncPackets();
        }
    }
}
