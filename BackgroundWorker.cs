﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Storage;
using Windows.System.Threading;

namespace BirdUWP
{
    public class BackgroundWorkerStarter
    {
        public const string m_entryPoint = "BirdUWP.BackgroundWorker";
        public const string m_taskName = "BackgroundWorker";
        public static bool m_registered = false;
        ApplicationTrigger m_trigger = null;
        BackgroundTaskRegistration m_task = null;


        /// <summary>
        /// Register a background task with the specified taskEntryPoint, name, trigger,
        /// and condition (optional).
        /// </summary>
        /// <param name="taskEntryPoint">Task entry point for the background task.</param>
        /// <param name="name">A name for the background task.</param>
        /// <param name="trigger">The trigger for the background task.</param>
        /// <param name="condition">An optional conditional event that must be true for the task to fire.</param>
        async public void RegisterBackgroundWorker()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                if (task.Value.Name == m_taskName)
                {
                    //m_task = (BackgroundTaskRegistration)task.Value;
                    //m_trigger = (ApplicationTrigger)m_task.Trigger;
                    //UpdateBackgroundTaskRegistrationStatus(true);
                    //return;
                    task.Value.Unregister(true);
                    break;
                }
            }
            if (TaskRequiresBackgroundAccess())
            {
                // If the user denies access, the task will not run.
                var requestTask = await BackgroundExecutionManager.RequestAccessAsync();
                if (requestTask != BackgroundAccessStatus.AlwaysAllowed)
                    return;
            }

            m_trigger = new ApplicationTrigger();
            var builder = new BackgroundTaskBuilder();

            builder.Name = m_taskName;
            builder.TaskEntryPoint = m_entryPoint;
            builder.SetTrigger(m_trigger);

            //if (condition != null)
            //{
            //    builder.AddCondition(condition);

            //    //
            //    // If the condition changes while the background task is executing then it will
            //    // be canceled.
            //    //
            //    builder.CancelOnConditionLoss = true;
            //}


            try
            {
                m_task = builder.Register();
            }
            catch (Exception ex)
            {
                LOG.E(ex.Message);
            }
            UpdateBackgroundTaskRegistrationStatus(true);

            //
            // Remove previous completion status.
            //
            //var settings = ApplicationData.Current.LocalSettings;
            //settings.Values.Remove(name);
        }
        public async void RegisterAndRun()
        {
            RegisterBackgroundWorker();
            if (m_task != null)
            {
                var result = await m_trigger.RequestAsync();
                LOG.I("Run result=" + result.ToString());
            }
        }
        /// <summary>
        /// Unregister background tasks with specified name.
        /// </summary>
        /// <param name="name">Name of the background task to unregister.</param>
        public static void UnregisterBackgroundTasks()
        {
            UpdateBackgroundTaskRegistrationStatus(false);
        }


        /// <summary>
        /// Store the registration status of a background task with a given name.
        /// </summary>
        /// <param name="name">Name of background task to store registration status for.</param>
        /// <param name="registered">TRUE if registered, FALSE if unregistered.</param>
        public static void UpdateBackgroundTaskRegistrationStatus(bool registered)
        {
            m_registered = registered;
        }

        /// <summary>
        /// Get the registration / completion status of the background task with
        /// given name.
        /// </summary>
        /// <param name="name">Name of background task to retreive registration status.</param>

        /// <summary>
        /// Determine if task with given name requires background access.
        /// </summary>
        /// <param name="name">Name of background task to query background access requirement.</param>
        public static bool TaskRequiresBackgroundAccess()
        {
            return false;
        }
    }
    public sealed class BackgroundWorker : IBackgroundTask
    {
        BackgroundTaskCancellationReason _cancelReason = BackgroundTaskCancellationReason.Abort;
        volatile bool _cancelRequested = false;
        BackgroundTaskDeferral _deferral = null;
        ThreadPoolTimer _periodicTimer = null;
        uint _progress = 0;
        public static BirdClient m_birdClient = null;
        public static string m_selectedDevName = "";
        IBackgroundTaskInstance m_taskInstance = null;

        public static void Set(BirdClient birdClient, string selectedDevName)
        {
            m_selectedDevName = selectedDevName;
            m_birdClient = birdClient;
        }

        //
        // The Run method is the entry point of a background task.
        //
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            m_taskInstance = taskInstance;
            Debug.WriteLine("Background " + taskInstance.Task.Name + " Starting...");
            //
            // Query BackgroundWorkCost
            // Guidance: If BackgroundWorkCost is high, then perform only the minimum amount
            // of work in the background task and return immediately.
            //
            var cost = BackgroundWorkCost.CurrentBackgroundWorkCost;
            var settings = ApplicationData.Current.LocalSettings;
            settings.Values["BackgroundWorkCost"] = cost.ToString();

            //
            // Associate a cancellation handler with the background task.
            //
            taskInstance.Canceled += new BackgroundTaskCanceledEventHandler(OnCanceled);

            //
            // Get the deferral object from the task instance, and take a reference to the taskInstance;
            //
            _deferral = taskInstance.GetDeferral();
            m_taskInstance = taskInstance;
            m_birdClient.Connect(m_selectedDevName);

            //_periodicTimer = ThreadPoolTimer.CreatePeriodicTimer(new TimerElapsedHandler(PeriodicTimerCallback), TimeSpan.FromSeconds(1));
        }

        //
        // Handles background task cancellation.
        //
        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            //
            // Indicate that the background task is canceled.
            //
            _cancelRequested = true;
            _cancelReason = reason;

            Debug.WriteLine("Background " + sender.Task.Name + " Cancel Requested...");
        }

        //
        // Simulate the background task activity.
        //
        private void PeriodicTimerCallback(ThreadPoolTimer timer)
        {
            if ((_cancelRequested == false) && (_progress < 100))
            {
                _progress += 10;
                m_taskInstance.Progress = _progress;
            }
            else
            {
                _periodicTimer.Cancel();

                var key = m_taskInstance.Task.Name;

                //
                // Record that this background task ran.
                //
                String taskStatus = (_progress < 100) ? "Canceled with reason: " + _cancelReason.ToString() : "Completed";
                var settings = ApplicationData.Current.LocalSettings;
                settings.Values[key] = taskStatus;
                Debug.WriteLine("Background " + m_taskInstance.Task.Name + settings.Values[key]);

                //
                // Indicate that the background task has completed.
                //
                _deferral.Complete();
            }
        }
    }
}

