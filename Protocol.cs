﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.UI.Input.Preview.Injection;
using static BirdUWP.ElapsedSince;

namespace BirdUWP
{
    public enum PktType
    {
        Void = 0x0,
        Status = 0x09,
        BtnStatus = 0x01,
        IMU9axStatus = 0x02,
        OFNstatus = 0x04,
        PairedThimblesReply = 0x10,
        CmdSetPairedThiStatuses = 0x0f,
        CmdSensorLightMode = 0x17,
        CmdSetIMU = 0x20,
        AckSwVersionThimble = 0x3D,
        IMU6axStatus = 0x44,
        ThiPairable = 0x66,
        CmdPairThimble = 0x67,
        CmdGetThimbleGyroOffset = 0x6B,
        AckGetThimbleGyroOffset = 0x6C,
        AckToClient = 0xAC,
        Unhandled = 0xff
    }

    static class ElapsedSince
    {
        public static int AsMSec(DateTime from) { return (int)(DateTime.Now - from).TotalMilliseconds; }
        public static int ElapsedAsMSec(DateTime from) { return (int)(DateTime.Now - from).TotalMilliseconds; }
    }

    class ByteConverter
    {
        byte[] shortVal = new byte[2];
        public short ToShort(byte[] arr, int ind)
        {
            shortVal[0] = arr[ind + 1];
            shortVal[1] = arr[ind];
            return BitConverter.ToInt16(shortVal, 0);
        }
    }

    public class PktHead
    {
        public static short SHORT_FROM_BUFF_POS(byte[] pbuff, int pos) { return (short)(pbuff[pos + 1] | (ushort)(pbuff[pos] << 8)); }
        public static void SHORT_TO_BUFF_POS(ushort aShort, ref byte[] pbuff, int pos)
        {
            (pbuff)[(pos) + 1] = (byte)((aShort) & 0xFF);
            (pbuff)[pos] = (byte)((aShort) >> 8);
        }
        public const int CLI_PKT_HDR_MAGIC_POS = 0;
        public const int CLI_PKT_HDR_THI_UID_POS = 1;
        public const int CLI_PKT_HDR_SEQ_NUM_POS = 1 + 2;
        public const int CLI_PKT_HDR_TYPE_POS = 1 + 2 + 1;
        public const int CLI_PKT_HDR_LEN_POS = 1 + 2 + 1 + 1;
        public const int CLI_PKT_HDR_LEN = 6;
        public const int CLI_PKT_MIN_LEN = 7;
        public const int Size = 5;
        public const byte Delimeter = 0x80;
        public ushort m_thiId = 0;
        public byte m_type = 0;
        public PktType TypeOfPkt { get { return (PktType)m_type; } }
        public byte m_seqNum = 0;
        public byte m_len = 0;
        //byte[] m_buf = new byte[Size];
        static int sentCount = 0;
        public DateTime m_createdAt = new DateTime();
        public DateTime m_processedAt = new DateTime();

        public PktHead Clone()
        {
            return (PktHead)MemberwiseClone();
        }
        public int GetSeqNum() { return m_seqNum; }
        public int PacketLen { get { return m_len + CLI_PKT_MIN_LEN; } }
        public bool Parse(byte[] buf, uint size)
        {
            m_thiId = (ushort)SHORT_FROM_BUFF_POS(buf, CLI_PKT_HDR_THI_UID_POS);
            m_seqNum = buf[CLI_PKT_HDR_SEQ_NUM_POS];
            m_type = buf[CLI_PKT_HDR_TYPE_POS];
            m_len = buf[CLI_PKT_HDR_LEN_POS];
            if (m_thiId != 0xbeef) //real thimble
                if (m_type == 0x6c)
                    ;// thimble packet
            if (m_len < CLI_PKT_MIN_LEN)
            {
                Debug.WriteLine("Invalid Packet: " + ToString());
                return false;
            }
            m_len -= CLI_PKT_MIN_LEN; //data len.
            return true;
        }
        static public bool ValidateCS(byte[] buf, uint size)
        {
            byte cs = CalcCS(buf, size);
            return cs == buf[size - 1];
        }
        static byte CalcCS(byte[] buf, uint size)
        {
            byte calcedCS = 0;
            byte resByte = 0;
            for (int i = 0; i < size; i++)
            {
                resByte = calcedCS; //to skip last operation
                calcedCS ^= buf[i];
            }
            return resByte;
        }
        public byte[] ToBinary()
        {
            return ToBinary(m_type, m_len, m_thiId);
        }
        static public byte[] ToBinary(byte pktType, byte dataLen = 0, ushort targetThi = 0)
        {
            byte[] buf = new byte[dataLen + CLI_PKT_MIN_LEN];
            buf[0] = Delimeter;
            buf[CLI_PKT_HDR_SEQ_NUM_POS] = (byte)(sentCount % 256);
            buf[CLI_PKT_HDR_TYPE_POS] = pktType;
            buf[CLI_PKT_HDR_LEN_POS] = (byte)(dataLen + CLI_PKT_MIN_LEN);
            SHORT_TO_BUFF_POS(targetThi, ref buf, 1);
            return buf;
        }
        public static void SetCS(ref byte[] buf)
        {
            buf[buf.Length - 1] = CalcCS(buf, (uint)buf.Length);
        }
        public override string ToString()
        {
            return "thiID=0x" + m_thiId.ToString("x2") +
                "; seqNum=" + m_seqNum +
                "; type=0x" + m_type.ToString("x2") + "(" + TypeOfPkt.ToString() + ")" +
                "; len=" + m_len;
        }
    }
    public class PktData
    {
        protected ArrayList m_data = new ArrayList();
        static public TimeChecker m_tCheckIMUProc = new TimeChecker("ProcData");
        public bool UnhandledData { get; set; }
        public bool IsValid { get; set; }

        public PktData(bool isValid = true)
        {
            IsValid = isValid;
        }
        public static bool operator !(PktData pktData)
        {
            return !pktData.IsValid;
        }
        protected static short SHORT_FROM_BUFF_POS(List<byte> buf, int pos)
        {
            return PktHead.SHORT_FROM_BUFF_POS(buf.ToArray(), pos);
        }
        public virtual bool Parse(byte[] rawData, uint size)
        {
            UnhandledData = (rawData == null || size == PktHead.CLI_PKT_HDR_LEN ? false : true);
            return true;
        }
        protected virtual bool ProcessPacketData(Packet packet, ref CUIControlsState ctrState)
        {
            return true;
        }
        public bool ProcessPacket(Packet packet, ref CUIControlsState ctrState)
        {
            ProcessPacketData(packet, ref ctrState);
            packet.Head.m_processedAt = DateTime.Now;
            return true;
        }

        public virtual void ToBinary(ref byte[] buf)
        {
            for (int i = PktHead.CLI_PKT_HDR_LEN; i < (buf.Length - 1); i++)
            {
                buf[i] = Convert.ToByte(m_data[i - PktHead.CLI_PKT_HDR_LEN]);
            }
        }
        public byte Count { get { return (byte)m_data.Count; } }
        public Object this[int ind]
        {
            get
            {
                return ind < m_data.Count ? m_data[ind] : new Object();
            }

            set
            {
                try
                {
                    if (ind >= m_data.Count)
                        m_data.Capacity = (ind + 1);
                    //while (ind >= m_data.Count) { m_data.Add(new Object()); }
                    m_data.Insert(ind, value);
                }
                catch (Exception ex)
                {
                    LOG.E("PktData set [] " + ex.Message);
                }
            }
        }
        public virtual PktData New() { return new PktData(); }
        public virtual PktData Clone()
        {
            PktData newData = New();
            newData.m_data = new ArrayList(m_data);
            return newData;
        }
        public override string ToString()
        {
            String outStr = "";
            for (int i = 0; i < m_data.Count; i++)
            {
                outStr += (i + ") " + m_data[i].ToString() + "; ");
            }
            return outStr;
        }
    }
    //public class PKT_DATA<(Packet )
    public class PktDataBtn : PktData
    {
        public enum tBtnsStateFlags
        {
            btnf_idle = 0,
            btnf_finger_detected = 1 << 2,
            btnf_pinch_detected = 1 << 3,
            btnf_ofn_dome_pressed_detected = 1 << 4,
            btnf_left_mouse_down = 1 << 5,
            btnf_home_down = 1 << 6,
            btnf_tip_switch = 1 << 7,
        };
        public override bool Parse(byte[] rawData, uint size)
        {
            if (size <= PktHead.CLI_PKT_MIN_LEN)
                return false;
            this[0] = rawData[PktHead.CLI_PKT_HDR_LEN];
            return true;
        }
        public override PktData New() { return new PktDataBtn(); }
        bool ALL_BITS_ON(byte val, tBtnsStateFlags bm)
        {
            return ((val) & ((byte)bm)) == ((byte)bm);
        }
        public byte GetBtnVal() { return (byte)this[0]; }
        public bool IsHomeDetected() { return ALL_BITS_ON(GetBtnVal(), tBtnsStateFlags.btnf_home_down); }
        public bool IsLeftMouseDetected() { return ALL_BITS_ON(GetBtnVal(), tBtnsStateFlags.btnf_left_mouse_down); }
        public bool IsTipSwitchDetected() { return ALL_BITS_ON(GetBtnVal(), tBtnsStateFlags.btnf_tip_switch); }
        public bool IsPinchDetected() { return ALL_BITS_ON(GetBtnVal(), tBtnsStateFlags.btnf_pinch_detected); }
        public bool IsFingerDetected() { return ALL_BITS_ON(GetBtnVal(), tBtnsStateFlags.btnf_finger_detected); }

        protected override bool ProcessPacketData(Packet packet, ref CUIControlsState ctrState)
        {
            ctrState.Btn(BtnType.Finger).CurState = IsFingerDetected() ? Button.State.Pressed : Button.State.Idle;
            ctrState.Btn(BtnType.Home).CurState = IsHomeDetected() ? Button.State.Pressed : Button.State.Idle;
            ctrState.Btn(BtnType.Pinch).CurState = IsPinchDetected() ? Button.State.Pressed : Button.State.Idle;
            ctrState.Btn(BtnType.Switch).CurState = IsTipSwitchDetected() ? Button.State.Pressed : Button.State.Idle;
            return true;
        }
    }
    public class PktDataStatus : PktData
    {
        /*
        Packet type "0x09 - “Thimble status” packet- no ack"
        direction: Thi->Sens->Client

        The packet payload :
        1 bit - thimble mode(touch / fly)
        1 bit - multithimble mode (active/waiting) (from thimble version 4.11.21)
        1 bit - finger present
        2 bits - gestures mode(6 axes, 9 axes, off)
        3 bits - buttery level(1 low - 5 high; 0 - empty)


        Bit 7		Bit 6		Bit 5		Bit4		Bit 3		Bit 2		Bit 1			Bit 0
        Battery		Battery		Battery		Gestures   Gestures		Finger		MultThimble		Thimble
        level		level		level		mode		mode		present		 mode			mode
        */
        public enum GestMode { Off, Axis6, Axis9 };
        public enum ThiMode { basic, fly, air_mouse };

        public override bool Parse(byte[] rawData, uint size)
        {
            if (size <= PktHead.CLI_PKT_MIN_LEN)
                return false;
            this[0] = rawData[PktHead.CLI_PKT_HDR_LEN];
            return true;
        }
        public override PktData New() { return new PktDataStatus(); }

        byte GetVal() { return (byte)this[0]; }
        ThiMode Mode()
        {

            ThiMode mode = ((GetVal() & 0x01) > 0) ? ThiMode.air_mouse : ThiMode.basic;
            return mode;
        }
        public bool IsActiveMultiThimble()
        {
            return (GetVal() & 0x02) > 0 ? true : false;
        }
        public bool FingerPresent() { return (GetVal() & 0x04) > 0; }
        public GestMode GesturesMode()
        {
            switch ((GetVal() & 0x18) >> 3)
            {
                case 1: return GestMode.Axis6;
                case 2: return GestMode.Axis9;
            }
            return GestMode.Off;
        }
        public bool GesturesOn() { return GesturesMode() != GestMode.Off; }
        public int BattLevel() { return (GetVal() >> 5) & 0x7; }
        public override string ToString()
        {
            return
                "mode=" + Mode() +
                "; finger=" + (FingerPresent() ? "yes" : "no") +
                "; gest=" + GesturesMode() +
                "; battLvl=" + BattLevel();
        }
        protected override bool ProcessPacketData(Packet packet, ref CUIControlsState ctrState)
        {
            ctrState.Btn(BtnType.Finger).CurState = FingerPresent() ? Button.State.Pressed : Button.State.Idle;
            return true;
        }
    }
    public class PktDataIMU : PktData
    {
        static ImuInterpreter m_imuInterpreter = new ImuInterpreter();
        static CGestureIntpr m_gestIntepreter = null;
        static public ImuInterpreter Interpreter { get { return m_imuInterpreter; } }
        static public bool IsIMUOffsetsRequested { get { return ImuInterpreter.IsIMUCalibValid(); } }

        static int i = 0;
        static short[,] data = new short[8, 6]
{
        { -96, 7652, 15532, -771, -369, -643 },
        //{ 96, 7652, -15532, 771, -369, 643 },
        { 100, 7572, 15244, -687, -256, -572 },
        { 228, 7536, 15344, -595, -107, -488 },
        { 188, 7404, 15268, -454, 21, -383 },
        { 192, 7412, 15132, -348, 135, -361 },
        { 12, 7480, 15172,  -218, 268, -319 },
        { -80, 7520, 15312, -152, 353, -318 },
        { -216, 7688, 15320, - 51, 368, -290 }

};
        public static void ReinitInterpreter()
        {
            m_imuInterpreter = new ImuInterpreter();
        }
        static int GetNextImuDataInd()
        {
            if (i == 8)
                i = 0;
            int j = i;
            i++;
            return 0;// j;
        }
        static bool isTestMode = false;
        public override bool Parse(byte[] rawData, uint size)
        {
            try
            {
                ByteConverter byteConverter = new ByteConverter();
                if (size <= (6 * sizeof(short) + PktHead.CLI_PKT_HDR_LEN))
                    return false;
                if (isTestMode)
                {
                    int ind = GetNextImuDataInd();
                    for (int i = 0; i < 6; i++)
                        this[i] = (short)data[ind, i];
                }
                else
                {
                    for (int i = PktHead.CLI_PKT_HDR_LEN; i < size; i += sizeof(short))
                        this[(i - PktHead.CLI_PKT_HDR_LEN) / sizeof(short)] = byteConverter.ToShort(rawData, i);
                }
                return true;
            }
            catch (Exception ex)
            {
                LOG.E("Parsing IMU data failure: " + ex.Message);
            }
            return false;
        }
        public override string ToString()
        {
            return
                "GyroX=" + GetGyroX() +
                ";GyroY=" + GetGyroY() +
                ";Gyro=" + GetGyroY();
        }
        static bool initGest = true;
        protected override bool ProcessPacketData(Packet packet, ref CUIControlsState ctrState)
        {
            AdjustPacketToHWVersion();
            Rotate180AroundY();
            m_tCheckIMUProc.Start(); //for profiling purposes only
            if (isTestMode)
            {
                string str = "imu: ";
                foreach (Object obj in m_data)
                    str += Convert.ToInt16(obj).ToString() + "; ";
                Debug.WriteLine(str);
            }
            bool retVal = m_imuInterpreter.ProcessPacket(packet, ref ctrState) == 0;
            if (initGest && m_gestIntepreter == null)
                m_gestIntepreter = new CGestureIntpr();
            if (m_gestIntepreter != null && PktProcessor.GestureIsOn)
                ctrState.SetGesture(m_gestIntepreter.OnUIState(ctrState));
            if (isTestMode)
            {
                Debug.WriteLine("vel=" + ctrState.m_angularVelocity.ToString());
            }
            //m_tCheckIMUProc.AddSample();//for profiling purposes only
            return true;
        }


        void Rotate180AroundY()
        {
            AcceleroX = (short)(-1 * AcceleroX);
            AcceleroZ = (short)(-1 * AcceleroZ);
            GyroX = (short)(-1 * GyroX);
            GyroZ = (short)(-1 * GyroZ);
        }

        static int prev_gyroX = 0, pprev_gyroX = 0, prev_gyroY = 0, pprev_gyroY = 0, prev_gyroZ = 0, pprev_gyroZ = 0;

        void AdjustPacketToHWVersion()
        {
            /*
                The axes we assume in this app: 
                - assume Bird's nose is toward the positive Y axis (below: facing the viewer)
                - yes.. it's a left-handed axis system - this is what we get from the IMU

                            Z
                            |
                            |
                            |_________ X
                            \
                             \
                              Y

                if this is not the case, we must correct it here.
            */

            int gyroX = GyroX;
            int gyroY = GyroY;
            int gyroZ = GyroZ;


            // handle negative saturation >> for values under -32K, the gyro returns positive value 0x7F00+
            const int minShort = Int16.MinValue + 1;

            const int wraparoundVal = 0x7F00;
            // if prev val (or the one before) was considerably negative, and this val is >7f00, it's a wrap-around
            if ((pprev_gyroX < minShort / 3 || prev_gyroX < minShort / 3) && gyroX >= wraparoundVal)
            {

                //LOGV("switch " << LOG_ARG(gyroX) << " " << LOG_ARG(pprev_gyroX) << LOG_ARG(prev_gyroX));
                GyroX = minShort;
            }
            if ((pprev_gyroY < minShort / 3 || prev_gyroY < minShort / 3) && gyroY >= wraparoundVal)
            {

                //LOGV("switch " << LOG_ARG(gyroY) << " " << LOG_ARG(pprev_gyroY) << LOG_ARG(prev_gyroY));
                GyroY = minShort;
            }
            if ((pprev_gyroZ < minShort / 3 || prev_gyroZ < minShort / 3) && gyroZ >= wraparoundVal)
            {

                //LOGV("switch " << LOG_ARG(gyroZ) << " " << LOG_ARG(pprev_gyroZ) << LOG_ARG(prev_gyroZ));
                GyroZ = minShort;
            }
            // remember far history
            pprev_gyroX = prev_gyroX; pprev_gyroY = prev_gyroY; pprev_gyroZ = prev_gyroZ;
            // remember near history
            prev_gyroX = GyroX; prev_gyroY = GyroY; prev_gyroZ = GyroZ;

            ///LOG(" gyroX=" << gyroX << " gyroY=" << gyroY << " gyroZ=" << gyroZ);
        }
        public PktDataIMU(bool isValid = true) : base(isValid) { }
        public override PktData New() { return new PktDataIMU(); }
        public bool IsValidAccelero() { return GetAcceleroX() != 0 || GetAcceleroY() != 0 || GetAcceleroZ() != 0; }
        public bool IsValidGyro() { return GetGyroX() != 0 || GetGyroY() != 0 || GetGyroZ() != 0; }
        public bool HasMagnetoData() { return false; }
        public short GetAcceleroX() { return Convert.ToInt16(this[0]); }
        public short GetAcceleroY() { return Convert.ToInt16(this[1]); }
        public short GetAcceleroZ() { return Convert.ToInt16(this[2]); }
        public short GetGyroX() { return Convert.ToInt16(this[3]); }
        public short GetGyroY() { return Convert.ToInt16(this[4]); }
        public short GetGyroZ() { return Convert.ToInt16(this[5]); }
        public short AcceleroX { get { return Convert.ToInt16(m_data[0]); } set { m_data[0] = value; } }
        public short AcceleroY { get { return Convert.ToInt16(m_data[1]); } set { m_data[1] = value; } }
        public short AcceleroZ { get { return Convert.ToInt16(m_data[2]); } set { m_data[2] = value; } }
        public short GyroX { get { return Convert.ToInt16(m_data[3]); } set { m_data[3] = value; } }
        public short GyroY { get { return Convert.ToInt16(m_data[4]); } set { m_data[4] = value; } }
        public short GyroZ { get { return Convert.ToInt16(m_data[5]); } set { m_data[5] = value; } }
        public short GetMagnetoX() { return Convert.ToInt16(this[6]); }
        public short GetMagnetoY() { return Convert.ToInt16(this[7]); }
        public short GetMagnetoZ() { return Convert.ToInt16(this[8]); }
        public static PktData AverageTo(PktDataIMU pkt1, PktDataIMU pkt2)
        {
            // set us to the average with pkt (IMU quantitative values)
            PktData res = pkt1.New();
            //PktDataIMU* pRes = PKT_DATA_PTR(res, PktDataIMU);

            res[0] = (short)((pkt1.GetAcceleroX() + pkt2.GetAcceleroX()) / 2);
            res[1] = (short)((pkt1.GetAcceleroY() + pkt2.GetAcceleroY()) / 2);
            res[2] = (short)((pkt1.GetAcceleroZ() + pkt2.GetAcceleroZ()) / 2);
            res[3] = (short)((pkt1.GetGyroX() + pkt2.GetGyroX()) / 2);
            res[4] = (short)((pkt1.GetGyroY() + pkt2.GetGyroY()) / 2);
            res[5] = (short)((pkt1.GetGyroZ() + pkt2.GetGyroZ()) / 2);

            //if (pkt1->HasMagnetoData())
            //{
            //    pRes->m_hasMagnetoData = true;
            //    pRes->MagnetoX() = SHORT(((int)pkt1->GetMagnetoX() + (int)pkt2->GetMagnetoX()) / 2);
            //    pRes->MagnetoY() = SHORT(((int)pkt1->GetMagnetoY() + (int)pkt2->GetMagnetoY()) / 2);
            //    pRes->MagnetoZ() = SHORT(((int)pkt1->GetMagnetoZ() + (int)pkt2->GetMagnetoZ()) / 2);
            //}
            return res;

        }

    }

    public class PktDataGyroOffsets : PktData
    {
        public override PktData New() { return new PktDataGyroOffsets(); }
        public override bool Parse(byte[] rawData, uint size)
        {
            if (size <= (3 * sizeof(short) + PktHead.CLI_PKT_HDR_LEN))
                return false;
            for (int i = PktHead.CLI_PKT_HDR_LEN; i < size; i += sizeof(short))
                this[(i - PktHead.CLI_PKT_HDR_LEN) / sizeof(short)] = PktHead.SHORT_FROM_BUFF_POS(rawData, i);
            return true;
        }
        protected override bool ProcessPacketData(Packet packet, ref CUIControlsState ctrState)
        {
            ImuInterpreter.SaveGyroOffsetIntoRam(
                Convert.ToInt16(this[0]),
                Convert.ToInt16(this[1]),
                Convert.ToInt16(this[2])
                );
            return true;
        }

    }

    public class PktDataOFN : PktData
    {
        public override PktData New() { return new PktDataOFN(); }
        public override bool Parse(byte[] rawData, uint size)
        {
            this[0] = (int)(sbyte)rawData[PktHead.CLI_PKT_HDR_LEN];
            this[1] = (int)(sbyte)rawData[PktHead.CLI_PKT_HDR_LEN + 1];
            this[2] = rawData[PktHead.CLI_PKT_HDR_LEN + 2];
            return true;
        }
        public int X { get { return Convert.ToInt32(this[0]); } }
        public int Y { get { return Convert.ToInt32(this[1]); } }
        public byte Flags { get { return Convert.ToByte(this[2]); } }

        protected override bool ProcessPacketData(Packet packet, ref CUIControlsState ctrState)
        {
            PktDataOFN ofnData = packet.Data as PktDataOFN;
            ctrState.m_OFN.SetFlags(ofnData.Flags);
            Point coors = new Point(X, Y);
            ctrState.m_OFN.SetCoors(coors.Rot90());
            return true;
        }
    }
    public class PktDataPairedThimbles : PktData
    {
        public override PktData New() { return new PktDataPairedThimbles(); }
        public override bool Parse(byte[] rawData, uint size)
        {
            uint n = (size - PktHead.CLI_PKT_HDR_LEN) / sizeof(short);
            for (int i = PktHead.CLI_PKT_HDR_LEN; i < size; i += sizeof(short))
                this[(i - PktHead.CLI_PKT_HDR_LEN) / sizeof(short)] = PktHead.SHORT_FROM_BUFF_POS(rawData, i);
            return true;
        }
        public ushort GetThiId(int ind = 0)
        {
            return ind < Count ? Convert.ToUInt16(this[ind]) : (ushort)0;
        }
    }

    public class PktDataPairableThimble : PktData
    {
        public override PktData New() { return new PktDataPairableThimble(); }
        public override bool Parse(byte[] rawData, uint size)
        {
            this[0] = PktHead.SHORT_FROM_BUFF_POS(rawData, PktHead.CLI_PKT_HDR_LEN);
            return true;
        }
        public ushort ParableThiId { get { return Convert.ToUInt16(this[0]); } }
    }
    public class Packet
    {
        static Dictionary<PktType, PktData> m_pktDataFactory = new Dictionary<PktType, PktData>();
        public PktHead Head { get; set; }
        public PktData Data { get; set; }
        static CUIControlsState m_ctrlState = new CUIControlsState();
        static public CUIControlsState CtrlState
        {
            get { return m_ctrlState; }
            set { m_ctrlState = value; }
        }

        static Packet() { RegisterDataTypes(); }

        public Packet()
        {
            Head = new PktHead();
            Data = null;
        }
        public Packet(Packet pkt)
        {
            Head = pkt.Head.Clone();
            Data = pkt.Data == null ? null : pkt.Data.Clone();
        }

        public Packet(PktType pktType)
        {
            Head = new PktHead();
            Data = CreatePktData(pktType);
        }
        public Packet Duplicate()
        {
            return new Packet(this);
        }
        public bool ParseData(byte[] rawData, uint size)
        {
            //if (!Head.Parse(rawData, size))
            //    return false;
            Head.m_createdAt = DateTime.Now;
            Data = CreatePktData(Head.TypeOfPkt);
            Data.UnhandledData = false;
            Data.IsValid = Data.Parse(rawData, size);
            //if (!(Data is PktDataIMU)) //don't process every IMU packet 
            //Data.ProcessPacket(this, ref m_ctrlState);
            return Data.IsValid;
        }
        public bool ProcessData()
        {
            bool res = Data.ProcessPacket(this, ref m_ctrlState);
            Head.m_processedAt = DateTime.Now;
            return res;
        }
        public override string ToString()
        {
            return "Head=" + Head.TypeOfPkt.ToString() + "; Data=" + Data.ToString();
        }
        public byte[] ToBinary()
        {
            Head.m_len = Data.Count;
            byte[] resBuf = Head.ToBinary();
            Data.ToBinary(ref resBuf);
            PktHead.SetCS(ref resBuf);
            return resBuf;
        }
        static void RegisterDataTypes()
        {
            if (m_pktDataFactory.Count > 0)
                return;
            m_pktDataFactory[PktType.IMU6axStatus] = new PktDataIMU();
            m_pktDataFactory[PktType.IMU9axStatus] = new PktDataIMU();
            m_pktDataFactory[PktType.BtnStatus] = new PktDataBtn();
            m_pktDataFactory[PktType.Status] = new PktDataStatus();
            m_pktDataFactory[PktType.AckGetThimbleGyroOffset] = new PktDataGyroOffsets();
            m_pktDataFactory[PktType.ThiPairable] = new PktDataPairableThimble();
            m_pktDataFactory[PktType.PairedThimblesReply] = new PktDataPairedThimbles();
            m_pktDataFactory[PktType.OFNstatus] = new PktDataOFN();
        }
        public PktData CreatePktData(PktType pktType)
        {
            if (m_pktDataFactory.ContainsKey(pktType))
                return m_pktDataFactory[pktType].New();
            return new PktData();
        }
    }
    public struct tOrientPRY
    {
        public float pitchAngle;
        public float rollAngle;
        public float yawAngle;
    }
    public enum BtnType { Finger, Home, Pinch, Switch };
    public class Button
    {
        public enum State { Idle, Pressed };
        State m_curState = State.Idle;
        DateTime m_lastState = new DateTime();
        DateTime m_lastChange = new DateTime();

        public Button(BtnType btnType)
        {
            Type = btnType;
            IsChanged = false;
            PrevState = State.Idle;
        }

        public BtnType Type { get; set; }
        public State CurState
        {
            get
            {
                return m_curState;
            }
            set
            {
                m_lastState = DateTime.Now;
                PrevState = m_curState;
                m_curState = value;
                IsChanged = (m_curState != PrevState);
                if (IsChanged)
                    m_lastChange = m_lastState;
            }
        }
        public void Idle() { PrevState = State.Idle; CurState = State.Idle; }
        public double ElapsedSinceLastSet() { return (DateTime.Now - m_lastState).TotalMilliseconds; }
        public double ElapsedSinceLastChange() { return (DateTime.Now - m_lastChange).TotalMilliseconds; }
        public void Check()
        {
            if (IsPressed && ElapsedSinceLastSet() > 500)
                Idle();
        }
        private State PrevState { get; set; }
        public bool IsChanged { set; get; }
        public bool IsPressed { get { return CurState == State.Pressed; } }
        public bool IsStarted { get { return IsChanged && IsPressed; } }
        public bool IsEnded { get { return IsChanged && !IsPressed; } }
    }
    public class OFN
    {
        [Flags]
        enum OfnStatus
        {

            fing_present_bit = 1 << 7,
            tap_hold_bit = 1 << 6,
            dblclk_bit = 1 << 5,
            click_bit = 1 << 4,
            tap_tap_and_hold_bit = 1 << 3
        }
        class MoveSess
        {
            public enum Dir {None, X, Y};
            int m_shift = 0;
            Dir m_dir = Dir.None;
            uint m_count = 0;

            public void Reset() { m_shift = 0; m_dir = Dir.None; m_count = 0; }
            public void SetCoors(Data prevData, Data curData)
            {
                if (ElapsedAsMSec(prevData.m_flagsTimestamp) > 300)
                    Reset();
                if (m_dir != Dir.None)
                {
                    int curShift = GetShift(curData.m_dCoors);
                    if ((curShift > 0) != (m_shift > 0))
                        Reset();
                    else
                    {
                        m_shift += curShift;
                        m_count++;
                    }
                }
                if (m_dir == Dir.None)
                {
                    m_dir = curData.m_dCoors.x != 0 ? Dir.X : (curData.m_dCoors.y != 0 ? Dir.Y : Dir.None);
                    m_shift = GetShift(curData.m_dCoors);
                    m_count++;
                }
            }
            int GetShift(Point coors)
            {
                if (m_dir != Dir.None)
                    return m_dir == Dir.X ? coors.x : coors.y;
                return 0;
            }
            public int GetTotalShift(out Dir dir, out uint count)
            {
                dir = m_dir;
                count = m_count;
                return m_shift;
            }
        }
        class Data
        {
            public static bool operator ==(Data d1, Data d2)
            {
                return

                    d1.m_btnFlags == d2.m_btnFlags &&
                    d1.m_dCoors == d2.m_dCoors;
            }
            public static bool operator !=(Data d1, Data d2)
            {
                return !(d1 == d2);
            }
            public byte m_btnFlags = 0;    // state of tap/button
            public DateTime m_flagsTimestamp;
            public Point m_dCoors = new Point(0, 0);  // coors of finger movement
            public bool m_tapTapAndHoldOcuured = false;
            public bool m_tapAndHoldOcuured = false;
        }
        Data data = new Data();
        Data prev_data = new Data();
        DateTime m_lastCoorsSet;
        DateTime m_lastChange;
        static int maxGapForTap_TapAndHold_Event_mili_Sec = 800;


        bool ALL_BITS_ON(byte val, OfnStatus bm) { return (((OfnStatus)val) & bm) == bm; }
        void SET_BIT_MASK(byte p, OfnStatus n) { p = (byte)((OfnStatus)p | n); }
        //	OFN() : data{}, prev_data{} {}
        //	bool operator ==(OFN anotherOFN)  {
        //		return data == anotherOFN.data;
        //	}
        public void Stable()
        {
            // remove one-time events
            //data.m_btnFlags &= ~(OfnStatus.click_bit & OfnStatus.dblclk_bit);
            // forget changes
            prev_data = data;
        }
        //void HandleStateTimeouts() { }
        public void SetIdle()
        {
            prev_data = data;
            data = new Data();
            m_moveSess.Reset();
        }
        public bool IsIdle()
        {
            return data == new Data();
        }
        public bool IsOFNFingerPresent()
        {
            return ALL_BITS_ON(data.m_btnFlags, OfnStatus.fing_present_bit);
        }
        public bool IsOFNClickDetected()
        {
            return ALL_BITS_ON(data.m_btnFlags, OfnStatus.click_bit);
        }
        public bool IsOFNDoubleclickDetected()
        {
            return ALL_BITS_ON(data.m_btnFlags, OfnStatus.dblclk_bit);
        }
        public bool IsOFNTapAndHoldDetected()
        {
            return ALL_BITS_ON(data.m_btnFlags, OfnStatus.tap_hold_bit);
        }
        public bool IsOFNTapTapAndHoldDetected()
        {
            return ALL_BITS_ON(data.m_btnFlags, OfnStatus.tap_tap_and_hold_bit);
        }
        public bool IsOFNTapAndHoldStarted()
        {
            return (data.m_tapAndHoldOcuured && !prev_data.m_tapAndHoldOcuured);
        }
        public bool IsOFNTapTapAndHoldStarted()
        {
            return (data.m_tapTapAndHoldOcuured && !prev_data.m_tapTapAndHoldOcuured);
        }
        public void SetFlags(byte btnFlags)
        {
            m_lastChange = DateTime.Now;
            prev_data.m_btnFlags = data.m_btnFlags;
            prev_data.m_tapAndHoldOcuured = data.m_tapAndHoldOcuured;
            prev_data.m_tapTapAndHoldOcuured = data.m_tapTapAndHoldOcuured;
            prev_data.m_flagsTimestamp = data.m_flagsTimestamp;
            //LOG("OFN flags input = " << std::bitset < 8 > (btnFlags) << " " << LOG_ARG(prev_data.m_tapTapAndHoldOcuured) << " " << LOG_ARG(prev_data.m_tapAndHoldOcuured));

            data.m_flagsTimestamp = DateTime.Now;
            data.m_btnFlags = (byte)(btnFlags & (0xF0));
            data.m_tapAndHoldOcuured = false;
            data.m_tapTapAndHoldOcuured = false;

            if (ALL_BITS_ON(data.m_btnFlags, OfnStatus.tap_hold_bit))
            {
                //btnFlags |= OfnStatus.fing_present_bit; //Temporary. It has to be done on the thimble side
                SET_BIT_MASK(data.m_btnFlags, OfnStatus.fing_present_bit);
                data.m_tapAndHoldOcuured = true;
            }



            if (ALL_BITS_ON(data.m_btnFlags, OfnStatus.fing_present_bit))
            {
                if (prev_data.m_tapAndHoldOcuured)
                {
                    SET_BIT_MASK(data.m_btnFlags, OfnStatus.tap_hold_bit);
                    data.m_tapAndHoldOcuured = true;
                }

                if (prev_data.m_tapTapAndHoldOcuured)
                {
                    SET_BIT_MASK(data.m_btnFlags, OfnStatus.tap_tap_and_hold_bit);
                    data.m_tapTapAndHoldOcuured = true;

                }
                else
                {
                    if (ALL_BITS_ON(prev_data.m_btnFlags, OfnStatus.click_bit) &&
                        //	ALL_BITS_ON(data.m_btnFlags, OfnStatus.fing_present_bit) &&
                        ElapsedSince.AsMSec(prev_data.m_flagsTimestamp) <= maxGapForTap_TapAndHold_Event_mili_Sec)
                    {
                        SET_BIT_MASK(data.m_btnFlags, OfnStatus.tap_tap_and_hold_bit);
                        data.m_tapTapAndHoldOcuured = true;
                    }
                }
            }

            //LOG("OFN flags output = " << std::bitset < 8 > (data.m_btnFlags) << " " << LOG_ARG(data.m_tapTapAndHoldOcuured) << " " << LOG_ARG(data.m_tapAndHoldOcuured));
        }
        public byte GetFlags()
        {
            return data.m_btnFlags;
        }
        MoveSess m_moveSess = new MoveSess();
        public void SetCoors(Point dCoors)
        {
            ///		auto dCoors = _dCoors *3; // #gr temp
            prev_data.m_dCoors = data.m_dCoors;
            data.m_dCoors = dCoors;
            m_moveSess.SetCoors(prev_data, data);
            m_lastCoorsSet = DateTime.Now;
            m_lastChange = DateTime.Now;
        }
        public int GetDownMovement( out uint count)
        {
            MoveSess.Dir dir = MoveSess.Dir.None;
            int shift = m_moveSess.GetTotalShift(out dir, out count);
            if (shift < 0 && dir == MoveSess.Dir.Y)
                return Math.Abs(shift);
            return 0;
        }
        public uint ElapsedSinceLastCoorsSet()
        {
            return (uint)ElapsedSince.AsMSec(m_lastCoorsSet);
        }
        public uint ElapsedSinceLastChange()
        {
            return (uint)ElapsedSince.AsMSec(m_lastChange);
        }
        public Point GetCoors()
        {
            return data.m_dCoors;
        }
        public bool IsBtnStateChanged()
        {
            return prev_data.m_btnFlags != data.m_btnFlags;
        }
        public bool IsBtnStateChanged(byte bitFlag)
        {
            return ALL_BITS_ON(data.m_btnFlags, (OfnStatus)bitFlag) != ALL_BITS_ON(prev_data.m_btnFlags, (OfnStatus)bitFlag);
        }
        public bool IsCoorsStateChanged()
        {
            return prev_data.m_dCoors != data.m_dCoors;
        }
        public bool IsCoorsDirRight()
        {
            return GetCoors().x > 0;
        }
        public bool IsCoorsDirUp()
        {
            return GetCoors().y > 0;
        }
    }

    public class CUIControlsState
    {
        GestureId m_prev_gest = GestureId.gestId_invalid;
        GestureId m_gest = GestureId.gestId_invalid;
        public CVector m_velocity = new CVector(); //	thimble velocity 
        public CVector m_angularVelocity = new CVector();  //	thimble angular velocity 
        public CVector m_orient = new CVector();    //	thimble orientation (vectorial)
        public tOrientPRY m_orientPRY = new tOrientPRY();
        public double m_IMUsamplePeriodMSec = 0;
        public CQuaternion m_orientQ = new CQuaternion();
        public OFN m_OFN = new OFN();
        Button[] m_buttons = new Button[] {
            new Button(BtnType.Finger),
            new Button(BtnType.Home),
            new Button(BtnType.Pinch),
            new Button(BtnType.Switch)
        };
        public Button Btn(int ind)
        {
            return m_buttons[ind];
        }
        public Button Btn(BtnType btnType)
        {
            return m_buttons[(int)btnType];
        }
        public GestureId GetGesture()
        {
            return m_gest;
        }
        public void SetGesture(GestureId gest)
        {
            m_prev_gest = m_gest;
            m_gest = gest;
        }
        public bool IsGestureChanged()
        {
            return m_prev_gest != m_gest;
        }
    }

    public class UIActions
    {
        static public void OnPacket(Packet packet)
        {
            if (packet.Data is PktDataBtn)
                OnButtonPacket(packet);
            else if (packet.Data is PktDataIMU)
                OnImuPacket(packet);
        }
        static void OnImuPacket(Packet packet)
        {

        }
        static void OnButtonPacket(Packet packet)
        {
            if (Packet.CtrlState.Btn(BtnType.Switch).IsChanged)
            {
                InjectMouseAction(Packet.CtrlState.Btn(BtnType.Switch).IsPressed ?
                    InjectedInputMouseOptions.LeftDown :
                    InjectedInputMouseOptions.LeftUp);
            }
        }
        static InputInjector m_injector = null;
        static InputInjector Injector
        {
            get
            {
                if (m_injector == null)
                    m_injector = InputInjector.TryCreate();
                return m_injector;
            }
        }
        static public async void InjectMouseClick()
        {
            try
            {
                if (InjectMouseAction(InjectedInputMouseOptions.LeftDown))
                {
                    await Task.Delay(50);
                    InjectMouseAction(InjectedInputMouseOptions.LeftUp);
                }
            }
            catch (Exception ex)
            {
                LOG.E(ex.Message);
            }
        }
        static public bool InjectMouseAction(InjectedInputMouseOptions act)
        {
            try
            {
                InjectedInputMouseInfo info = new InjectedInputMouseInfo();
                List<InjectedInputMouseInfo> infoArr = new List<InjectedInputMouseInfo>();
                info.MouseOptions = act;
                infoArr.Add(info);
                Injector.InjectMouseInput(infoArr);
                return true;
            }
            catch (Exception ex)
            {
                LOG.E(ex.Message);
                return false;
            }

        }

        class Protocol
        {
        }
    }
}
