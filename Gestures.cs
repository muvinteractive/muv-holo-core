﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using static BirdUWP.StaticDefs;
using static BirdUWP.ThrowHelper;

namespace BirdUWP
{
    /******************************************************************************
	Class:	CGesture

	- a gesture here is defined as:	
		A set of IMU parameters where each parameter conforms to a specific value pattern
	- gesture should be detected long enough to be recognize as such
	- we'll examine the following IMU parameters (as fuzzy sets):
		- tilt of the hand (from g)
		- velocity of rotation (gyro)

*******************************************************************************/
    public enum LingVal
    {
        NO_MOVE, LEFT_SLOW, LEFT_FAST, RIGHT_SLOW, RIGHT_FAST, UP_SLOW, UP_FAST, DOWN_SLOW, DOWN_FAST, MID, LEFT, RIGHT,
        UP, DOWN, FRONT, BACK, SLIGHT_LEFT, SLIGHT_RIGHT, SLIGHT_UP, SLIGHT_DOWN, SLIGHT_FRONT, SLIGHT_BACK,
    };

    public enum ParamType { VELOC_YAW, VELOC_PITCH, VELOC_ROLL, ORIENT_LEFT_RIGHT, ORIENT_UP_DOWN, ORIENT_FRONT_BACK, };
    public enum GestureId
    {
        gestId_left,
        gestId_right,
        gestId_up,
        gestId_down,
        gestId_come,
        gestId_go,
        gestId_prayAndPush,
        gestId_pullAnArrow,
        gestId_pullBowBack,
        gestId_reloadAGun,
        gestId___num,
        gestId_invalid = gestId___num,
    }

    public abstract class CGesture
    {
        protected struct Cvals
        {
            static public double gyr_max_val = 100.0;
            static public double orient_max_val = 1.0;
            static public double ref_prd_msec = 10.0;
            static public double slideleft_RC = 180;///250;		
            static public double slideright_RC = 300;
            static public double slideup_RC = 100;///200;
            static public double slidedown_RC = 150;///200;
            static public double default_RC = 200;
            static public double pnp_RC = 120;
            static public double pullAnArrow_RC = 300;
        }

        public struct truthVal
        {
            double val;
            public truthVal(double v = 0)
            {
                val = v;
                // class invariant
                EST_INVARIANT_THROW_RTE(val >= 0 && val <= 1.0);
            }
            static public implicit operator double(truthVal tVal) { return tVal.val; }
            static public implicit operator truthVal(double dVal) { return new truthVal(dVal); }
            public truthVal And(truthVal val2)
            {
                return new truthVal(Math.Min(val, (double)val2));
            }
            public truthVal Or(truthVal val2)
            {
                return new truthVal(Math.Max(val, (double)val2));
            }
            static public truthVal operator &(truthVal val1, truthVal val2)
            {
                return val1.And(val2);
            }
            static public truthVal operator |(truthVal val1, truthVal val2)
            {

                return val1.Or(val2);
            }
            static public truthVal operator !(truthVal tVal)
            {
                return new truthVal(1.0 - tVal.val);
            }
        };

        // define the FLS's liguistic values

        public class MembershipFunc
        {
            /*
                A general trapeziod membership function

                1|  vx1 _____ vx2
                 |     /     \
                0|____/_______\__
                 |  min       max
                */
            // define the vertices 
            double m_vxMin;
            double m_vx1;
            double m_vx2;
            double m_vxMax;

            bool m_bRads = false;



            public MembershipFunc(double vxMin, double vx1, double vx2, double vxMax)
            {
                m_vxMin = vxMin;
                m_vx1 = vx1;
                m_vx2 = vx2;
                m_vxMax = vxMax;
                // establish class invariant
                EST_INVARIANT_THROW_RTE(m_vxMin <= m_vx1 && m_vx1 <= m_vx2 && m_vx2 <= m_vxMax);
            }

            public double Calc(double inp)
            {
                if (m_bRads)
                {
                    // cyclic func
                    if (inp < m_vxMin)
                    {
                        inp += cvals.rad2PI; // move to second 2PI cycle
                    }
                }
                if (inp <= m_vxMin)
                    return 0.0;
                if (inp < m_vx1)
                    return (inp - m_vxMin) / (m_vx1 - m_vxMin);
                if (inp <= m_vx2)
                    return 1.0;
                if (inp < m_vxMax)
                    return (m_vxMax - inp) / (m_vxMax - m_vx2);
                return 0;
            }
            public MembershipFunc Mirror()
            {
                return new MembershipFunc(-m_vxMax, -m_vx2, -m_vx1, -m_vxMin);
            }
        }

        protected CExpFilter m_pTimeAverFilt; // average value of this gesture over time
        protected class FuzzySet : Dictionary<LingVal, MembershipFunc> { }

        protected Dictionary<ParamType, FuzzySet> m_params = new Dictionary<ParamType, FuzzySet>();
        protected double m_gestureSensFact = 1.0;
        protected bool m_bEnabled = true;

        protected truthVal VELOC_PITCH_is_DOWN_FAST;
        protected truthVal VELOC_PITCH_is_UP_FAST;
        protected truthVal VELOC_YAW_is_RIGHT_FAST;
        protected truthVal VELOC_YAW_is_RIGHT_SLOW;
        protected truthVal VELOC_ROLL_is_NO_MOVE;
        protected truthVal VELOC_ROLL_is_LEFT_SLOW;
        protected truthVal VELOC_ROLL_is_RIGHT_SLOW;
        protected truthVal VELOC_ROLL_is_LEFT_FAST;
        protected truthVal VELOC_ROLL_is_RIGHT_FAST;
        protected truthVal ORIENT_is_RIGHT;
        protected truthVal ORIENT_is_MID_FRONT_BACK;
        protected truthVal ORIENT_is_SLIGHT_BACK;
        protected truthVal ORIENT_is_SLIGHT_FRONT;
        protected truthVal ORIENT_is_BACK;
        protected truthVal ORIENT_is_DOWN;
        protected truthVal ORIENT_is_FRONT;
        protected truthVal ORIENT_is_MID_UP_DOWN;
        protected truthVal ORIENT_is_MID_LEFT_RIGHT;
        protected truthVal ORIENT_is_SLIGHT_UP;
        protected truthVal ORIENT_is_UP;
        protected truthVal ORIENT_is_SLIGHT_DOWN;
        protected truthVal VELOC_YAW_is_LEFT_FAST;
        protected truthVal VELOC_YAW_is_LEFT_SLOW;
        protected truthVal VELOC_YAW_is_NO_MOVE;
        protected truthVal VELOC_PITCHYAW_is_DOWNLEFT_FAST;
        protected truthVal ORIENT_is_SLIGHT_RIGHT;

        protected void EvalRules(CUIControlsState UIControlsState)
        {
            VELOC_PITCH_is_DOWN_FAST = new truthVal(m_params[ParamType.VELOC_PITCH][LingVal.DOWN_FAST].Calc(UIControlsState.m_angularVelocity.m_x));
            VELOC_PITCH_is_UP_FAST = new truthVal(m_params[ParamType.VELOC_PITCH][LingVal.UP_FAST].Calc(UIControlsState.m_angularVelocity.m_x));
            VELOC_YAW_is_RIGHT_FAST = new truthVal(m_params[ParamType.VELOC_YAW][LingVal.RIGHT_FAST].Calc(UIControlsState.m_angularVelocity.m_z));
            VELOC_YAW_is_RIGHT_SLOW = new truthVal(m_params[ParamType.VELOC_YAW][LingVal.RIGHT_SLOW].Calc(UIControlsState.m_angularVelocity.m_z));
            VELOC_YAW_is_LEFT_FAST = new truthVal(m_params[ParamType.VELOC_YAW][LingVal.LEFT_FAST].Calc(UIControlsState.m_angularVelocity.m_z));
            VELOC_YAW_is_LEFT_SLOW = new truthVal(m_params[ParamType.VELOC_YAW][LingVal.LEFT_SLOW].Calc(UIControlsState.m_angularVelocity.m_z));
            VELOC_YAW_is_NO_MOVE = new truthVal(m_params[ParamType.VELOC_YAW][LingVal.NO_MOVE].Calc(UIControlsState.m_angularVelocity.m_z));
            VELOC_ROLL_is_NO_MOVE = new truthVal(m_params[ParamType.VELOC_ROLL][LingVal.NO_MOVE].Calc(UIControlsState.m_angularVelocity.m_y));
            VELOC_ROLL_is_LEFT_SLOW = new truthVal(m_params[ParamType.VELOC_ROLL][LingVal.LEFT_SLOW].Calc(UIControlsState.m_angularVelocity.m_y));
            VELOC_ROLL_is_LEFT_FAST = new truthVal(m_params[ParamType.VELOC_ROLL][LingVal.LEFT_FAST].Calc(UIControlsState.m_angularVelocity.m_y));
            VELOC_ROLL_is_RIGHT_FAST = new truthVal(m_params[ParamType.VELOC_ROLL][LingVal.RIGHT_FAST].Calc(UIControlsState.m_angularVelocity.m_y));
            VELOC_ROLL_is_RIGHT_SLOW = new truthVal(m_params[ParamType.VELOC_ROLL][LingVal.RIGHT_SLOW].Calc(UIControlsState.m_angularVelocity.m_y));
            ORIENT_is_RIGHT = new truthVal(m_params[ParamType.ORIENT_LEFT_RIGHT][LingVal.RIGHT].Calc(UIControlsState.m_orient.m_x));
            ORIENT_is_SLIGHT_RIGHT = new truthVal(m_params[ParamType.ORIENT_LEFT_RIGHT][LingVal.SLIGHT_RIGHT].Calc(UIControlsState.m_orient.m_x));
            ORIENT_is_MID_FRONT_BACK = new truthVal(m_params[ParamType.ORIENT_FRONT_BACK][LingVal.MID].Calc(UIControlsState.m_orient.m_y));
            ORIENT_is_SLIGHT_BACK = new truthVal(m_params[ParamType.ORIENT_FRONT_BACK][LingVal.SLIGHT_BACK].Calc(UIControlsState.m_orient.m_y));
            ORIENT_is_SLIGHT_FRONT = new truthVal(m_params[ParamType.ORIENT_FRONT_BACK][LingVal.SLIGHT_FRONT].Calc(UIControlsState.m_orient.m_y));
            ORIENT_is_BACK = new truthVal(m_params[ParamType.ORIENT_FRONT_BACK][LingVal.BACK].Calc(UIControlsState.m_orient.m_y));
            ORIENT_is_FRONT = new truthVal(m_params[ParamType.ORIENT_FRONT_BACK][LingVal.FRONT].Calc(UIControlsState.m_orient.m_y));
            ORIENT_is_MID_UP_DOWN = new truthVal(m_params[ParamType.ORIENT_UP_DOWN][LingVal.MID].Calc(UIControlsState.m_orient.m_z));
            ORIENT_is_SLIGHT_UP = new truthVal(m_params[ParamType.ORIENT_UP_DOWN][LingVal.SLIGHT_UP].Calc(UIControlsState.m_orient.m_z));
            ORIENT_is_UP = new truthVal(m_params[ParamType.ORIENT_UP_DOWN][LingVal.UP].Calc(UIControlsState.m_orient.m_z));
            ORIENT_is_SLIGHT_DOWN = new truthVal(m_params[ParamType.ORIENT_UP_DOWN][LingVal.SLIGHT_DOWN].Calc(UIControlsState.m_orient.m_z));
            ORIENT_is_MID_LEFT_RIGHT = new truthVal(m_params[ParamType.ORIENT_LEFT_RIGHT][LingVal.MID].Calc(UIControlsState.m_orient.m_x));
            ORIENT_is_DOWN = new truthVal(m_params[ParamType.ORIENT_UP_DOWN][LingVal.DOWN].Calc(UIControlsState.m_orient.m_z));
        }

        // NVI: the "public non-virt calling private virt" idiom
        public abstract truthVal GestureEval_(CUIControlsState UIControlsState);

        public CGesture()
        {
            // membership change points
            const double nomove_to_slow_start = 0.5;
            const double nomove_to_slow_end = 1.2;
            const double slow_to_fast_start = 1.8;
            const double slow_to_fast_end = 2.3;//2.5
            var trapGyrNoMove = new MembershipFunc(-nomove_to_slow_end, -nomove_to_slow_start, nomove_to_slow_start, nomove_to_slow_end);
            var trapGyrPosSlow = new MembershipFunc(nomove_to_slow_start, nomove_to_slow_end, slow_to_fast_start, slow_to_fast_end);
            var trapGyrPosFast = new MembershipFunc(slow_to_fast_start, slow_to_fast_end, Cvals.gyr_max_val, Cvals.gyr_max_val);

            // define fuzzy sets (one for each parameter):

            // imagine the palm is an ariplane, so we apply the terms
            // yaw, roll and pitch to it. 

            // yaw velocity (left/right @ horiz plam )
            m_params[ParamType.VELOC_YAW] = new FuzzySet
            {
                { LingVal.NO_MOVE, trapGyrNoMove },
                { LingVal.RIGHT_SLOW, trapGyrPosSlow.Mirror() },
                { LingVal.RIGHT_FAST, trapGyrPosFast.Mirror() },
                { LingVal.LEFT_SLOW, trapGyrPosSlow },
                { LingVal.LEFT_FAST, trapGyrPosFast }
            };

            // pitch velocity (up/down @ horiz plam )
            m_params[ParamType.VELOC_PITCH] = new FuzzySet
            {
                { LingVal.NO_MOVE, trapGyrNoMove },
                { LingVal.UP_SLOW, trapGyrPosSlow.Mirror() },
                { LingVal.UP_FAST, trapGyrPosFast.Mirror() },
                { LingVal.DOWN_SLOW, trapGyrPosSlow },
                { LingVal.DOWN_FAST, trapGyrPosFast }
            };

            // roll velocity (rolling left/right)
            m_params[ParamType.VELOC_ROLL] = new FuzzySet
            {
                { LingVal.NO_MOVE, trapGyrNoMove },
                { LingVal.RIGHT_SLOW, trapGyrPosSlow.Mirror() },
                { LingVal.RIGHT_FAST, trapGyrPosFast.Mirror() },
                { LingVal.LEFT_SLOW, trapGyrPosSlow },
                { LingVal.LEFT_FAST, trapGyrPosFast }
            };

            // orientation left/right
            // membership change points
            const double noorient_to_slight_start = 0.45;
            const double noorient_to_slight_end = 0.5;
            const double slight_to_orient_start = 0.65;
            const double slight_to_orient_end = 0.7;
            var trapNotOriented = new MembershipFunc(-noorient_to_slight_end, -noorient_to_slight_start, noorient_to_slight_start, noorient_to_slight_end);
            var trapSlightOriented = new MembershipFunc(noorient_to_slight_start, noorient_to_slight_end, slight_to_orient_start, slight_to_orient_end);
            var trapOriented = new MembershipFunc(slight_to_orient_start, slight_to_orient_end, Cvals.orient_max_val, Cvals.orient_max_val);

            m_params[ParamType.ORIENT_LEFT_RIGHT] = new FuzzySet
            {
                { LingVal.MID, trapNotOriented },
                { LingVal.SLIGHT_LEFT, trapSlightOriented.Mirror() },
                { LingVal.LEFT, trapOriented.Mirror() },
                { LingVal.SLIGHT_RIGHT, trapSlightOriented },
                { LingVal.RIGHT, trapOriented }
            };

            m_params[ParamType.ORIENT_UP_DOWN] = new FuzzySet
            {
                { LingVal.MID, trapNotOriented },
                { LingVal.SLIGHT_UP, trapSlightOriented.Mirror() },
                { LingVal.UP, trapOriented.Mirror() },
                { LingVal.SLIGHT_DOWN, trapSlightOriented },
                { LingVal.DOWN, trapOriented }
            };

            m_params[ParamType.ORIENT_FRONT_BACK] = new FuzzySet
            {
                { LingVal.MID, trapNotOriented },
                { LingVal.SLIGHT_FRONT, trapSlightOriented.Mirror() },
                { LingVal.FRONT, trapOriented.Mirror() },
                { LingVal.SLIGHT_BACK, trapSlightOriented },
                { LingVal.BACK, trapOriented }
            };

        }
        // evaluation function for this gesture >> NVI: the "public non-virt calling private virt" idiom
        public truthVal GestureEval(CUIControlsState UIControlsState)
        {
            // calc raw gesture truth value
            double gestVal = GestureEval_(UIControlsState);

            // calc fillter alpha to accomodate current sampling rate
            if (UIControlsState.m_IMUsamplePeriodMSec != 0)
            {
                var effectiveRC = GetRC() * m_gestureSensFact;
                m_pTimeAverFilt.SetAlpha(CExpFilter.CalcAlpha(UIControlsState.m_IMUsamplePeriodMSec, effectiveRC));
            }

            return m_pTimeAverFilt.OnInput(gestVal);
        }
        // type of gesture
        public abstract GestureId GetGestType();
        public void InitSession()
        {
            m_pTimeAverFilt.InitSession();
        }
        public abstract double GetRC();
        public void SetSensitivity(double sensFact)
        {
            /*
            sens factor 0.5 - 2.0
            */
            EST_INVARIANT_THROW_RTE(sensFact >= 0.5 && sensFact <= 2.0);

            m_gestureSensFact = sensFact;
        }
        public double GetSensitivity()
        {
            return m_gestureSensFact;
        }
        public void SetEnabled(bool bEnabled)
        {
            m_bEnabled = bEnabled;
        }
        public bool GetEnabled()
        {
            return m_bEnabled;
        }
    };

    class CGesture_SlideLeft : CGesture
    {
        override public truthVal GestureEval_(CUIControlsState UIControlsState)
        {
            EvalRules(UIControlsState); // evaluate all rules 

            // calc the rules for this gesture

            /* 
                #gr - differentiate the real left/right gesture from mere movement
                - when user means a gesture, the palm is usu. open (as if to reach
                for the virtual object to be moved)
                - thus, we should add rules for LINEAR velocity, to detect when 
                the palm is moved open (ie. the lin. velocity is perpend. to
                the finger).

            */

            truthVal dynVal =
                // wrist/fingers are closing
                VELOC_PITCH_is_DOWN_FAST;// &&
                                         // fingers move down 
                                         //(VELOC_YAW_is_RIGHT_FAST || VELOC_YAW_is_RIGHT_SLOW);

            truthVal statVal =
                // backhand to the right (palm to the left)
                ORIENT_is_RIGHT.And(
                // arm orientation straigh or down a bit
                (ORIENT_is_MID_FRONT_BACK.Or(ORIENT_is_SLIGHT_BACK)).And(
                // arm roll strait (+/-)
                (ORIENT_is_MID_UP_DOWN.Or(ORIENT_is_SLIGHT_UP.Or(ORIENT_is_SLIGHT_DOWN)))));

            truthVal retVal = dynVal.And(statVal);

            return retVal;
        }
        override public GestureId GetGestType() { return GestureId.gestId_left; }
        override public double GetRC() { return Cvals.slideleft_RC; }
        public CGesture_SlideLeft()
        {
            m_pTimeAverFilt = new CExpFilter(CExpFilter.CalcAlpha(Cvals.ref_prd_msec, GetRC()), FILTER_LPASS);
        }
    };

    public class CGesture_SlideRight : CGesture
    {
        public CGesture_SlideRight()
        {
            m_pTimeAverFilt = new CExpFilter(CExpFilter.CalcAlpha(Cvals.ref_prd_msec, GetRC()), FILTER_LPASS);
        }
        public override truthVal GestureEval_(CUIControlsState UIControlsState)
        {
            EvalRules(UIControlsState); // evaluate all rules 

            // calc the rules for this gesture

            /* 
                #gr - differentiate the real left/right gesture from mere movement
                - when user means a gesture, the palm is usu. open (as if to reach
                for the virtual object to be moved)
                - thus, we should add rules for LINEAR velocity, to detect when 
                the palm is moved open (ie. the lin. velocity is perpend. to
                the finger).

            */

            truthVal dynVal =
                // wrist/fingers are closing
                VELOC_PITCH_is_UP_FAST;// &&
                                       // fingers move down 
                                       //(VELOC_YAW_is_RIGHT_FAST || VELOC_YAW_is_RIGHT_SLOW);

            truthVal statVal =
                // backhand to the right (palm to the left)
                ORIENT_is_RIGHT.And(
                // arm orientation straigh or down a bit
                (ORIENT_is_MID_FRONT_BACK.Or(ORIENT_is_SLIGHT_BACK)).And(
                // arm roll strait (+/-)
                (ORIENT_is_MID_UP_DOWN.Or(ORIENT_is_SLIGHT_UP.Or(ORIENT_is_SLIGHT_DOWN)))));

            truthVal retVal = dynVal.And(statVal);

            return retVal;
        }
        public override GestureId GetGestType() { return GestureId.gestId_right; }
        public override double GetRC() { return Cvals.slideright_RC; }
    }

    public class CGesture_PrayAndPush : CGesture
    {
        public CGesture_PrayAndPush()
        {
            m_pTimeAverFilt = new CExpFilter(CExpFilter.CalcAlpha(Cvals.ref_prd_msec, GetRC()), FILTER_LPASS);
        }
        public override truthVal GestureEval_(CUIControlsState UIControlsState)
        {

            return new truthVal();
        }
        public override GestureId GetGestType() { return GestureId.gestId_prayAndPush; }
        public override double GetRC() { return Cvals.pnp_RC; }
    };

    //    class CGesture_PullAnArrow : public CGesture {
    //	truthVal GestureEval_(const CUIControlsState& UIControlsState) override;
    //	GestureId GetType() const override { return GestureId::gestId_pullAnArrow; }
    //double GetRC() const override { return Cvals.pullAnArrow_RC; }
    //public:

    //    CGesture_PullAnArrow();
    //};

    //class CGesture_PullBowBack : public CGesture {
    //	truthVal GestureEval_(const CUIControlsState& UIControlsState) override;
    //	GestureId GetType() const override { return GestureId::gestId_pullBowBack; }
    //	double GetRC() const override { return Cvals.pnp_RC; }
    //public:

    //    CGesture_PullBowBack();
    //};

    //class CGesture_ReloadAGun : public CGesture {
    //	truthVal GestureEval_(const CUIControlsState& UIControlsState) override;
    //	GestureId GetType() const override { return GestureId::gestId_reloadAGun; }
    //	double GetRC() const override { return Cvals.pnp_RC; }
    //public:

    //    CGesture_ReloadAGun();
    //};


    class CGesture_Up : CGesture
    {

        public CGesture_Up()
        {
            m_pTimeAverFilt = new CExpFilter(CExpFilter.CalcAlpha(Cvals.ref_prd_msec, GetRC()), FILTER_LPASS);
        }
        public override truthVal GestureEval_(CUIControlsState UIControlsState)
        {
            EvalRules(UIControlsState); // evaluate all rules 

            // calc the rules for this gesture

            /* 
                #gr - differentiate the real left/right gesture from mere movement
                - when user means a gesture, the palm is usu. open (as if to reach
                for the virtual object to be moved)
                - thus, we should add rules for LINEAR velocity, to detect when 
                the palm is moved open (ie. the lin. velocity is perpend. to
                the finger).

            */

            truthVal dynVal =
                // wrist/fingers going up
                VELOC_PITCH_is_DOWN_FAST.And(
                // slight roll is allowed
                VELOC_ROLL_is_NO_MOVE.Or(VELOC_ROLL_is_RIGHT_SLOW.Or(VELOC_ROLL_is_LEFT_SLOW)));

            truthVal statVal =
                // all movement is around x axis, g on x is ~= 0
                ORIENT_is_MID_LEFT_RIGHT.And(
                ORIENT_is_MID_UP_DOWN.Or(ORIENT_is_SLIGHT_UP.Or(ORIENT_is_UP)));

            truthVal retVal = dynVal.And(statVal);
            return retVal;
        }
        public override GestureId GetGestType() { return GestureId.gestId_up; }
        public override double GetRC() { return Cvals.slideup_RC; }
    }

    class CGesture_Down : CGesture
    {
        public CGesture_Down()
        {
            m_pTimeAverFilt = new CExpFilter(CExpFilter.CalcAlpha(Cvals.ref_prd_msec, GetRC()), FILTER_LPASS);
        }
        public override truthVal GestureEval_(CUIControlsState UIControlsState)
        {
            EvalRules(UIControlsState); // evaluate all rules 

            // calc the rules for this gesture

            truthVal dynVal =
                // wrist/fingers going down
                VELOC_PITCH_is_DOWN_FAST.Or(VELOC_YAW_is_RIGHT_FAST).And(
                // slight roll is allowed
                VELOC_ROLL_is_NO_MOVE.Or(VELOC_ROLL_is_RIGHT_SLOW.Or(VELOC_ROLL_is_LEFT_SLOW)));

            truthVal statVal =
                (ORIENT_is_MID_LEFT_RIGHT | ORIENT_is_SLIGHT_RIGHT) &
                (ORIENT_is_MID_UP_DOWN | ORIENT_is_SLIGHT_DOWN | ORIENT_is_DOWN);

            truthVal retVal = dynVal & statVal;

            return retVal;
        }
        public override GestureId GetGestType() { return GestureId.gestId_down; }
        public override double GetRC() { return Cvals.slidedown_RC; }
    };

    //class CGesture_Go : public CGesture {
    //	truthVal GestureEval_(const CUIControlsState& UIControlsState) override;
    //	GestureId GetType() const override { return GestureId::gestId_go; }
    //	double GetRC() const override { return Cvals.default_RC; }
    //public:

    //    CGesture_Go();
    //};

    //class CGesture_Come : public CGesture {
    //	truthVal GestureEval_(const CUIControlsState& UIControlsState) override;
    //	GestureId GetType() const override { return GestureId::gestId_come; }
    //	double GetRC() const override { return Cvals.default_RC; }
    //public:

    //    CGesture_Come();
    //};


    class CGestureDB : List<CGesture>
    {

        static class Cvals
        {
            public static double gest_detec_hi_thr = 0.7; // above it we detect a gesture
            public static double gest_detec_lo_thr = 0.5; // below it we retire a gesture
        }

        int m_lastDetectedGestInd = -1;
        void InitSession()
        {
            foreach (var gest in this)
            {
                gest.InitSession();
            }
        }


        public CGestureDB(List<CGesture> gests) : base(gests)
        {
            m_lastDetectedGestInd = -1;
        }
        // we don't convert back to crisp values. instead, we want to choose the
        // most likely gesture, given the input data
        string GestureIdToString(GestureId gId)
        {
            switch (gId)
            {
                case GestureId.gestId_left: return "left";
                case GestureId.gestId_right: return "right";
                case GestureId.gestId_up: return "up";
                case GestureId.gestId_down: return "down";
            }
            return "undefined";
        }
        public GestureId Arbiter(CUIControlsState UIControlsState)
        {
            /*
                we don't convert back to crisp values. instead, we want to choose the
                most likely gesture, given the input data
                - we use hysteresis
                */

            // if some prev gesture detected, it must decay before we look for another one
            if (m_lastDetectedGestInd > -1)
            {
                var pGest = this[m_lastDetectedGestInd];
                // did the gesture reach the lo thr ?
                if (pGest.GestureEval(UIControlsState) < CGestureDB.Cvals.gest_detec_lo_thr)
                {
                    m_lastDetectedGestInd = -1; // no current detected gesture
                                                //InitSession();		// reset all gesture sessions
                }
                else
                {
                    return pGest.GetGestType();
                }
            }

            // if no prev gesture detected, look for one
            if (m_lastDetectedGestInd == -1)
            {
                double maxVal = 0;
                int iGest = -1;
                for (int i = 0; i < Count; i++)
                {
                    if (!this[i].GetEnabled())
                        continue;
                    double thisVal = 0;
                    if (maxVal < (thisVal = this[i].GestureEval(UIControlsState)))
                    {
                        maxVal = thisVal;
                        iGest = i;
                    }
                }
                // is the winner above our hi thr?
                if (iGest != -1 && maxVal >= Cvals.gest_detec_hi_thr)
                {
                    m_lastDetectedGestInd = iGest;
                    LOG.I("Selected Gesture " + GestureIdToString(this[iGest].GetGestType()) + "; maxVal=" + maxVal);
                    return this[iGest].GetGestType();
                }
            }
            return GestureId.gestId_invalid;
        }
    }

    class CGestureIntpr
    {

        public static int def_sensitivity = 50;
        CGestureDB m_gestDB = new CGestureDB(new List<CGesture>()
	// list of possible gestures
	    {
            new CGesture_SlideLeft(),
            new CGesture_SlideRight(),
            new CGesture_Up(),
            new CGesture_Down(),
        });

        public CGestureIntpr()
        {
            foreach (var gest in m_gestDB)
            {
                SetSensitivity(def_sensitivity, gest);
            }
        }
        CGesture GetGestById(GestureId gestId)
        {
            int index = (int)gestId - 1;
            if (index >= 0 && index < m_gestDB.Count)
                return m_gestDB[index];
            return null;
        }
        public GestureId OnUIState(CUIControlsState UIControlsState)
        {
            return m_gestDB.Arbiter(UIControlsState);
        }
        int GetSensitivity(GestureId gestId)
        {
            CGesture gest = GetGestById(gestId);
            if (gest == null)
                return 0;
            var gestureSens = gest.GetSensitivity();
            return (int)(Math.Round(Math.Log(1.0 / gestureSens) / Math.Log(2) * def_sensitivity) + def_sensitivity);
        }
        void SetSensitivity(int sens, CGesture gest)
        {
            double gestureSens = 1.0 / Math.Pow(2.0, (double)(sens - def_sensitivity) / def_sensitivity);
            gest.SetSensitivity(gestureSens);
        }
        bool GetEnabled(GestureId gestId)
        {
            CGesture gest = GetGestById(gestId);
            if (gest != null)
                return gest.GetEnabled();
            return false;
        }
        void SetEnabled(GestureId gestId, bool Enabled)
        {
            CGesture gest = GetGestById(gestId);
            if (gest != null)
                gest.SetEnabled(Enabled);
        }
    }
}